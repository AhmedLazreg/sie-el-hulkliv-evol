/* ------------------------------------------------------------------------------
Auteur : Ahmed LAZREG
Date   : 12/04/2012

Objectifs :

Dans le cadre du projet HULK, realiser plusieurs taches.

- Generer des fichiers liv/uc/*.uc.xml
- Generer des fichiers liv/uc/*.ucx.xml
- Generer des fichiers liv/exalead/xml/*.urx.xml

-------------------------------------------------------------------
23/05/2012 AL
-------------------------------------------------------------------
Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;


-------------------------------------------------------------------
01/02/2013 AL
-------------------------------------------------------------------
Ajout de parametres "-test" "-prod" "-sbox1" à la ligne de commande
Cela permettra de faire des traitements differents selon les cas
-prod ou prod = on travaille sur hulk/liv/
-test ou test = on travaille sur hulk/livtest/
-sbox1 ou sbox1 = on travaille sur hulk/sandbox1/
//
-------------------------------------------------------------------
26/02/2013 AL
-------------------------------------------------------------------
Ajout d'une variable G_smode pour stocker la plate-forme cible "prod", "test" ou "sandbox1"

------------------------------------------------------------------------------
26/02/2013 AL
-------------------------------------------------------------------
Demarrage du versionnig manuel du programme hulk.bal
pour initier le projet de versionning des scripts, programmes, dtd, etc.

Ce programme a ete copie en version 1.0

------------------------------------------------------------------------------
26/02/2013 AL
------------------------------------------------------------------------------
version 1.1

lots de correctifs ou evolutions :

1 en test desactiver la generation des zapette/thematiques pour les sources dans les urx.xml


------------------------------------------------------------------------------
11/03/2013 AL
------------------------------------------------------------------------------
Version 1.2

Integration des documents EL dans dalloz.fr

1 mise en place des uc/titre1 pour dalloz
2 patch des attribut debua@T (pour les etudes + formules)

3 modification de l'element uc/recherche

------------------------------------------------------------------------------
17/05/2013 AL
Evolution de l'attribut debua@M document-id
Pour les formule indiquer document-id = id de la formules
Exemple document-id="Z2M011001, document-id="Z2M011002"

// 24/11/2015 MB :
Correction général sur les URF (stats XITi)
------------------------------------------------------------------------------
15/06/2016 alazreg ajout du cas GP95/FREG avec DTD FPRO
------------------------------------------------------------------------------
28/11/2016 mantis 13898 cas du code civil dalloz, articles anciens
------------------------------------------------------------------------------ */
// 07/07/2017 ALAZREG LAB15 : utilisation d'une globale pour faciliter le test de la matiere CDXX GPXX dans differentes fonctions
// ATTENTION : a déclarer avant le include lib/hulk.lib car la librairie utilise cette variable. Sinon ERREUR de compilation et exécution.
var g_codeMatiere = "";

#include "dico.var"
#include "lib/ccol.lib"
#include "lib/utils.lib"
#include "lib/pmap.lib"
#include "lib/string.lib"
#include "lib/hulk.lib"
#include "lib/opt.regex"
#include "lib/parse.lib"
#include "lib/dz.lib"
#include "lib/time.lib"

/* ------------------------------------------------------------------------------
Variables globales
------------------------------------------------------------------------------ */

#define progname "hulk.bal"
#define version "1.02.01"

// 21/09/2016 alazreg map de transcodage vers unicode pour avoir des xml valides sans entités nommées
var g_map_entites_nommees_to_unicode = Map(
"&nbsp;","&#160;"
,"&apos;","&#700;"
,"&Eacute;","&#201;"
,"&agrave;","&#224;"
,"&egrave;","&#232;"
,"è","&#232;"
,"&eacute;","&#233;"
,"é","&#233;"
,"&ecirc;","&#234;"
,"&icirc;","&#238;"
,"&oelig;","&#339;"
,"&ocirc;","&#244;"
,"&ucirc;","&#251;"
,"&acirc;","&#226;"
,"&rsquo;","&#8217;"
,"&lsquo;","&#8216;"
,"&ccedil;","&#231;"
,"&raquo;","&#187;"
,"&laquo;","&#171;"
,"&emsp14;","&#160;"
,"&ugrave;","&#249;"
,"&amp;","&#38;"
,"&mdash;","&#8212;"
,"&plus;","&#43;"
,"&lsqb;","&#91;"
,"&rsqb;","&#93;"
,"&deg;","&#176;"
,"\"","&#34;"
);

// 21/09/2016 alazreg mantis 14215 : ajout d'une Map pour ordonner les elements //lien de la zone Voir aussi
// 07/10/2016 alazreg mantis 14253 : remplacement de la Map g_map_voir_aussi_ccol par 2 Map spécifiques g_map_voir_aussi_ccol et g_map_voir_aussi_synt
// 									En attente de trouver une idée géniale pour ordonner tous les liens "Voir aussi" dans une seule Map
// var g_map_voir_aussi_rang = Map(
// "Derniers textes intégrés","0"
// ,"La jurisprudence","1"
// ,"Alerte CC","2"
// ,"La synthèse","3"
// ,"Texte intégral (Format PDF)","4"
// ,"Archives","5"
// ,"Accords et arrêtés","600"
// );

// 02/10/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5
// ajout Map pour centraliser les picto
var G_map_picto=Map(
"Nouveautés du mois","nouveaute_du_mois.png"
,"Nouveautés de la quinzaine","nouveaute_du_mois.png"
);

// 02/10/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5
// ajouter un lien "Nouveautés de la quinzaine" sous "Alerte CC"
var g_map_voir_aussi_ccol = Map(
"Alerte CC","1"
,"Nouveautés de la quinzaine","2"
,"Synthèse","3"
,"Synthèse (CCN)","4"
,"Synthèse (région)","5"
,"Synthèse (champs d'application)","6"
,"Synthèse (accords nationaux)","7"
,"Texte intégral (format pdf)","8"
,"Synthèse (format pdf)","9"
,"Texte intégral","10"
,"Texte intégral (ouvriers)","11"
,"Texte intégral (ETAM)","12"
,"Texte intégral (employés)","13"
,"Texte intégral (cadres)","14"
,"Derniers textes intégrés","15"
,"Accords et arrêtés","16"
,"Jurisprudence","17"
// ,"Archives","17"
// mantis jouve 13189
// ,"Versions antérieures","17"
,"Texte intégral (versions antérieures)","18" // https://jira.els-gestion.eu/browse/SIECDP-34
,"Synthèse (versions antérieures)","18" // https://jira.els-gestion.eu/browse/SIECDP-34
);

// 10/03/2014 alazreg debug
var G_bLoadSetJrpDz = true;

// 03/06/2013 alazreg
// ajout Map pour les Alerte Redaction
var G_mapAlerteRedaction = nothing;

// 03/10/2012 AL/MB
var G_mapJrpinfo = nothing;
// 0105/2013 MB : ajout var elnet_liv_dir_sgm pour livrer les code EL utilises dans les sgm a partir de elnet
var elnet_liv_sgm_dir = filePath(env("ELA_CDROM"),"elnet","liv","sgm");
// 20/08/2012 AL/MB : ajout set des ids jrp a ne pas transformer 
// tester pour les 1400 jrp EL a pousser chez jouve
var G_setIdjrpNoTransform = Set();
// 13/09/2012 MB : utilisation de la liste de jrp fournies par DZ :
//                 ELA_DATACOMM/jrp/dz/logic-id/JURIS_LOGIC_ID-distinct.txt
//                 pour que les EL livrent les jrp absentes de cette liste

// 19/06/2013 MB : nouvelle livraison de DZ ==> fichier a prendre en compte : 2013-05-22_HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.TXT
//var fileTxt_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","JURIS_LOGIC_ID-distinct.txt");
//var fileSet_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","JURIS_LOGIC_ID-distinct.set"); // pour charger directement le set si plus recent que le fichier txt

// 17/03/2014 SF/MB : nouvelle livraison de DZ ==> fichier a prendre en compte : 2013-05-22_HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.txt
//var fileTxt_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","2013-05-22_HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.txt");
//var fileSet_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","2013-05-22_HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.set"); // pour charger directement le set si plus recent que le fichier txt

var fileTxt_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.txt");
var fileSet_IDs_jrp_DZ = filePath(env("ELA_DATACOMM"), "jrp/dz/logic-id","HULK_JURISPRUDENCE_EJP_NEW_LOGIC_ID.set"); // pour charger directement le set si plus recent que le fichier txt


var G_setAllDZJrp = Set();
var G_setIdjrpToBuild = Set();
// 23/07/2013 AL/MB : ajout set pour ne pas livrer toutes les jrp : option "-jrpmat"
var G_setIdjrpToBuildMat = Set();

//var exoticjrpFile = filePath(env("ELA_TMP_IDX"),"HulkJrpToBuild.set");
var exoticjrpFile = filePath(env("ELA_LIV"),"miscdata","HulkJrpToBuild.set");
var exoticjrpFileMat = filePath(env("ELA_LIV"),"miscdata","HulkJrpToBuildMat.set");
// 22/07/2013 MB : on ajoute un fichier pour les jrp a nettoyer de mongodb (nettoyees du set HulkJrpToBuild.set car disponible dans la nouvelle liste DZ)
var G_fileIdjrpToClean = filePath(env("ELA_LIV"),"miscdata","G_fileIdjrpToClean.txt");
var G_setIdjrpToClean = Set();

// 05/11/2012 MB : ajouts de 2 maps pour les infos baseref
//                 utilises pour recuperer le nom abrege de la juridiction
//                 pour la construction des id logiques des jrp trouvees dans comjrp
var G_mapBaseRefIdDpm = Map();
var G_mapBaseRefAbrevNames = Map();
// 08/10/2013 MB : declaration 	G_mapMDFduTexte  en globale car sinon erreur ds fct inline
var G_mapMDFduTexte ; 

var G_mapIdEtIdRecapnew = Map();

var G_mapIdetLstThemesBoVeille = Map();

var G_exitval = 0;
var G_ela_liv = nothing;

var G_maptxtdata = Map();
var G_mapTxtVersion = nothing;
// 07/10/2013 MB : on ajoute la map tx versionning
var G_maptxtversionning = nothing;
var G_mapTxtMDF = nothing;

var G_bJrp = false; // debug pour preparer les id logiques jrp

var G_bMfi = false;

var G_bUc = false;
var G_bExalead = false;
var G_bPatchSgml = false;
var G_bUnicode = false;
// var G_bCdcoll = false;
var G_bSfiles = false;
var G_bSverif = false;

// -------------------------------------------------------------------
// 01/02/2013 AL
// Ajout de parametres "-test" "-prod" "-sbox1" à la ligne de commande
// Cela permettra de faire des traitements differents selon les cas
// -prod ou prod = on travaille sur hulk/liv/
// -test ou test = on travaille sur hulk/livtest/
// -sbox1 ou sbox1 = on travaille sur hulk/sandbox1/
// -------------------------------------------------------------------
var G_bTest = false;
var G_bProd = false;
var G_bSbox = false;

// 26/02/2013 AL
// Ajout d'une variable pour stocker la plate-forme cible "prod", "test" ou "sandbox1"
var G_smode = "na";

// -------------------------------------------------------------------


var G_dirin = nothing;
var G_filesgm = nothing;

var G_mapFilenameDtd = Map();

var G_fileLogName = nothing;
var G_fileLog = nothing;
var G_enteteXml = nothing;
var G_regexpToc = RegExp("\\.toc\\.sgm$");
var G_regexpTap = RegExp("\\.tap\\.sgm$"); 
// 10/10/2012 MB : ajout regexp pour les fichiers images
var G_regexpIcon = RegExp("\\.[a-zA-Z]+$");
var G_regexpSpace = RegExp(" +");
var G_regexpPdf = RegExp("\\.pdf$");
var G_regexpIDTronq = RegExp("-.+$");
// 11/07/2013 MB : on met dans ELA_LIV (avec les fichiers de log)
//var Hulk_icons_Ok_Nok = filePath(env("ELA_TMP_IDX"),"Hulk_icons_Ok_Nok.trace");
var Hulk_icons_Ok_Nok = filePath(env("ELA_LIV"),"Hulk_icons_Ok_Nok.trace");

// 11/10/2012 MB : ajout regexp pour inserer les IDs logiques jrp dans les elts ID3 
//                 ne concerne que les indjrp et les indetjrp
var regexpIndJrp = RegExp("[dg]p[0-9][0-9]_ind(et)?jrp.optj.sgm");

// 10/09/2013 Mb : ajout regexp pour jrp EJP V2 
//                 ne pas les livrer meme si ID logique absent de liste DZ
var regexpIDEJPV2 = RegExp("^COURD");

// 29/03/2013 MB : apres le code minier, d'autres codes doivent egalement être traites en code EL
//                ==> on ajoute donc le set set G_setCodesELavecTypeNo, pour regrouper ces codes 

// 03/09/2013 MB : le code civil ancien EL ne doit plus etre livre (suite demande Dalloz, mantis EL : 4100) ==> reporte (voir mail J Skrabo du 09/09/2013 10:58)
//                 a reporter en prod apres tests PE et validation Moe/Moa
// 12/08/2014 MB : mantis 2693 ==> on n'utilise plus de code minier EL, on le remplace par celui de Dalloz
// var G_setCodesELavecTypeNo = Set("CM","CDC", "CTM", "CMP", "CPCA", "CRA", "CCVA", "CUA");
var G_setCodesELavecTypeNo = Set("CDC", "CTM", "CMP", "CPCA", "CRA", "CCVA", "CUA");

//attente mise en place DZ var G_setCodesELavecTypeNo = Set("CM","CDC", "CTM", "CMP", "CPCA", "CRA", /*"CCVA",*/ "CUA");

// 02/05/2013 MB : ajout liste pour le traitement du mode flow
var G_listGiFormtx = List("FORMT1","FORMT2","FORMT3","FORMT4","FORMT5","FORMT6","FORMT7");
var G_setGiFormtx  =  Set("FORMT1","FORMT2","FORMT3","FORMT4","FORMT5","FORMT6","FORMT7");

//Ye 07/05/2014 Mantis 2763
var G_setUri = Set();
//
// YE 02/07/2014 Mantis 7258
var G_mapBlocsNameRjOfOuvrageId = nothing;
var G_idelemlast = "";
var G_mapBlocsRj = Map();

// 08/01/2016 SF ajout PackOpe
var G_mapPackTitles = Map();
var G_mapPackDocuments = Map();

// 26/06/2016 SF ajout Preventeur
var G_mapPreventeurDocuments = Map();

// 23/11/2016 SF ajout pour migration GB
var G_mapRenvoisGB = Map();

// YE 04/04/2014 Mantis 6632
// Cette fonction retourne le rang des archives des conventions collectives
// Dont le but de les classer par ordre chronologiques 

function getRang_archive_dp15(racine){
	// 04/06/2014 alazreg/sfouzi
	// mauvais classement des archives
	// remplacement du explode dessous par l'attribut ARCHIVE de la racine
	var rang = 0;
	// var currentYear = dec(timeFormat(timeCurrent(),"%Y"));
	var currentYear = 2500;
	//cout << format("debug function getRang_archive_dp15 currentYear = %1\n",currentYear);

	// par defaut on positionne l'anne de l'archive sur l'annee courante.
	var archiveYear = currentYear;
	if hasAttr(racine,"ARCHIVE") archiveYear = dec(attr["ARCHIVE",racine]); // idSgm = CC002ARCHIVE2013 => archiveYear = 2013

	if archiveYear != nothing && isaNumber(archiveYear){
		rang = currentYear - archiveYear;
	}
	if rang !=0 return rang;
	else return nothing;
}

function set_exitval(nombre){
	// Cette fonction verifie et affecte a G_exitval la valeur la plus grande
	// Cela permet de sortir du programme avec le code d'erreur le plus important
	// 0 = tout va bien
	// 1 = warning
	// 2 = erreurs
	if nombre > G_exitval G_exitval = nombre;
}

var G_mapRecmaj = nothing;

function initMapRecmaj() {
	// dp15 : recupere les couples (CC,IDrecmaj) utilises pour les liens à droite des CC

	var dtd, pr, r ;
	var mapRecmaj = Map();

	// 11/08/2017 ALAZREG https://jira.els-gestion.eu/browse/SIEDV-5
	// c'est plus simple d'avoir toutes les datas regroupées dans le même dossier
	// var recmajfile = filePath(env("ELA_LIV"),"alimhulk","dp15","sgm","recmaj.optj.sgm");
	var recmajfile = filePath(env("ELA_LIV"),"sgm","recmaj.optj.sgm");
	// cout << format("\ninitMapRecmaj : lecture du recmaj : '%1'...",recmajfile);

	dtd = filePath(env("ELA_DTD"),"recmaj-optj.dtd");
	pr = parseDocument(List(env("ELA_DEC"),dtd,recmajfile),Map("cat",List(env("CATALOG"))));
	if pr.status == 0 {

		r = root(pr.document);

		for nodeMAJCC in searchElemNodes(r,"MAJCC"){
			// si l'un des deux attributs est absent alors on passe au MAJCC suivant
			if not hasAttr(nodeMAJCC ,"CCOL") continue;
			if not hasAttr(nodeMAJCC,"ID") continue;
			mapRecmaj[attr["CCOL",nodeMAJCC].transcript(UpperCase)] = attr["ID",nodeMAJCC].transcript(UpperCase);
		}
	}

	return mapRecmaj;
}

// function printn(message){
	// cette fonction peut etre utilisee si on repete beaucoup le caracte \n dans le code source
	// cout << message+"\n";
// }

function donne_moi_un_modulo(nombre){
	// cette fonction calcule le modulo moyen en fonction du nombre reçu en parametre

	// -------------------------------------------------------------------
	// par defaut on retourne 10% du nombre reçu
	// solution abandonnee car retourne des nombres qui ne sont pas multiple de 10
	// => donc pas ergonomiques à la lecture
	// -------------------------------------------------------------------
	//var modulo = dec(nombre*0.1);
	//if (dec(modulo) == 0) modulo = 1;
	//return modulo;

	if (nombre < 100) return 10;
	if (nombre < 1000) return 100;
	if (nombre < 10000) return 500;
	if (nombre < 100000) return 1000;
	return 2000; // valeur par defaut si nombre superieur a 100 000
}

function nettoyerTitre(tiet){
	// Cette fonction nettoie les titres des etudes
	// On garde seulement les caracteres a-z A-Z 0-9
	//
	// normalise : &Eacute; => E &eacute; => e
	// NoAccents : É => E  e => e

	//return tiet.normalise(listechars).replace(0,"&rsquo;","'").replace(0,"&nbsp;"," ").replace(0,"[^a-zA-Z0-9]","").transcript(LowerCase);
	return tiet.transcript(LowerCase).normalise(listchars).transcript(NoAccents).replace(0,"&#150;","-").replace(0,"&#156;","&oelig;").replace(0,"&#339;","&oelig;").replace(0,"&#146;","&rsquo;").replace(0,"&rsquo;","'").replace(0,"&nbsp;"," ").replace(0,"[^a-zA-Z0-9]","");
}

function initMapIdetThemesBoVeille_hulk(){
	// Cette fonction initialise une Map G_mapIdetLstTheme
	// Cles = idref des etudes
	// valeurs = Liste des themes associes à l'etude
	// Exemples fictifs :
	// Z1002 => List(theme1, theme2, theme3)
	// W6023 => List(theme1, theme2)

	// 06/08/2012 AL
	// Nouveau
	// Utilisation du script boveilletheme2map pour mettre a jour la Map des theme avant son utilisation
	// Sinon la Map etait mise a jour ponctuellement
	//
	//var filename = filePath(env("ELA_TMP_IDX"),"boveilletheme.map"); // pour elnet prendre les themes boveille
	//var filename = filePath(env("ELA_TMP_IDX"),"boveilletheme_hulk.map"); // pour hulk prendre les theme hulk fournis par carine niot
	system("/usr/local/ela/bin/boveilletheme2map /elweb/portailel/documents/hulk/etudes_theme_hulk.csv -hulk");

	var filename = filePath(env("ELA_TMP_IDX"),"boveilletheme_hulk2.map"); // pour hulk prendre les theme hulk fournis par carine niot
	var G_mapBoveilleThemes = loadObjectFromFile(filename);
	if G_mapBoveilleThemes == nothing G_mapBoveilleThemes = Map();

	var G_setTitresNonTrouves = Set();

	var maplstet = nothing;
	var titreNettoye = nothing;
	var maplstetNettoye = Map();
	var idet = nothing;
	var mapIdetTiet = Map();

	for dp in eSort(G_mapBoveilleThemes){
		//if dp != "DP02" continue;

		// Recuperation des titres + id d'etudes depuis les Map elnet/liv/miscdata/dpxx_lst-et.clt
		filename = filePath(G_ela_liv,"miscdata",dp.transcript(LowerCase)+"_lst-et.clt");
		if fileType(filename) != "file"{
			//cout << format("Le fichier %1 n'existe pas\n",filename);
			continue;
		}

		//cout << format("Je traite %1\n",filename);

		//maplstet = nothing;
		maplstet = loadObjectFromFile(filename);
		if maplstet == nothing maplstet = Map();
		maplstetNettoye = Map();

		// 04/10/2011 AL
		// Nettoyage des titres dans les map dpxx_lst-et.clt
		// On ajoute dans la maplstet locale les versions nettoyees des titres d'etudes
		// Cela facilitera la compraison plus bas avec les titre issu du csv boveille
		for tiet, idet in maplstet mapIdetTiet[idet] = tiet;

		for tiet, idet in maplstet{
			titreNettoye = nettoyerTitre(tiet);
			maplstetNettoye[titreNettoye] = idet;
			//cout << format("titreNettoye = '%1'\n",titreNettoye);
		}

		// Debug
		//cout << format("\n%1 titres d'etudes nettoyes\n\n",maplstetNettoye.length());
		//for titreNettoye in eSort(maplstetNettoye) cout << format("titreNettoye = '%1'\n",titreNettoye);

		// Classement des themes par etudes
		// Z1002 => List(theme1, theme2, theme3)
		for theme in G_mapBoveilleThemes[dp]{
			for tiet in G_mapBoveilleThemes[dp][theme]{
				titreNettoye = nettoyerTitre(tiet);
				if maplstetNettoye.knows(titreNettoye){
					idet = maplstetNettoye[titreNettoye];
					if !G_mapIdetLstThemesBoVeille.knows(idet) G_mapIdetLstThemesBoVeille[idet] = List();
					G_mapIdetLstThemesBoVeille[idet] << theme ; // YTR 25/11/2011 : on supprime le suffixe de test : + "(theme boveille)";
				}
				else{
					// Titre de l'etude inconnu dans le dp
					//cout << format("%1 titre etude inconnu '%2'\n",dp,tiet);
					G_setTitresNonTrouves << format("%1 titre etude inconnu '%2'\n",dp,tiet);
				}
			}
		}
	}

	// Envoi par mail des titres d'etudes inconnus dans le fonds documentaire ELnet
	if G_setTitresNonTrouves.length() != 0{
		filename = filePath(G_ela_liv,"boveille.hulk.mail.txt");
		var file = FileStream(filename,"w");
		file << format("\nHULK %1 titres d'etudes du bo veille sont inconnus dans les etudes du fonds documentaire ELnet\n\n",G_setTitresNonTrouves.length());
		for titre in eSort(G_setTitresNonTrouves) file << titre;
		close(file);
		var dest = "$user alazreg";
		var sujet = "HULK BO veille titres etudes inconnus";
		var mailcontent = "";
		// 24/09/2012 mb 
		//sendmail(dest,sujet,mailcontent,filename);
	}

	return 0;

	// Debug : affichage des themes classes par etude
	/*
	cout << "\naffichage des themes classes par etude\n\n";
	for idet in eSort(G_mapIdetLstThemesBoVeille){
	cout << format("%1\t%2\n",idet,mapIdetTiet[idet]);
	for theme in G_mapIdetLstThemesBoVeille[idet]{
		cout << format("  %1\n",theme);
	}
	}

	return 0;
	*/
}

function syntaxe(){
	cout << "\nexemples : \nbalise -src $ELA_SRC_GEN/hulk.bal -args -d dossier -uc -patch -cdcoll\n";
	cout << "balise -src $ELA_SRC_GEN/hulk.bal -args -d dossier -sfile -d dir \n\tpour alimenter tous les CDxx/GPxx Hulk avec les fichiers statiques : www/CDxx/TYPEOUVRAGE/fichier.pdf/gif...\n";
	cout << "balise -src $ELA_SRC_GEN/hulk.bal -args -d dossier -sverif -d dir \n\tpour verifier la presence/absence des fichiers statiques (pdf|gif...) dans Hulk\n";
	cout << "balise -src $ELA_SRC_GEN/hulk.bal -args -d dossier -patch -jrpdepuis jj/mm/aaaa\n\tpour traiter toutes les jrps depuis cette date\n";
	
}

var G_dirmiscdata = nothing;

function handleCmdLine(){

	if Arguments.length() == 0{
		cout << "Il faut donner au moins deux arguments : le dossier SGML + une option -uc -patch -cdcoll ...\n";
		syntaxe();
		abort(1);
	}

	G_ela_liv = env("ELA_LIV");
	G_dirmiscdata = filePath(env("ELA_LIV"),"miscdata");
	
	var arg = "";
	var option = "";
	for i = 0 to Arguments.length()-1{
		arg = Arguments[i];
		switch(arg){

			// 18/04/2014 alazreg/sfouzi
			// ajout argument -dirmiscdata
			// sinon en mode debug on ecrase celui de prod car on utilise ELA_LIV
		case "-dirmiscdata":
		case "dirmiscdata":
			G_dirmiscdata = Arguments[i+1];
			break;

		case "-help":
		case "help":
			cout << "Vous avez demande l'aide avec l'option -help\n";
			syntaxe();
			abort(1);
			break;

			// debug alazreg
		case "-noloadsetjrpdz":
		case "noloadsetjrpdz":
			G_bLoadSetJrpDz = false;
			break;
			
			// Pour creer la cdcoll.sgm
		// case "-cdcoll":
		// case "cdcoll":
			// G_bCdcoll = true;
			// break;

			/*
		25/09/2012 MB : l'option jrp sera utilisee 
		pour ecraser le set des jrp "exotiques" 
		*/

		case "jrp":
		case "-jrp":
		case "-exoticjrp":
		case "exoticjrp":
			G_bJrp = true;
			break;
			
		case "mfi":
		case "-mfi":
			G_bMfi = true;
			break;

			// Booleen pour creer les UC, UC Extension, etc.
		case "uc":
		case "-uc":
			G_bUc = true;
			break;
			
			// Pour parser le SGML UTF-8 avec ubalise et generer directement du XML UTF-8 
			// sans avoir à transcoder les entites HTML par la suite
		case "-unicode":
		case "unicode":
			G_bUnicode = true;
			break;

		case "-exalead":
		case "exalead":
			G_bExalead = true;
			break;

		case "-patch":
		case "patch":
			G_bPatchSgml = true;
			break;
			// G_bSfiles
			// 23/10/2012 MB : ajout option -sfiles pour alimenter les CD/GPxx avec les fichiers statiques : www/CDxx/TYPEOUVRAGE/fichier.pdf/gif...
		case "sfiles":
		case "-sfiles":
			G_bSfiles = true;
			break;   

			// G_bSverif
			// 23/10/2012 MB : ajout option -sverif pour verifier la presence/absence dans les CD/GPxx des fichiers statiques : pdf/gif...
			//                 contrairement à l'option -siles, cette option ne copie pas les images dans www/CDxx/TYPEOUVRAGE/fichier.pdf
		case "sverif":
		case "-sverif":
			G_bSverif = true;
			break;

		case "prod":
		case "-prod":
			G_bProd = true;
			G_smode = "prod";
			break;

		case "test":
	case "-test":
		G_bTest = true;
		G_smode = "test";
			G_ela_liv = env("ELA_LIVTEST");
		break;

		case "sbox1":
		case "-sbox1":
		case "sandbox1":
		case "-sandbox1":
			G_bSbox = true;
			G_smode = "sandbox1";
			G_ela_liv = env("ELA_SANDBOX1");
			break;

		case "sbox4":
		case "-sbox4":
		case "sandbox4":
		case "-sandbox4":
			G_bSbox = true;
			G_smode = "sandbox4";
			G_ela_liv = env("ELA_SANDBOX4");
			break;

		case "-d":
		case "-dirin":
		case "dirin":
			G_dirin = Arguments[i+1];
			if (G_dirin == ".")  G_dirin = getCurrentDir();
			break;

		case "-f":
			G_filesgm = Arguments[i+1];
			G_dirin = fileDirName(G_filesgm);
			break;
		}
	}

	// 18/04/2014 alazreg/sfouzi
	// initialiser les chemins miscdata
	// creer le chemin miscdata s'il n'existe pas
	if fileType(G_dirmiscdata) != "dir" system("mkdir -p "+G_dirmiscdata);
	exoticjrpFile = filePath(G_dirmiscdata,"HulkJrpToBuild.set");
	exoticjrpFileMat = filePath(G_dirmiscdata,"HulkJrpToBuildMat.set");
	// 22/07/2013 MB : on ajoute un fichier pour les jrp a nettoyer de mongodb (nettoyees du set HulkJrpToBuild.set car disponible dans la nouvelle liste DZ)
	G_fileIdjrpToClean = filePath(G_dirmiscdata,"G_fileIdjrpToClean.txt");

}

function chargerMapsfromBaseRef() forward;

function initGlobales(){
	// ----------------------------------------------------------
	// Cette fonction initialise les variables globales
	// ----------------------------------------------------------

	// cout << "debug je suis dans la fonction initGlobales\n";
	
	// 16/08/2012 AL
	// Pour renvoyer sur le bon id du recapnew on a besoin de la Map dessous
	// Cette Map est initialisee dans la main du cdcoll_create.bal
	var f_mapIdEtIdRecapnew = filePath(G_ela_liv,"miscdata","idEtIdRecapnew.map");
	cout << format("debug Chargement de la map %1\n",f_mapIdEtIdRecapnew);
	G_mapIdEtIdRecapnew = loadObjectFromFile(f_mapIdEtIdRecapnew);
	if G_mapIdEtIdRecapnew == nothing G_mapIdEtIdRecapnew = Map();


	// Initialiser la Map des thematiques HULK
	initMapIdetThemesBoVeille_hulk();

	G_dirmiscdata = filePath(env("ELA_LIV"),"miscdata");
	
	var temps = timeFormat(timeCurrent(),"%Y-%m-%d_%H-%M-%S");

	G_fileLogName = filePath(env("ELA_TRACE"),"hulk_bal_"+temps+".log");
	G_fileLog = FileStream(G_fileLogName,"w");

	// -----------------------------------------------------------------------
	// 26/02/2013 AL
	// -----------------------------------------------------------------------
	// Probleme de parse rencontres plusieurs fois sur le fichiers aaelnet_txtbloc_200812.optj.sgm
	// je cree par defaut une entrees dans la Map en attendant de trouver une meilleure solution
	// -----------------------------------------------------------------------
	// G_mapFilenameDtd["aaelnet_txtbloc_200812.optj.sgm"] = "txtbloc-optj.dtd";
	// G_mapFilenameDtd["aaelnet_txtantbloc_200812.optj.sgm"] = "txtbloc-optj.dtd";

	// cout << format("%1 Initialisation de la Map des DTD terminee\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));

	G_enteteXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

	// Charger la Map des textes pour pouvoir calculer les TXTBLOC par exemple
	// cout << format("%1 Chargement de la Map des txtdata\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	G_maptxtdata = loadObjectFromFile(filePath(G_dirmiscdata,"maptxt.map"));
	if G_maptxtdata == nothing G_maptxtdata = Map();
	// cout << format("%1 Chargement de la Map des txtdata termine\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));

	// 13/09/2012 MB : charger le set des jrp fournies par DZ
	//    cout << format("%1 Chargement de la Map des jrp fournies par DZ\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	//    G_setAllDZJrp = charger_idlogiques_jrp_DZ(fileTxt_IDs_jrp_DZ);

	// 09/10/2012 AL
	// Pour gagner du temps de chargement en mode debug
	// Je charge le Set s'il est plus recent que la liste fournie par Dalloz

	var dateTxt = fileDate(fileTxt_IDs_jrp_DZ);
	var dateSet = fileDate(fileSet_IDs_jrp_DZ);
	if dateTxt == nothing dateTxt = Time(0);
	if dateSet == nothing dateSet = Time(0);
	
	// 10/03/2014 alazreg
	if (G_bLoadSetJrpDz){
		if timeDiff(dateTxt,dateSet)>=0{
			cout << format("timeDiff\(dateTxt,dateSet\) = %1\n",timeDiff(dateTxt,dateSet));
			cout << format("%1 Mise a jour en cours du Set des jrp fournies par DZ\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
			G_setAllDZJrp = charger_idlogiques_jrp_DZ(fileTxt_IDs_jrp_DZ);
			setObjectInFile(fileSet_IDs_jrp_DZ,G_setAllDZJrp);
			cout << format("%1 Mise a jour terminee du Set des jrp fournies par DZ\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
		}
		// else{
		// cout << format("%1 Chargement du set des jrp fournies par DZ\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
		// G_setAllDZJrp = loadObjectFromFile(fileSet_IDs_jrp_DZ);
		// }
		
		cout << format("debug %1 Chargement du set des jrp fournies par DZ\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
		G_setAllDZJrp = loadObjectFromFile(fileSet_IDs_jrp_DZ);
		cout << format("debug %1 Fin du chargement ========> taille du Set = %2\n",timeFormat(timeCurrent(),"%Y-%m-%d - %T"),G_setAllDZJrp.length());
	}

	// ------------------------------------------------------------------------
	// 03/10/2012 MB : si pas d'appel avec option reconstruction du set G_setIdjrpToBuild 
	// ==> G_setIdjrpToBuild est vide ! on charge celui de ela_tmp_idx
	// cela devrait suffir si on fait appel à make_uc sans les options patch et jrp
	// ------------------------------------------------------------------------
	// 08/02/2013 al/mb
	// ne pas preciser -jrp dans l'appel à ce programme pour cumuler les id jrp EL dans le Set G_setIdjrpToBuild
	// ------------------------------------------------------------------------
	// Appel dans script hulk_liv => set balisecmd = "balise -src $ELA_SRC_GEN/hulk.bal -args -patch -d . -jrp $1"
	// devient set balisecmd = "balise -src $ELA_SRC_GEN/hulk.bal -args -patch -d . $1"

	//    if !G_bJrp {
	// YE 21/01/2014 Ne pas charger le Set.
	// L'objectif est de creer un Set en focntion des jrp a traiter 
	cout << format("debug %1 Chargement de G_setIdjrpToBuild\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	G_setIdjrpToBuild = loadObjectFromFile(exoticjrpFile);
	cout << format("debug %1 Fin Chargement de G_setIdjrpToBuild\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	
	// YE Le 19/02/2014 ne pas appliquer cette methode
	// 18/04/2014 alazreg
	// ajout controle if setExoticjrpFile == nothing
	if G_setIdjrpToBuild == nothing G_setIdjrpToBuild = Set();
	// else cout << format("le set G_setIdjrpToBuild a ete charge à partir de %1\n",exoticjrpFile);
	//    }

	// 03/10/2012 AL/MB
	// Cette map est chargee avec l'ancien set (prod precedente avec options patch et jrp )
	// si hulk.bal est appelee avec des 2 options de toute façon cette map sera ecrasee 
	// par celle chargee apres le patch

	
	// G_mapJrpinfo = chargerMapJrpinfoEL(G_setIdjrpToBuild);
	cout << format("debug %1 Chargement de G_mapJrpinfo\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	G_mapJrpinfo = loadObjectFromFile(filePath(G_dirmiscdata,"jrpinfo.map"));
	cout << format("debug %1 Fin Chargement de G_mapJrpinfo\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	
	if G_mapJrpinfo == nothing G_mapJrpinfo = Map();
	

	
	// 05/11/2012 MB : chargement des maps pour traiter les id logiques dans le comjrp
	//  
	cout << format("%1 Chargement des maps a partir des extractions BR\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));           
	chargerMapsfromBaseRef();
	cout << format("%1 Fin du chargement\n",timeFormat(timeCurrent(),"%Y-%m-%d - %T"));

	// 22/07/2013 MB : initialisation du set des jrp a nettoyer du set HulkJrpToBuild.set car presente dans la nouvelle livraison DZ des ID logiques
	G_setIdjrpToClean = Set();
}


function listfilesgm(dirsgm,regexp){
	var listfilesgm = List();
	var basename = nothing;
	cout << format("function listfilesgm dirsgm = '%1'\n",dirsgm);
	var liste = fileReadDir(dirsgm,true);
	cout << format("function listfilesgm liste = %1 fichiers\n",liste.length());
	for elem in eSort(fileReadDir(dirsgm,true)){
		if elem.rsearch(0,regexp) != nothing{
			basename = fileBaseName(elem);
			if basename.rsearch(0,".patch.sgm$") != nothing continue;
			//if basename == "subscription.sgm" continue;
			//if basename == "cdcoll.sgm" continue;
			//if basename.rsearch(0,".toc.sgm$") != nothing continue;
			//if basename.rsearch(0,".tap.sgm$") != nothing continue;
			//if basename.rsearch(0,".lst\\-[a-z0-9]+.optj.sgm$") != nothing continue;

			
			listfilesgm << elem;
		}
	}

	if G_filesgm != nothing listfilesgm = List(G_filesgm);

	return listfilesgm;
}


function listfilesgmShort(dirsgm,regexp){
	var listfilesgm = List();
	var basename = nothing;
	cout << format("function listfilesgmShort dirsgm = '%1'\n",dirsgm);
	var liste = fileReadDir(dirsgm,true);
	cout << format("function listfilesgmShort liste = %1 fichiers\n",liste.length());
	for elem in eSort(fileReadDir(dirsgm,false)){
		if elem.rsearch(0,regexp) != nothing{
			basename = fileBaseName(elem);
			if basename.rsearch(0,".patch.sgm$") != nothing continue;
			//if basename == "subscription.sgm" continue;
			//if basename == "cdcoll.sgm" continue;
			//if basename.rsearch(0,".toc.sgm$") != nothing continue;
			//if basename.rsearch(0,".tap.sgm$") != nothing continue;
			//if basename.rsearch(0,".lst\\-[a-z0-9]+.optj.sgm$") != nothing continue;

			
			listfilesgm << elem;
		}
	}

	if G_filesgm != nothing listfilesgm = List(G_filesgm);

	return listfilesgm;
}




/*
14/09/2012 MB : pour livraison de tte jrp absente de DZ
function idJrpPresentInDZ(idlogique) : teste si l'idlogique construit est presents dans la liste DZ

*/
function idJrpPresentInDZ(idLogiqueEL){
	return(G_setAllDZJrp.knows(idLogiqueEL));
} 



/*
17/09/2012 MB : pour livraison de tte jrp absente de DZ
function make_exoticJrp : cette fonction lance le programme makeblocs avec l'option exoticjrp
pour creer les blocs jrp à livrer pour Hulk

Appel deplace dans le script : hulk_liv
*/
/*function make_exoticJrp(setFileToProcess){


	
}
*/







function getAbrevJuridicFromBaseRef(idJrp) forward;

// Liste des documents annexes au SGML (gif html pdf ...)
var G_listeDocumentsAnnexes = nothing;

function deplacerAussiLeHtml(file_source,file_cible){
	// mantis 4420
	// cette fonction deplace le fichier .htm en meme temps que le .gif .png
	// exemple :
	// mv www/html/icons/x3m330001.png www/EN23I/DPFORM2/X3M001/x3m330001.png
	// penser aussi a mv www/html/icons/x3m330001.htm www/EN23I/DPFORM2/X3M001/x3m330001.htm

	var fic_htm = file_source.extract(0,file_source.length()-4)+".htm";
	if fileDate(fic_htm) != nothing{
		var fic_htm_cible = filePath(fileDirName(file_cible),fileBaseName(fic_htm));
		fileRename(fic_htm,fic_htm_cible);
	}
	
	return 0;
}

function ventilerLesDocumentsAnnexes_2(racine,giracine,idracine,codeMatiere,typeOuvrage,basenameSgm){
	// -------------------------------------------------------------------------
	// 03/12/2012 AL
	// -------------------------------------------------------------------------
	// Cette fonction ventille dans les dossiers annexes www/MATIERE/TYPEOUGVRAGE/OUVRAGE/ 
	// les documents annexes qui se trouvent dans les dossiers www/pdf et www/html/icons
	// Exemples :
	// www/html/icons/z2215eq1.* seront deplaces dans le dossier www/CD02/ETD/Z2215/
	// www/pdf/z2215.pdf seront deplaces dans le dossier www/CD02/ETD/Z2215/
	//
	// -------------------------------------------------------------------------
	// 12/03/2013 alazreg
	// -------------------------------------------------------------------------
	// bug corrige
	// Des documents annexes etaient mal classes
	// Exemples :
	//-rw-rw-r-- 1 alazreg adoc  8591 Mar 11 16:49 www/SOURCES/DOCAM/13E/y0013eq1.gif
	//-rw-rw-r-- 1 alazreg adoc 11040 Mar 11 16:49 www/SOURCES/DOCAM/13E/y0013eq2.gif
	//...
	//...
	//-rw-rw-r-- 1 alazreg adoc  5765 Mar 11 16:52 www/SOURCES/DOCAM/9E/z6099eq9.gif
	//-rw-rw-r-- 1 alazreg adoc 15394 Mar 11 16:52 www/SOURCES/DOCAM/9E/z9029eq1.gif
	//-rw-rw-r-- 1 alazreg adoc  4577 Mar 11 16:52 www/SOURCES/DOCAM/9E/z9039eq1.gif
	// -------------------------------------------------------------------------
	
	
	var ela_liv = G_ela_liv;
	var dir_www = filePath(ela_liv,"www");
	var dir_www_pdf = filePath(dir_www,"pdf");
	var dir_www_html_icons = filePath(dir_www,"html","icons");
	
	var filenamelog = filePath(ela_liv,"ventilerLesDocumentsAnnexes.txt");
	var filelog = FileStream(filenamelog,"w");
	
	// Construire le chemin du dossier annexe s'il n'existe pas
	// Pour les etudes on ne prend pas la refonte pour constituer le chemin
	var dir_www_matiere_typeouvrage_ouvrage = filePath(dir_www,codeMatiere,typeOuvrage,idracine);
	if giracine == "ETD-OPTJ" dir_www_matiere_typeouvrage_ouvrage = filePath(dir_www,codeMatiere,typeOuvrage,idracine.explode("-")[0]);

	
	// 03/10/2013 alazreg
	// Il faut que le chemin des documents annexes soit le meme que UC
	// pour les bloc utiliser la fonction getOuvrageId appelee aussi dans la fonction make_uc
	if (Set("FP-OPTJ","QR-OPTJ","FPRO-OPTJ").knows(giracine) && Set("EN23I","GP23","GP25","GP59","GP66","GP67","GP68","GP69","GP74").knows(codeMatiere)){
		//	idelem = getOuvrageId(codeMatiere,giracine, idracine,basenameSgm);
		dir_www_matiere_typeouvrage_ouvrage = filePath(dir_www,codeMatiere,typeOuvrage,getOuvrageId(codeMatiere,giracine, idracine,basenameSgm));
	} 

	// cout << format("dir_www_matiere_typeouvrage_ouvrage = %1\n",dir_www_matiere_typeouvrage_ouvrage);

	// --------------------------------------------------------------------
	// Creer le dossier www/MATIERE/TYPEOUVRAGE/OUVRAGE/ s'il n'existe pas
	// --------------------------------------------------------------------
	// 24/04/2013 MB : fileDate == noting ?
	// je corrige le test car il genere des erreurs sgml
	// if fileDate(dir_www_matiere_typeouvrage_ouvrage) {
	if fileDate(dir_www_matiere_typeouvrage_ouvrage) == nothing {		
		var retourSystem = system(format("mkdir -p %1 >& /dev/null",dir_www_matiere_typeouvrage_ouvrage));
		//cout << format("retourSystem = %1\n",retourSystem);
	}

	// --------------------------------------------------------------------
	// 06/12/2012 al
	// Parcourir les elements ill/fig pour copier les fichiers illustrations appeles
	// --------------------------------------------------------------------
	var figFilename = nothing;
	var systemCmd = nothing;

	var filebasename = nothing;
	var filebasenameUpper = nothing;
	var file_cible = nothing;
	var file_source = nothing;

	// 19/08/2016 SF mantis 13715 LAB15 : copie du PDF du code NAF dans le dossier ./www/CD15/CORNAFCC/NAF2008
	if giracine == "CORNAFCC-OPTJ" {
		// cout << format("####### debugsf copie PDF NAF");
		var ficpdf = "NAF.pdf";
		file_source = filePath("/mac.public/_DOCUMENTS_COMMUNS/codeNAF", ficpdf);
		file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,ficpdf);
		// cout << format("file_source = %1\n", file_source);
		// cout << format("file_cible = %1\n", file_cible);
		fileCopy(file_source,file_cible);
	}
	
	// 13/09/2016 SF mantis 13716 LAB15 : copie des PDF des cc dans le dossier ./www/CD15/CCOL/
	if giracine == "CCOL-OPTJ" {
		// cout << format("**************  je suis dans CCOL-OPTJ ***********");
		// var ficpdf = format("%1.pdf",idracine.transcript(UpperCase));
		var ficpdf = format("%1.pdf",idracine);
		if (hasAttr(racine,"IDCC") && attr["IDCC",racine] != "") {
			var idcc = attr["IDCC",racine];
			// ficpdf = format("%1_%2.pdf",idcc,idracine.transcript(UpperCase));
			ficpdf = format("%1_%2.pdf",idcc,idracine);
		}
	
		// 19/01/2017 alazreg CCOL PDF des archives
		// Les ID des CCOL archives sont en majuscules ( exemple : ID="CC545ARCHIVE2016")
		// Les PDF des archives sont nommés *archiveAAAA.pdf (exemple : 7009_CC545archive2016.pdf)
		// Il faut transcoder la chaine 'ARCHIVE' en 'archive'
		ficpdf = ficpdf.replace(0,"ARCHIVE","archive");
	
		// var file_source = filePath("/mac.public/_DOCUMENTS_COMMUNS/CCOL/PDF",ficpdf);
		var file_source = filePath(dir_www_pdf,ficpdf);
		file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,ficpdf);
		// cout << format("debug file_source = %1\n", file_source);
		// cout << format("debug file_cible  = %1\n", file_cible);
		fileCopy(file_source,file_cible);
	}
	
	// --------------------------------------------------------------------
	// 16/12/2015 alazreg mantis 12247
	// ajout cas des LISTE/ITEM/@REFID qui pointe vers un PDF
	// Exemple :
	// - Calendriers des assemblees pour le CD05 dp05_lst-calendrier.optj.sgm
	// - Liste des bulletins speciaux pour tous les CD dpxx_lst-bullspe.optj.sgm
	// --------------------------------------------------------------------
	if giracine == "LISTE"{
		for item in searchElemNodes(racine,"ITEM","REFID",nothing){
			var ficpdf = "";
			if attr["REFID",item].search(0,".pdf") > 0 ficpdf = attr["REFID",item];
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,ficpdf);
			file_source = filePath(dir_www_pdf,ficpdf);
			fileCopy(file_source,file_cible);
		}
	}

	for fig in searchElemNodes(racine,"FIG","FICHIER",nothing){
		// --------------------------------------------------------------------
		// IMPORTANT : commencer par copier depuis le dossier www/pdf qui contient potentiellement des "faux gif" 
		// qui sont en realite des pdf
		// --------------------------------------------------------------------
		//cout << format("DEBUG /*19/09/2013*/ -----> attr[FICHIER,fig] = %1\n",echo(fig));
		if (attr["FICHIER",fig] == "") continue;

		// --------------------------------------------------------------------
		// On supprimer une eventuelle extension .pdf .gif .png qui fait doublon avec l'attribut fig@format
		// --------------------------------------------------------------------
		filebasename = attr["FICHIER",fig].explode(".")[0];

		// --------------------------------------------------------------------
		// On ajoute l'extension qui est indiquee dans l'attribut fig@format
		// Si attribut fig@format absent on ajoute .pdf par defaut
		// --------------------------------------------------------------------
		if hasAttr(fig,"FORMAT") filebasename << "."+attr["FORMAT",fig];
		else filebasename << ".pdf";

		// --------------------------------------------------------------------
		// On passe tout en minuscule car les illustrations sont en minuscule (normalement)
		// --------------------------------------------------------------------
		filebasename = filebasename.transcript(LowerCase);

		// --------------------------------------------------------------------
		// IMPORTANT :
		// Le dossier www/pdf contient parfois des doublons errones de ww/html/icons
		// rechercher dans le dossier www/pdf en premier
		// --------------------------------------------------------------------
		file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
		file_source = filePath(dir_www_pdf,filebasename);
		
		//cout << format("\n\nDEBUG /*19/09/2013*/ -----> file_source=%1\tfile_cible=%2\n",file_source,file_cible);

		
		if (fileDate(file_source) != nothing) {			
			// 18/09/2015 SF mantis 11702
			// je remplace la fonction fileRename par fileCopy dans le cas des sources
			// les illustrations ne sont pas copiees dans le dossier cible des textes
			// (www/SOURCES/TXT/ELNETTXTBLOC??????) 
			// car la fonction fileRename deplace les illustrations dans le dossier cible des textes anterieurs
			// (www/SOURCES/TXT/ELNETTXTANTBLOC??????) 
			// fileRename(file_source,file_cible);
			if (codeMatiere == "SOURCES") fileCopy(file_source,file_cible);
			else fileRename(file_source,file_cible);
		}
		
		// --------------------------------------------------------------------
		// rechercher dans le dossier www/html/icons en second
		// Cela permet d'ecraser les pdf errones s'ils existent
		// --------------------------------------------------------------------
		file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
		file_source = filePath(dir_www_html_icons,filebasename);
		if (fileDate(file_source) != nothing){
			// 18/09/2015 SF mantis 11702
			// je remplace la fonction fileRename par fileCopy dans le cas des sources
			// les illustrations ne sont pas copiees dans le dossier cible des textes
			// (www/SOURCES/TXT/ELNETTXTBLOC??????) 
			// car la fonction fileRename deplace les illustrations dans le dossier cible des textes anterieurs
			// (www/SOURCES/TXT/ELNETTXTANTBLOC??????) 
			// fileRename(file_source,file_cible);
			if (codeMatiere == "SOURCES") fileCopy(file_source,file_cible);
			else fileRename(file_source,file_cible);
			// 04/04/2014 alazreg
			// mantis 4420
			deplacerAussiLeHtml(file_source,file_cible);
		}
	}
	
	// --------------------------------------------------------------------
	// Cas particuliers
	// Dans le cas des gp20/qr on prend les elements <GP20 IDFICHE>
	// --------------------------------------------------------------------
	if (giracine == "QR-OPTJ" && codeMatiere == "GP20"){
		for gp20 in searchElemNodes(racine,"GP20","IDFICHE",nothing){
			filebasename = attr["IDFICHE",gp20].transcript(LowerCase)+".pdf";

			// --------------------------------------------------------------------
			// IMPORTANT :
			// Le dossier www/pdf contient parfois des doublons errones de ww/html/icons
			// rechercher dans le dossier www/pdf en premier
			// --------------------------------------------------------------------
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
			file_source = filePath(dir_www_pdf,filebasename);
			if (fileDate(file_source) != nothing) fileRename(file_source,file_cible);
			
			// --------------------------------------------------------------------
			// rechercher dans le dossier www/html/icons en second
			// Cela permet d'ecraser les pdf errones s'ils existent
			// --------------------------------------------------------------------
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
			file_source = filePath(dir_www_html_icons,filebasename);
			if (fileDate(file_source) != nothing){
				fileRename(file_source,file_cible);
				deplacerAussiLeHtml(file_source,file_cible);
			}
		}
	}

	// --------------------------------------------------------------------
	// 08/04/2015 alazreg/sfouzi mantis 9698
	// Cas particuliers
	// Dans le cas des gp76 gp95/dpform2 on prend les elements <FORMTx ID>
	// --------------------------------------------------------------------
	if (giracine == "DPFORM2-OPTJ" && Set("GP76","GP95").knows(codeMatiere)){
		for formt in searchElemNodes(racine,List("FORMT1","FORMT2","FORMT3","FORMT4","FORMT5","FORMT6","FORMT7"),"ID",nothing){
			filebasename = attr["ID",formt].transcript(LowerCase)+".pdf";

			// --------------------------------------------------------------------
			// IMPORTANT :
			// Le dossier www/pdf contient parfois des doublons errones de ww/html/icons
			// rechercher dans le dossier www/pdf en premier
			// --------------------------------------------------------------------
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
			file_source = filePath(dir_www_pdf,filebasename);
			if (fileDate(file_source) != nothing) fileRename(file_source,file_cible);
			
			// --------------------------------------------------------------------
			// rechercher dans le dossier www/html/icons en second
			// Cela permet d'ecraser les pdf errones s'ils existent
			// --------------------------------------------------------------------
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
			file_source = filePath(dir_www_html_icons,filebasename);
			if (fileDate(file_source) != nothing){
				fileRename(file_source,file_cible);
				deplacerAussiLeHtml(file_source,file_cible);
			}
		}
	}

	// --------------------------------------------------------------------
	// S'il reste des fichiers dans www/pdf/ et www/html/icons/
	// il faut tenter de les ventiler s'ils correspondent à l'ouvrage
	// --------------------------------------------------------------------
	// jj/mm/aaaa alazreg
	// mantis xxx
	// attention au cas du gp20/formules qui appellent des pdf qui commencent avec un nom d'un autre encart
	// --------------------------------------------------------------------
	// 03/11/2014 alazreg
	// mantis 9085
	// reactivation pour prendre les pdf des syntheses dp15
	// --------------------------------------------------------------------

	var liste_fic_www_pdf = fileReadDir(dir_www_pdf,true);
	var liste_fic_www_html_icons = fileReadDir(dir_www_html_icons,true);

	var idouvrage = idracine.explode("-")[0];

	for filename in liste_fic_www_pdf + liste_fic_www_html_icons{
		filebasename = fileBaseName(filename);
		filebasenameUpper = filebasename.transcript(UpperCase);
		// je deplace seulement les pdf qui correspondent exactement au nom de l'ouvrage
		// if (filebasenameUpper.search(0,idouvrage) == 0)
		if (filebasenameUpper.explode(".")[0] == idouvrage){
			file_cible = filePath(dir_www_matiere_typeouvrage_ouvrage,filebasename);
			file_source = filePath(filename);
			if (fileDate(file_source) != nothing) fileRename(file_source,file_cible);
		}
	}
	
	close(filelog);
	
	
	return 0;
}


function getTitreRacine(racine,giracine,idracine){
	var stitre = "Titre vide "+idracine;

	var titreElem = enclosed(racine,"TI");
	if titreElem != nothing stitre = content(titreElem);

	switch(giracine){
	case "TXTBLOC-OPTJ":
		stitre = "Textes";
		break;
	case "TXT-OPTJ":
		titreElem = enclosed(racine,"DOCTITLE");
		if titreElem != nothing stitre = content(titreElem);
		break;
	case "FP-OPTJ":
		titreElem = enclosed(racine,"TIFP");
		if titreElem != nothing stitre = content(titreElem);
		break;
	case "QR-OPTJ":
		titreElem = child(racine,"TIQR");
		if titreElem != nothing stitre = content(titreElem);
		break;
	case "DPFORM-OPTJ":
	case "DPFORM2-OPTJ":
		titreElem = enclosed(racine,"TI0");
		if titreElem != nothing stitre = content(titreElem);
		break;
	}
	
	return stitre;
}

function getCodeMatiere(node) forward;

var G_map_id_logique_jrp_dalloz = nothing;

function get_map_id_logique_jrp_dalloz(){
	// cette fonction charge les deux map de correspondance iddpm / id logique jrp dalloz
	// elle retourne une map qui contient la somme des deux autres
	
	// /usr/local/ela/tmp-idx/jrp_id_el_id_logique_dz_depuis_br.map
	// /usr/local/ela/tmp-idx/jrp_id_el_id_logique_dz_depuis_jrpinfo.map
	// cout << "debug function get_map_id_logique_jrp_dalloz\n";
	var map = Map();

	// 1. on initalise avec la map issue de jrpinfo
	var map_jrp_id_el_id_logique_dz_depuis_jrpinfo = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"jrp_id_el_id_logique_dz_depuis_jrpinfo.map"));
	if isaMap(map_jrp_id_el_id_logique_dz_depuis_jrpinfo){
		for cle,set in map_jrp_id_el_id_logique_dz_depuis_jrpinfo{
			// cle_upper_case = cle.transcript(UpperCase);
			// if !map.knows(cle_upper_case) map[cle_upper_case] = set;
			// on tri le Set pour garder uniquement la premiere valeur
			map[cle.transcript(UpperCase)] = eSort(set)[0];
		}
	}

	// on vide les variables map pour economiser de la memoire
	// cout << format("debug function get_map_id_logique_jrp_dalloz map_jrp_id_el_id_logique_dz_depuis_jrpinfo contient %1 cles\n",map_jrp_id_el_id_logique_dz_depuis_jrpinfo.length());
	map_jrp_id_el_id_logique_dz_depuis_jrpinfo = nothing;

	// 2. les informaions de la br sont prioritaires sur celles de jrpinfo
	// on ecrase et on complete avec la map issue de la br
	var map_jrp_id_el_id_logique_dz_depuis_br = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"jrp_id_el_id_logique_dz_depuis_br.map"));
	
	// var cle_upper_case = nothing;
	if isaMap(map_jrp_id_el_id_logique_dz_depuis_br){
		for cle,set in map_jrp_id_el_id_logique_dz_depuis_br map[cle.transcript(UpperCase)] = eSort(set)[0];
	}
	
	// cout << format("debug function get_map_id_logique_jrp_dalloz map_jrp_id_el_id_logique_dz_depuis_br contient %1 cles\n",map_jrp_id_el_id_logique_dz_depuis_br.length());
	map_jrp_id_el_id_logique_dz_depuis_br = nothing;
	
	// 4. on retourne la map fusionnee
	// cout << format("debug function get_map_id_logique_jrp_dalloz map contient %1 cles\n",map.length());
	return map;
}

function get_idjrpbloc(idref){
	var idjrpbloc = "ELNETDECISBLOCAAAAMM";
	var idref_lower = idref.transcript(LowerCase);
	if G_mapJrpinfo.knows(idref_lower){
		var annee =  G_mapJrpinfo[idref_lower].annee;
		var mois =  G_mapJrpinfo[idref_lower].mois;
		idjrpbloc = format("ELNETDECISBLOC%1%2",annee,mois);
	}
	return idjrpbloc;
}

function get_uri_ouvrage_jrp_el(idref){
	var idjrpbloc = get_idjrpbloc(idref);
	var uri_ouvrage_jrp = format("EL/SOURCES/JRP/%1/%2",idjrpbloc,idref);
	return uri_ouvrage_jrp;
}

function resoudre_id_logique_jrp_dalloz(racine){
	var idracine = attr["ID",racine];
	
	// on initialise la map des jrp dalloz au premier appel
	if G_map_id_logique_jrp_dalloz == nothing G_map_id_logique_jrp_dalloz = get_map_id_logique_jrp_dalloz();
	
	// var iddpmList = eSort(G_map_id_logique_jrp_dalloz);
	// cout << format("debug iddpmList contient %1 cles\n",iddpmList.length());
	// var elemList = searchElemNodes(racine,nothing,"IDREF",eSort(G_map_id_logique_jrp_dalloz));
	
	var listjrpElems_idref = List("JRP","JCC","JTCF","JCAP","JCM","JCSOC","JCCRIM","JCCOM","JCC0","JCC1","JCC2","JCC3","JCR","JCA","JDPH","JTGI","JTCOM","JTI","JTC","JCE","JCAA","JTASS","JTA","JTCE","JEDH","JCAUT");
	
	// on ajoute a la liste certains elements specifiques
	listjrpElems_idref << "ID3"; // pour les index jrp
	
	// for iddpm in iddpmList{
	// var elemList = searchElemNodes(racine,listjrpElems_idref,"IDREF",iddpm);
	var elemList = searchElemNodes(racine,listjrpElems_idref,"IDREF");
	// if elemList.length() == 0 continue;
	// cout << format("debug elemList contient %1 elements\n",elemList.length());
	// cout << format("debug elemList contient ceci \n'%1'\n",elemList);
	
	for elem in elemList{
		var idref = attr["IDREF",elem].transcript(UpperCase);
		
		if not G_map_id_logique_jrp_dalloz.knows(idref){
			// ajouter iddpm au set des jrp EL a livrer
			G_setIdjrpToBuildMat << idref;
			G_setIdjrpToBuild << idref;
			continue;
		}
		// supprimer iddpm des set des jrp EL a livrer
		if G_setIdjrpToBuildMat.knows(idref) G_setIdjrpToBuildMat.remove(idref);
		if G_setIdjrpToBuild.knows(idref){		
			G_setIdjrpToBuild.remove(idref);			
		}
		
		// remplacer iddpm par id logique de la jrp dalloz
		attr["IDREF",elem] = G_map_id_logique_jrp_dalloz[idref];

		// mettre a jour les Set de jrp EL a nettoyer
		// a ameliorer : verifier dans la derniere livraison si la jrp EL a ete livree
		// a ameliorer : ajouter le chemin EL/SOURCES/JRP/ELNETDECISBLOCAAAAMM/
		// exemple fictif : EL/SOURCES/JRP/ELNETDECISBLOC200908/A12345
		G_setIdjrpToClean << get_uri_ouvrage_jrp_el(idref);
		// G_setIdjrpToClean << idref;
	}
	// }
	
	// cout << "debug je quitte function resoudre_id_logique_jrp_dalloz\n";
	return 0;
}

function creer_delivery_suppressions_auto(G_fileIdjrpToClean,G_setIdjrpToClean){
	// cette fonction cree un fichier hulk-delivery pour la suppression automatique
	// des jrp el livrees au workflow
	// sinon on aura des doublon avec les jrp dalloz
	
	var dirout = fileDirName(G_fileIdjrpToClean);
	var xmlout = filePath(dirout,"hulk-delivery-suppression-auto.xml");
	// cout << format("debug function creer_delivery_suppressions_auto xmlout = '%1'\n",xmlout);
	
	var fileout = FileStream(xmlout,"w");
	if fileout == nothing{
		cout << format("ERREUR GENERATION FICHIER %1\n",xmlout);
		return 1;
	}
	
	// modele de delivery.xml
	// <delivery date="2014-04-14T15:35:00" desc="suppression ouvrages EL" importer="alazreg">
	// <del-entity uri="EL/SOURCES/CM" scope="entity" />
	// </delivery>

	var ladate = timeFormat(timeCurrent(),"%Y-%m-%dT%T");
	var user = systemInfo("LoginName");

	fileout << format("<delivery date=\"%1\" desc=\"suppression auto ouvrages JRP EL\" importer=\"%2\">",ladate,user);
	
	fileout << format("<!-- %1 ouvrages a supprimer -->",G_setIdjrpToClean.length());
	
	// parcourir le set des iddpm pour creer le fichier delivery.xml
	for elem in eSort(G_setIdjrpToClean){
		fileout << format("<del-entity uri=\"%1\" scope=\"entity\" />",elem);
	}
	
	fileout << "</delivery>";
	
	close(fileout);
	
	return 0;
}

// 11/02/2015 SF / MB mantis 10005
// decliner le code de l'expropriation en ancien (CEXP) et nouveau (NEXP)
function modifListCodes(racine) {
	var listItemCEUP = searchElemNodes(racine,"ITEM","REFID","CEUP");
	if listItemCEUP.length() == 0 return racine;
	var itemCEUP = listItemCEUP[0];
	// cout << format("itemCEUP AVANT :\n%1\n", itemCEUP);
	var titleCEUP = child(itemCEUP,"TITLE");
	// cout << format("titleCEUP = %1\n", titleCEUP);
	changeContent(child(titleCEUP), "Code de l'expropriation pour cause d'utilit&eacute; publique (ancien)");
	// cout << format("itemCEUP APRES :\n%1\n", itemCEUP);
	
	var doc = newCoreDocument();
	var itemNEXP = Node(doc,"ITEM",Node(doc,"TITLE",Node(doc,"#CDATA","Code de l'expropriation pour cause d'utilit&eacute; publique")));
	addAttr(itemNEXP,"REFID","NEXP");
	
	// 13/02/2015 SF inverser l'ordre des 2 codes ==> NEXP avant CEXP
	// insertSubTree(parent(itemCEUP),rank(itemCEUP)+1,itemNEXP);	
	insertSubTree(parent(itemCEUP),rank(itemCEUP),itemNEXP);	
	
	return racine;
}

function ajouterCodeFASC(racine, matiere) {
	// cout << format("******** debugsf 14/10/2015 je suis dans la fct ajouterCodeFASC\n");
	var codePrecedent = "CCV";
	if (matiere == "CD01") {
		// codePrecedent = "CPRF";
		codePrecedent = "LPF";
	}
	
	// cout << format("******** debugsf 14/10/2015 codePrecedent = %1\n", codePrecedent);
	var listItemCodePrecedent = searchElemNodes(racine,"ITEM","REFID",codePrecedent);
	// cout << format("******** debugsf 14/10/2015 listItemCodePrecedent.length() = %1\n", listItemCodePrecedent.length());
	if listItemCodePrecedent.length() == 0 return racine;
	var itemCodePrecedent = listItemCodePrecedent[0];
	// cout << format("itemCodePrecedent AVANT :\n%1\n", itemCodePrecedent);
	// var titleCCIV = child(itemCodePrecedent,"TITLE");
	// cout << format("titleCCIV = %1\n", titleCCIV);
	// changeContent(child(titleCCIV), "Code de l'expropriation pour cause d'utilit&eacute; publique (ancien)");
	// cout << format("itemCodePrecedent APRES :\n%1\n", itemCodePrecedent);
	
	var doc = newCoreDocument();
	var itemFASC = Node(doc,"ITEM",Node(doc,"TITLE",Node(doc,"#CDATA","Projet de r&eacute;forme - Droits des obligations")));
	addAttr(itemFASC,"REFID","FASC");
	
	// 13/02/2015 SF inverser l'ordre des 2 codes ==> NEXP avant CEXP
	// insertSubTree(parent(itemCodePrecedent),rank(itemCodePrecedent)+1,itemFASC);	
	// insertSubTree(parent(itemCodePrecedent),rank(itemCodePrecedent),itemFASC);
	// cout << format("********** debugsf 14/10/2015 itemFASC = %1\n", itemFASC);
	insertSubTree(parent(itemCodePrecedent),rank(itemCodePrecedent)+1,itemFASC);
	// cout << format("********** debugsf 14/10/2015 racine ==> %1\n", racine);
	
	return racine;
}

function make_patch(listfilesgm){
	// Cette fonction realise les operations suivantes :
	// - patche les UA avec les FDOC
	//   Exemple : <DEBUA M="{'classement':'FDOC/EL/CD14/ETD'}">
	//
	// - patche les IDREF des XACODE avec des ID logiques
	//   Exemple : <XACODE CODE="CASF" IDREF="CASF64" NUM="121-7" TYPE="L">
	//             devient
	//             <XACODE CODE="CASF" IDREF="" NUM="121-7" TYPE="L">

	// cout << format("debug Je suis dans la fonction make_patch. G_bJrp = %1\n",G_bJrp);
	
	var ret,racine,giracine,dtd,doc,basename;
	var fileout = nothing;
	var fileoutname = nothing;

	var sIdLogique = nothing;
	var sFdoc = nothing;
	var sMatiere,giNode,idracine,sId;

	var sCode,sType,sNum,sNiv,sExnum,sSaisie, sAutre;
	var typeOuvrage = nothing;
	
	var liste_jrp_attribut_source_inconnu =List();
	var liste_jrp_sans_attribut_source = List();
	var liste_cas_non_resolus = List();
	var set_id_absents_de_DZ = Set(); 
	
	var fichiers_icons = List();
	
	var pdf_path = filePath(env("ELA_LIV"),"www","pdf");    
	var icon_path = filePath(env("ELA_LIV"),"www","html","icons"); 
	var path = pdf_path;
	var fileToCopy = "";

	var titreOuvrage = nothing;

	iSort(listfilesgm);
	// cout << format("DEBUG listfilesgm = %1 a traiter\n",listfilesgm.length());
	// for filesgm in listfilesgm cout << format("DEBUG filesgm = %1\n",filesgm);

	for filesgm in listfilesgm{
		basename = fileBaseName(filesgm);
		//cout << ".";
		flush(cout);
		if basename.rsearch(0,G_regexpToc) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}
		if Set("cdcoll.sgm","subscription.sgm","type_source_lpveille.sgm").knows(basename){
			continue;
		}
		
		// Test avec quelques fichiers SGML seulement
		//if !Set("y4001.body.sgm").knows(basename) continue;
		
		//cout << format("%1\n",filesgm);
		dtd = nothing;
		var dtd_basename = getDtdBaseNameFromFileName(filesgm);
		// dtd = filePath(env("ELA_DTD"),getDtdBaseNameFromFileName(filesgm));
		if  dtd_basename != nothing dtd = filePath(env("ELA_DTD"),dtd_basename);
		// cout << format("INFO DTD : %1 => %2\n",filesgm,dtd);

		if dtd == nothing{
			set_exitval(2);
			cout << format("ERREUR DTD = NOTHING pour %1\n",filesgm);
			continue;
		}

		// cout << format("debug traitement de %1 %2\n",filesgm,dtd);

		// 18/02/2013 AL
		// Je suis tombe sur 2 cas aaelnet_txtbloc_20812.optj.sgm et 9G.optj.sgm qui ne parsaient pas
		// a cause de dtd absente dans dtdfinder.txt
		// Si la dtd n'existe pas alors passer au fichier suivant
		if fileDate(dtd) == nothing{
			set_exitval(2);
			cout << format("ERREUR DTD n'existe pas : %1\n",dtd);
			continue;
		}

		// 23/05/2012 AL
		// Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
		// Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;

		if G_bUnicode{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		else{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
			// 04/07/2012 MB : en deplacant les traitements du patch apres utf8 ==> pb avec les entites num > 255
			// solution : NMTOKEN remplaces par CDATA
		}
		
		if ret.status != 0{
			G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,fileBaseName(dtd));
			// cout << format("DEBUG ERREUR PARSE %1 %2\n", filesgm,dtd);
			continue;
		}
		
		// G_fileLog << format("DEBUG PARSE OK %1 %2\n",filesgm,dtd);
		// cout << format("DEBUG PARSE OK %1 %2\n",filesgm,dtd);
		
		doc = ret.document;
		racine = root(doc);

		giracine = GI(racine);

		// 09/05/2014 YE/MB : on ajoute le patch des IDREF des NOMF des et en majuscule suite pb pro ==> mantis 7187
		for elem in searchElemNodes(racine,"NOMF","IDREF") attr["IDREF",elem] = attr["IDREF",elem].transcript(UpperCase);
		
		titreOuvrage = getTitreElem(racine,giracine,"");

		sMatiere = attr["MATIERE",racine];
		// 24/10/2012 MB : pour les txt/jrp l'attribut MATIERE = "ELNET", il faudra mettre "SOURCES" à la place
		if sMatiere == "ELNET" sMatiere = "SOURCES";
		g_codeMatiere = sMatiere;

		// MB 13/06/2012 : la racine des listes n'a pas d'ID (../data/dp46_lst-code.optj.sgm, ...)
		// ajouter un test
		if hasAttr(racine,"ID") idracine = attr["ID",racine];
		// --------------------------------------------------------------
		// 17/06/2015 alazreg
		// correction bug pour classer les documents annexes a FPBLOC[0-9]-ROOT
		// il faut les classer dans un dossier nomme www/$MATIERE/FP/FPBLOC[0-9] sans le -"ROOT"
		// --------------------------------------------------------------
		if idracine.search(0,"-ROOT") >0 idracine=idracine.replace(0,"-ROOT","").replace(0,"-","");

		// MB 10/10/2012 : dans les listes des codes, 
		//                  les IDs des codes EL doivent être transformes en leurs equivalents codes DalloZ !
		//if giracine == "LISTE" idracine = attr["KEY",racine];
		if giracine == "LISTE" {
			idracine = attr["KEY",racine];
			// si le fichier est en cours est une liste de codes : ex. : dp02_lst-code.optj.sgm
			// ==> le traiter ...
			if basename.search(0,"_lst-code") > 0 {
				// ... remplacer le code dans les elets item
				//cout << format("Insertion des IDs logiques dans la liste de code : %1\n",basename);
				
				// 19/03/2015 alazreg/sfouzi
				// mantis 10428 CNEXP NEXP
				// racine = modifListCodes(racine);
				
				// 14/10/2015 SF mantis 11888 --> 11897
				// liste code ajouter Projet de réforme - Droits des obligations					
				// ----------------------------------------------
				// 09/02/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-64
				// ne plus generer la ligne qui renvoie vers FASC
				// ----------------------------------------------
				// var setMatiereFASC = Set("CD01","CD05","CD11","CD17","CD13","CD09","CD22","CD16","CD26","CD18");
				// if setMatiereFASC.knows(sMatiere) {
					// cout << format("********** debugsf 14/10/2015 matiere = %1\n", sMatiere);
					// racine = ajouterCodeFASC(racine, sMatiere);
					// cout << format("racine ====>  %1 \n",racine);
				// }
				
				for item in searchElemNodes(racine,"ITEM","REFID"){
					if mapCorrespCodesEL_DZ.knows(attr["REFID",item]) {
						
						// 16/04/2013 MB : certains codes EL ne doivent pa etre patches
						//                lors du make_patch sur dpxx_lst-code.optj.sgm, les codes suivants ne seront pas patches
						// var setCodesANePasPatcher = Set("CDC","CTM","CMP","CPCA","CRA","CCVA","CUA","CM","CPMIV");
						if !setCodesANePasPatcher.knows(attr["REFID",item])
						attr["REFID",item] = mapCorrespCodesEL_DZ[attr["REFID",item]];
						// 01/05/2013 MB : s'il fait partie des codes EL == il faut le livrer s'il ne l'est pas deja
						// 02/05/2013 MB : pb car il faut repasser le patch sur ces codes vu qu'ils sont ajoutes lors de ce ... patch (sic!)
						//                 je desactive donc cette partie, pour le moment les codes doivent etre livres avec les sources ou avec l'option bofip
						
					}
					//-----------------------------------------------------------------------------------
					// 18/08/2014 SF mantis 2693
					// changer l’intitulé du code minier pour hulk
					// remplacer "Code minier (extraits)" par "Code minier"
					//-----------------------------------------------------------------------------------
					if attr["REFID",item] == "CMIN" {
						var title = child(item,"TITLE");
						changeContent(child(title),content(title).replace(0," \\(extraits\\)",""));							
					}
				}
			}
		}
		// -----------------------------------------------------------------------
		// Patch du TYPEOUVRAGE sur la racine
		// Penser a reporter le TYPEOUVRAGE sur les books de la cdcoll
		// -----------------------------------------------------------------------
		typeOuvrage = getTypeOuvrage(sMatiere,giracine, idracine,basename);
		// 15/06/2016 alazreg
		if attr["MATIERE",racine] == "GP95" && hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG" typeOuvrage = "FREG";
		addAttr(racine,"TYPEOUVRAGE",typeOuvrage);
		//cout << format("TYPEOUVRAGE = %1\n",attr["TYPEOUVRAGE",racine]);

		
		// -----------------------------------------------------------------------
		// 03/12/2012 al
		// Ventiler les dcuments annexes à l'ouvrage SGML (pdf gif html png ...)
		// -----------------------------------------------------------------------
		//ventilerLesDocumentsAnnexes(racine,idracine,sMatiere,typeOuvrage);
		// 12/12/2012 MB : ajout codeMatiere dans les parametres (suite erreur à la compil)
		// 10/01/2013 al
		// debug pour mettre a jour les illustrations de certaines matieres
		//if Set("GP20","GP25","GP59").knows(sMatiere) 


		// -----------------------------------------------------------------------
		// 12/03/2013 alazreg
		// La fonctionne ventilerLesDocumentsAnnexes est bugguee
		// Remplacer par ventilerLesDocumentsAnnexes_2
		// -----------------------------------------------------------------------
		// 03/210/2013 alazreg debug x3fpbloc4 illustrations
		ventilerLesDocumentsAnnexes_2(racine,giracine,idracine,getCodeMatiere(racine),typeOuvrage,basename);


		// -----------------------------------------------------------------------
		// Patch des DEBUA avec le classement FDOC
		// -----------------------------------------------------------------------
		for debua in searchElemNodes(racine,"DEBUA"){

			//if hasAttr(debua,"M") continue;
			sFdoc = "";
			
			// 02/05/2013 MB : pour le mode flow des formulaires (bloquer l'ascenseur sur 1 formule)
			// on ajoute l'id du document : ,voir e-mail Armand Betton : 25/01/2013 17:18
			// on recupere le document-id a partir de l'ID des formt[1-7] (la liste est dans G_listGiFormtx)
			// pour prevoir tout risque d'erreur, initialiser le document-id avec l'id de l'ouvrage

			// 08/10/2013 alazreg
			// mantis 4689 + 4703
			// il manque l'attribut FDOC sur certains document SGML
			// je renseigne un FDOC par defaut
			sFdoc = format("{'classement':'FDOC/%1'}",getFdocPath(sMatiere,giracine,idracine));
			// 15/06/2016 alazreg
			if sMatiere == "GP95" && hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG" sFdoc = format("{'classement':'FDOC/%1'}","EL/GP95/FREG");
			
			addAttr(debua,"M",sFdoc);
			
			var doc_id = idracine;
			var formtNodeList =  List();
			
			if Set("DPFORM2-OPTJ","DPFORM-OPTJ").knows(giracine){
				/*for tag in G_listGiFormtx {
		var rs = leftSibling(debua,tag);
			if isaElemNode(rs) */
				if hasAttr(debua,"IDELEM") doc_id = attr["IDELEM",debua];

				//cout << format("\n===---==== ******* Debug MB 18/06/2013 : formtNodeList[0] = %1\n",doc_id);
				//break;

				// -----------------------------------------------------------
				// 17/05/2013 alazreg
				// -----------------------------------------------------------
				// indiquer document-id = id de la formule et pas id d'un autre niveau
				// parfois nous avons niv0 ou niv1 à côte de debua
				// le debua est suivi de debur puis de niv 0 ou niv1
				// il faut donc prendre le rightSibling de position 2
				// -----------------------------------------------------------
				//cout << format("============== debug rightSibling(debua) = %1\n",GI(rightSibling(debua,2)));
				// 18/06/2013 MB : on modifie suite pb sur dp11 
				// <DEBUA N="1" SUIV="NON" T="Abandon de cr&eacute;ance" firstel="" uasize="36632">
				// <DEBUR N="1" T="Abandon de cr&eacute;ance"><DEBMODIF><NIV0 ID="Y1M01-1"><TI0>Abandon de cr&eacute;ance</TI0><FORMT1
				// le DEBMODIF qui peut etre partout peut parasiter cette facon de faire
				// ==> solution : faire un acc?par le nom et non la position de l'?ment
				// if not G_setGiFormtx.knows(GI(rightSibling(debua,2))) doc_id = attr["ID",searchElemNodes(rightSibling(debua,2),eSort(G_setGiFormtx),"ID",nothing)[0]];
				
				var next = rightSibling(debua);
				while (next != nothing && !Set("NIV0","NIV1","NIV2","NIV3").knows(GI(next)) && !G_setGiFormtx.knows(GI(next))){
					var ancNode = next;
					next = rightSibling(next);
					//cout << format("\tRS(%1) = %2\n",echo(ancNode),echo(next));
				}
				//cout << format("\t apres la boucle : next = %1\n",echo(next));
				if next != nothing {
					if Set("NIV0","NIV1","NIV2","NIV3").knows(GI(next)) {
						// ---------------------------------------------------------
						// 08/10/2013 alazreg
						// attention certaines formules SGML n'ont pas tous les elements necessaires (FORMTx)
						// j'ajoute un test pour verifier que nous avons bien des FORMTx avant de renseigner la valuer doc_id
						//cout << format("\t DEBUA --> NIVx --> FORMTx = %1\n",echo(searchElemNodes(next,G_listGiFormtx)[0]));
						//cout << format("next = %1\n",echo(next));
						// ---------------------------------------------------------
						// 18/03/2014 alazreg
						// mantis 6486 procedures LP35
						// Probleme pour recuperer attribut formtx@ID qui n'existe pas
						// Il faut le creer a partir de l'attribut formtx@refid avant de l'exploiter
						// ---------------------------------------------------------
						var liste_formtx = searchElemNodes(next,G_listGiFormtx);
						if liste_formtx.length()>0{
							var formtx = liste_formtx[0];
							if (not hasAttr(formtx,"ID") && hasAttr(formtx,"REFID")) addAttr(formtx,"ID",attr["REFID",formtx]);
							doc_id = attr["ID",formtx];
						}
					}
					else if G_setGiFormtx.knows(GI(next)) {
						//cout << format("\t DEBUA --> FORMTx = %1\n",echo(next));
						doc_id = attr["ID",next];
					}
				}
				

				//sFdoc = format("{'classement':'FDOC/%1'}",getFdocPath(sMatiere,giracine,idracine),);
				sFdoc = format("{'classement':'FDOC/%1','document-id':'%2'}",getFdocPath(sMatiere,giracine,idracine),doc_id);
				//cout << format("\n======= ******* Debug MB 02/05/2013 : correction mode flow : sFdoc = %1\n", sFdoc);
				
				// 09/02/2016 SF ajout pack ope
				if (hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
					sFdoc = format("{'classement':'FDOC/EL/PACKOPE/%1/%2','document-id':'%3'}",attr["PACKID",racine],getTypeOuvrage(sMatiere,giracine,idracine,""),doc_id);
				}
			}
			// 05/06/2013 MB : mode flow traiter aussi les FP/QR/FPRO
			// elements devant toujours commencer une UA 	
			// "FP-OPTJ",	List("FP-OPTJ", "BLOC"),
			// "FPRO-OPTJ",	List("FPRO-OPTJ"),
			// "QR-OPTJ",	List("QR-OPTJ","GP20ZONE", "GP20THEM", "GP20"),
			else if Set("FP-OPTJ").knows(giracine){
				if hasAttr(debua,"IDELEM") {
					doc_id = attr["IDELEM",debua];
				}

				// 18/06/2013 MB : on modifie suite pb sur dp11 (cause debmodif parasite) voir explication plus haut pour les form
				/*
		cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1\n",echo(rightSibling(debua,2)));
		if not Set("FP-OPTJ").knows(GI(rightSibling(debua,2))){
		cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1 et searchElemNodes(rightSibling(debua,2),Set(FP-OPTJ),ID,nothing)[0]=%2\n",echo(rightSibling(debua,2)),echo(searchElemNodes(rightSibling(debua,2),"FP-OPTJ","ID")[0]));
		doc_id = attr["ID",searchElemNodes(rightSibling(debua,2),"FP-OPTJ","ID",nothing)[0]];
		}
		*/
				
				var next = rightSibling(debua);
				while (next != nothing && !Set("BLOC").knows(GI(next)) && GI(next) != "FP-OPTJ"){
					var ancNode = next;
					next = rightSibling(next);
					//cout << format("\tRS(%1) = %2\n",echo(ancNode),echo(next));
				}
				//cout << format("\t apres la boucle : next = %1\n",echo(next));
				if next != nothing {
					if Set("BLOC").knows(GI(next)) {
						//cout << format("\t DEBUA --> NIVx --> BLOC = %1\n",echo(searchElemNodes(next,"FP-OPTJ")[0]));
						doc_id = attr["ID",searchElemNodes(next,"FP-OPTJ")[0]];
					}
					else if GI(next) == "FP-OPTJ" {
						//cout << format("\t DEBUA --> FP-OPTJ = %1\n",echo(next));
						doc_id = attr["ID",next];
					}
				} 
				

				sFdoc = format("{'classement':'FDOC/%1','document-id':'%2'}",getFdocPath(sMatiere,giracine,idracine),doc_id);
				// 04/01/2016 SF modif fdoc packope
				if (hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
					sFdoc = format("{'classement':'FDOC/EL/PACKOPE/%1/%2','document-id':'%3'}",attr["PACKID",racine],getTypeOuvrage(sMatiere,giracine,idracine,""),doc_id);
				}
				
			}
			// 06/06/2013 MB : fpro
			else if Set("FPRO-OPTJ").knows(giracine){
				if hasAttr(debua,"IDELEM") {
					doc_id = attr["IDELEM",debua];
				}

				// 18/06/2013 MB : on modifie suite pb sur dp11 (cause debmodif parasite) voir explication plus haut pour les form
				/*
		cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1\n",echo(rightSibling(debua,2)));
		if not Set("FPRO-OPTJ").knows(GI(rightSibling(debua,2))){
		cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1 et searchElemNodes(rightSibling(debua,2),Set(FPRO-OPTJ),ID,nothing)[0]=%2\n",echo(rightSibling(debua,2)),echo(searchElemNodes(rightSibling(debua,2),"FPRO-OPTJ","ID")[0]));
		doc_id = attr["ID",searchElemNodes(rightSibling(debua,2),"FPRO-OPTJ","ID",nothing)[0]];
		}
		*/
				
				var next = rightSibling(debua);
				while (next != nothing && !Set("FPRO-OPTJ").knows(GI(next)) && GI(next) != "BLOC"){
					var ancNode = next;
					next = rightSibling(next);
					//cout << format("\tRS(%1) = %2\n",echo(ancNode),echo(next));
				}
				//cout << format("\t apres la boucle : next = %1\n",echo(next));
				if next != nothing {
					if Set("BLOC").knows(GI(next)) {
						//cout << format("\t DEBUA --> NIVx --> BLOC = %1\n",echo(searchElemNodes(next,"FPRO-OPTJ")[0]));
						doc_id = attr["ID",searchElemNodes(next,"FPRO-OPTJ")[0]];
					}
					else if GI(next) == "FPRO-OPTJ" {
						//cout << format("\t DEBUA --> BLOC = %1\n",echo(next));
						doc_id = attr["ID",next];
					}
				}
				
				sFdoc = format("{'classement':'FDOC/%1','document-id':'%2'}",getFdocPath(sMatiere,giracine,idracine),doc_id);	      
			}
			// 06/06/2013 MB : qr sauf gp20
			else if (Set("QR-OPTJ").knows(giracine) && sMatiere != "GP20") {
				if hasAttr(debua,"IDELEM") {
					doc_id = attr["IDELEM",debua];
				}
				// 18/06/2013 MB : on modifie suite pb sur dp11 (cause debmodif parasite) voir explication plus haut pour les form
				// cout << format("\n===---==== **** cas 1 *** Debug MB 06/06/2013 : rightSibling(debua,2) = %1\n",echo(rightSibling(debua,2)));
				// if not Set("QR-OPTJ").knows(GI(rightSibling(debua,2))){
				cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1 et searchElemNodes(rightSibling(debua,2),Set(QR-OPTJ),ID,nothing)[0]=%2\n",echo(rightSibling(debua,2)),echo(searchElemNodes(rightSibling(debua,2),"QR-OPTJ","ID")[0]));
				// doc_id = attr["ID",searchElemNodes(rightSibling(debua,2),"QR-OPTJ","ID",nothing)[0]];
				// }
				var next = rightSibling(debua);
				while (next != nothing && !Set("QR-OPTJ").knows(GI(next)) && GI(next) != "BLOC"){
					var ancNode = next;
					next = rightSibling(next);
					//cout << format("\tRS(%1) = %2\n",echo(ancNode),echo(next));
				}
				//cout << format("\t apres la boucle : next = %1\n",echo(next));
				if next != nothing {
					if Set("BLOC").knows(GI(next)) {
						//cout << format("\t DEBUA --> BLOC --> QR-OPTJ = %1\n",echo(searchElemNodes(next,"QR-OPTJ")[0]));
						doc_id = attr["ID",searchElemNodes(next,"QR-OPTJ")[0]];
					}
					else if GI(next) == "QR-OPTJ" {
						//cout << format("\t DEBUA --> FORMTx = %1\n",echo(next));
						doc_id = attr["ID",next];
					}
				}
				
				sFdoc = format("{'classement':'FDOC/%1','document-id':'%2'}",getFdocPath(sMatiere,giracine,idracine),doc_id);	      
			}
			// 
			// 11/06/2013 MB : qr du gp20
			else if (Set("QR-OPTJ").knows(giracine) && sMatiere == "GP20") {
				if hasAttr(debua,"IDELEM") {
					doc_id = attr["IDELEM",debua];
				}

		// cas particulier du gp20, le bloc n'est pas toujour a la meme position ==> on utilise les GI dans rightsibling
		// A suivre ....
				// if not Set("GP20").knows(GI(rightSibling(debua,2))){
		//cout << format("\n===---==== ******* Debug MB 06/06/2013 : rightSibling(debua,2) = %1 et searchElemNodes(rightSibling(debua,2),Set(QR-OPTJ),ID,nothing)[0]=%2\n",echo(rightSibling(debua,2)),echo(searchElemNodes(rightSibling(debua,2),"QR-OPTJ","ID")[0]));
				// doc_id = attr["ID",searchElemNodes(rightSibling(debua,2),"GP20","ID",nothing)[0]];
				// }

				var next = rightSibling(debua);
				while (next != nothing && !Set("GP20").knows(GI(next)) && !Set("GP20ZONE", "GP20THEM").knows(GI(next))){
					var ancNode = next;
					next = rightSibling(next);
					//cout << format("\tRS(%1) = %2\n",echo(ancNode),echo(next));
				}
				//cout << format("\t apres la boucle : next = %1\n",echo(next));
				if next != nothing {
					if Set("GP20ZONE", "GP20THEM").knows(GI(next)) {
						//cout << format("\t DEBUA --> GP20THEM/GP20ZONE --> QR-OPTJ = %1\n",echo(searchElemNodes(next,"GP20")[0]));
						doc_id = attr["ID",searchElemNodes(next,"GP20")[0]];
					}
					else if GI(next) == "GP20" {
						//cout << format("\t DEBUA --> GP20 = %1\n",echo(next));
						doc_id = attr["ID",next];
					}
				}

				sFdoc = format("{'classement':'FDOC/%1','document-id':'%2'}",getFdocPath(sMatiere,giracine,idracine),doc_id);	      
			}
			else {
				// 02/05/2013 MB : fin modifs MB
				// ancien fonctionnement : sans document-id : à migrer vers le nouveau au fur et a mesure a terme...
				sFdoc = format("{'classement':'FDOC/%1'}",getFdocPath(sMatiere,giracine,idracine));
			}

			if sMatiere == "GP95" && hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG" sFdoc = format("{'classement':'FDOC/%1'}","EL/GP95/FREG");

			// 29/12/2017 alazreg https://jira.els-gestion.eu/browse/SIEYSTONE-22 : evolution YELLOWSTONE pour les EL/JRP TYPE=D8
			// cout << format("debug giracine=%1\n",giracine);
			if giracine=="DECISBLOC-OPTJ"{
				var decis_optj=rightSibling(debua,"DECIS-OPTJ");
				if decis_optj!=nothing{
					if hasAttr(decis_optj,"TYPE") && attr["TYPE",decis_optj]=="D8" sFdoc="{'classement':'FDOC/EL/SOURCES/JRP/JPEURO'}";
				}
			}			
			addAttr(debua,"M",sFdoc);

			// -----------------------------------------------------------------------
			// 11/03/2013 AL
			// Integration des doc el dans dalloz.fr
			// Modifier les titre des debua pour les etudes + formules pour commencer
			// puis etendre aux autres documents
			// -----------------------------------------------------------------------
			if hasAttr(debua,"T"){
				if giracine == "ETD-OPTJ" attr["T",debua] = titreOuvrage;
			}
		}  // fin boucle for debua...

		// -----------------------------------------------------------------------
		// Patch des id logiques des codes
		// -----------------------------------------------------------------------
		// 17/07/2013 MB : il faut ajouter le traitt des PXCODE dans les textes s'ils n'ont pas de CDXACODE/XACODE
		//                 mantis 2846
		// si txt ==> on patche les PXCODE
		if (giracine == "TXTBLOC-OPTJ") {
			racine = patcher_pxcode_in_txtboc(racine);  
		}
		// fin ajouts MB  17/07/2013
		
		for xacode in searchElemNodes(racine,List("CDXACODE","XACODE")){
			sCode = "";
			sType = "";
			sNum = "";
			sNiv = "";
			sExnum = "";
			sSaisie = "";
			sAutre = "";
			
			if hasAttr(xacode,"CODE") {
				// 25/10/2012 MB : pb qlques liens errones au fil du texte
				// à cause de la casse dans le nom du code ==> on met tout en maj
				sCode = attr["CODE",xacode].transcript(UpperCase);

			}
			if hasAttr(xacode,"TYPE") sType = attr["TYPE",xacode];
			if hasAttr(xacode,"NUM") sNum = attr["NUM",xacode];
			if hasAttr(xacode,"NIV") sNiv = attr["NIV",xacode];
			if hasAttr(xacode,"EXNUM") sExnum = attr["EXNUM",xacode];
			if hasAttr(xacode,"SAISIE") sSaisie = attr["SAISIE",xacode];
			if hasAttr(xacode,"AUTRE") sAutre = attr["AUTRE",xacode];
			
			// if sCode == "CCV" && hasAttr(xacode,"versionCible") && attr["versionCible",xacode] == "OLD"{
				// if sNum == "2277" cout << format("debug avant %1\n",echo(xacode));
			// }
			
			// 25/03/2013 MB : ajout cas particulier pour le code minier DZ : mail YT du 25/03/2013 14:52
			// il ne faut pas creer d'id logique vers le code minier DZ sur les <xacode code="cm" TYPE="NO"> qui reste EL) 
			// 29/03/2013 d'autre autres evolutions, à traiter comme le code minier EL les autres codes suivants : CDC, CTM, CMP, CPCA, CRA, CCVA, CUA, CM
			// ==> on utilise un set des codes a traiter : G_setCodesELavecTypeNo

			//if (!(sCode == "CM" && sType.transcript(UpperCase) == "NO")) {	  
			if (!(G_setCodesELavecTypeNo.knows(sCode) && sType.transcript(UpperCase) == "NO")){
				// 17/04/2013 MB : transfo en id logique uniquement si absent du set : setCodesANePasPatcher
				if !setCodesANePasPatcher.knows(sCode)
				sIdLogique = hulk_getCodeLogicID(sCode,sType,sNum,sNiv,sExnum,sSaisie, sAutre);
			}
			else {
				//cout << format("DEBUG MB cas CM avec TYPE no detecte : %1\n",xacode);
				sIdLogique = "";
			}

			// ---------------------------------------------------------
			// 12/10/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-359 idem mantis 13898 pour CT2 OLD
			// ---------------------------------------------------------
			// 28/11/2016 alazreg mantis 13898
			// dans le cas du code civil dalloz ancien article ne pas poser ID LOGIQUE mais laisser IDREF physique jusqu'à ce que DALLOZ pose les ID LOGIQUES sur les anciens articles
			// ---------------------------------------------------------
			// if sCode == "CCV" && hasAttr(xacode,"versionCible") && attr["versionCible",xacode] == "OLD"
			if Set("CCV","CT2").knows(sCode) && hasAttr(xacode,"versionCible") && attr["versionCible",xacode] == "OLD"
			{
				sIdLogique="";
				// if sNum == "2277" cout << format("debug apres %1\n",echo(xacode));
			}

			if sIdLogique != ""{
				//attr["IDREF",xacode] = sIdLogique;
				// 17/04/2013 MB : transfo en id logique uniquement si absent du set : setCodesANePasPatcher 
				if !setCodesANePasPatcher.knows(sCode) {
					addAttr(xacode,"IDREF",sIdLogique);
					if GI(xacode) == "CDXACODE" changeGI(xacode,"XACODE");
				}
				// 01/05/2013 MB : s'il fait partie des codes EL == il faut le livrer s'il ne l'est pas deja
				// 02/05/2013 MB : attn,prevoir de le patcher a son tour pour inserer l'attribut M="DOC/..."
			}
		}
		
		
		// YE Mantis 0012547 Le 22/02/2016 patch "Principales nouveautés"
		if sMatiere == "CD01" {
			var setEtd = Set("Z1999", "Z1998", "Z1997", "Z1996", "Z1995", "Z1994");
			var idEtd = idracine.explode("-")[0];
			
			if setEtd.knows(idEtd){
				cout << format("la Matiere est %1 \n",sMatiere);				
				for etpres in searchElemNodes(racine,"ETPRES"){	
									
					var titrePresElem = child(etpres,"T-BEF");
					var titrePres = content(child(etpres,"T-BEF"));							
					// if titrePres == "Pr&eacute;sentation" changeContent(child(etpres,"T-BEF"),"Principales nouveaut&eacute;s");
					deleteSubTree(titrePresElem);
					var nodeTbef = Node(doc, "T-BEF", Node(doc,"#CDATA","Principales nouveaut&eacute;s"));	
					insertSubTree(etpres,0,nodeTbef);
				}		
			}
		}
		
		// -----------------------------------------------------------------------
		// 13/06/2012 MB :
		// Patch des id logiques des jrp
		// -----------------------------------------------------------------------
		// 17/04/2014 alazreg/sfouzi
		// remplacement de l'ancien code source
		// -----------------------------------------------------------------------
		
		//cout << format("26/08/2014 : DEBUG giracine = '%1' avant tests : <> COMJRP-OPTJ\n",echo(racine));
		
		if !Set("COMJRP-OPTJ","LISTE").knows(giracine){
			// cout << format("debug racine avant appel = '%1'\n",echo(racine));
			resoudre_id_logique_jrp_dalloz(racine);
		}

		fileoutname = filesgm+".patch.sgm";
		// cout << format("DEBUG fileoutname = %1\n",fileoutname);
		fileout = FileStream(fileoutname,"w");
		dumpSubTree(racine,fileout,Map());
		//dumpSubTreeSGMLwithRC(racine,fileout);
		corriger_dumpbalise46(fileoutname);
		close(fileout);
		
		close(doc);
		
		// 27/12/2012 al
		// Mise en commentaire du cout
		//cout << "\n";
		
		// 14/09/2012 MB : ecrire dans un fichier (ds ELA_TMP_IDX) les jrp absentes de DZ
		// ce fichier sera utilise par makeblocs avec l'option "exoticjrp" pour creer les blocs jrp
		// à livrer dans Hulk
		// 25/09/2012 MB : n'ecraser ce fichier que si le flag : G_bJrp est positionne 

		//    if G_bJrp {
		setObjectInFile(exoticjrpFile, G_setIdjrpToBuild);
		// 04/09/2013 MB : suite pb jrp non livree ! ==> maj du set des jrp specifiques aux matieres : G_setIdjrpToBuildMat 
		
		setObjectInFile(exoticjrpFileMat, G_setIdjrpToBuildMat);
		//cout << format("le fichier : %1 a ete recree.\n",exoticjrpFile);
		//    }

		var filetrace = FileStream(filePath(env("ELA_LIV"),"liste_jrp_attribut_source_inconnu.txt"),"w");
		for elem in liste_jrp_attribut_source_inconnu
		filetrace << format("%1\n",elem);
		close(filetrace);

		filetrace = FileStream(filePath(env("ELA_LIV"),"liste_jrp_sans_attribut_source.txt"),"w");
		for elem in liste_jrp_sans_attribut_source
		filetrace << format("%1\n",elem);
		close(filetrace);
		
		filetrace = FileStream(filePath(env("ELA_LIV"),"liste_cas_non_resolus.txt"),"w");
		for elem in liste_cas_non_resolus
		filetrace << format("%1\n",elem);
		close(filetrace);
		
		filetrace = FileStream(Hulk_icons_Ok_Nok,"w");
		for elem in fichiers_icons
		filetrace << elem ;
		close(filetrace);

		// 22/07/2013 MB : sauvegarde des jrp qui ne sont plus livrees pour nettoyage dans mongodb
		filetrace = FileStream(G_fileIdjrpToClean,"w");
		for elem in eSort(G_setIdjrpToClean) filetrace << format("%1\n",elem);
		close(filetrace);
		if G_setIdjrpToClean.length() > 0 creer_delivery_suppressions_auto(G_fileIdjrpToClean,G_setIdjrpToClean);
		if G_setIdjrpToClean.length() > 0 cout << format("Warning : des jrp ne sont plus livrees par les EL ==> un nettoyage doit etre fait dans la base mongodb :\n Pour la liste de ces jrp, consulter le fichier : %1\n",G_fileIdjrpToClean);

		// 23/07/2013 AL/MB : sauvegarde du set des jrp non cumule (qui conncerne que les docs traite
		//                    on ecrase pas au deuxieme appel du patch pour traiter les jrp
		//                    si un autre appel est ajoute ex. txt) ==> le traietr ici
		if (G_dirin.search(0,"/tmp") < 0) setObjectInFile(exoticjrpFileMat, G_setIdjrpToBuildMat);
	}   
}



// 23/10/2012 MB : ajout fct pour alimenter les sous-dossiers www/(CD|GP)XX avec les pdf/img/png...
// 					on reprend ce qui est fait dans la fonction  make_patch en enlevant les traitements en plus

function alim_CD_static_files(listfilesgm){

	// Cette fonction alimente les CD|GP en images/pdf statiques
	// ces fichiers sont livres dans ELnet dans www/pdf et www/html/icons
	// pour Hulk il faudra les livrer dans www/CDxx/ETD/z2025/z2025.pdf         
	// ce traitement est deja realise par make_patch : comme make_patch fait plein d'autres 
	// traitements, on le met ici pour prevoir une realimentation sans refaire le patch

	cout << format("Je suis dans la fonction alim_CD_static_files\n");

	var ret,racine,giracine,dtd,doc,basename;

	var sIdLogique = nothing;
	var sFdoc = nothing;
	var sMatiere,giNode,idracine,sId;

	var sCode,sType,sNum,sNiv,sExnum,sSaisie, sAutre;
	var typeOuvrage = nothing;

	var fichiers_icons = List();

	var pdf_path = filePath(env("ELA_LIV"),"www","pdf");    
	var icon_path = filePath(env("ELA_LIV"),"www","html","icons"); 
	var path = pdf_path;
	var fileToCopy = ""; 


	for filesgm in eSort(listfilesgm){
		basename = fileBaseName(filesgm);
		cout << ".";
		flush(cout);
		if basename.rsearch(0,G_regexpToc) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}
		if Set("cdcoll.sgm","subscription.sgm","type_source_lpveille.sgm").knows(basename){
			continue;
		}
		
		dtd = filePath(env("ELA_DTD"),getDtdBaseNameFromFileName(filesgm));
		
		// 23/05/2012 AL
		// Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
		// Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;
		
		if G_bUnicode{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		else{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
			// 04/07/2012 MB : en deplacant les traitements du patch apres utf8 ==> pb avec les entites num > 255
			// solution : NMTOKEN remplaces par CDATA
		}
		
		if ret.status != 0{
			G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,fileBaseName(dtd));
			continue;
		}
		
		//G_fileLog << format("PARSE OK %1 %2\n",filesgm,fileBaseName(dtd));
		
		doc = ret.document;
		racine = root(doc);
		
		giracine = GI(racine);
		sMatiere = attr["MATIERE",racine];
		// 24/10/2012 MB : pour les txt/jrp l'attribut MATIERE = "ELNET", il faudra mettre "SOURCES" à la place
		//                 idealement changer l'attribut MATIERE qui ets egal à "ELNET" en "SOURCES" (+tard car utilise ailleurs)
		if sMatiere == "ELNET" sMatiere = "SOURCES";

		// MB 13/06/2012 : la racine des listes n'a pas d'ID (../data/dp46_lst-code.optj.sgm, ...)
		// ajouter un test
		if hasAttr(racine,"ID")
		idracine = attr["ID",racine];
		
		typeOuvrage = getTypeOuvrage(sMatiere,giracine, idracine,basename);
		
		// MB 10/10/2012 : ajout repartition des fichiers images, html et pdf dans www/MATIERE/TYPEOUVRAGE/ID/fichier.pdf 
		//                 a faire apres l'etape d'ajout du typeouvrage
		// 
		// commencer par traiter le cas particulier du gp20
		if giracine == "QR-OPTJ" && sMatiere == "GP20" {
			//cout << format("-------> CAS giracine == QR-OPTJ\n");
			for fichier_icon in searchElemNodes(racine,"GP20","IDFICHE") {
				//cout << format("------->GP20 IDFICHE\n");
				var fichier = attr["IDFICHE",fichier_icon].transcript(LowerCase);
				var format = "pdf";
				fichier = format("%1.%2",fichier,format);
				path = pdf_path;
				fileToCopy = fichier;
				fichier = filePath(path,fichier);
				

				// 12/11/2012 MB : on garde les id :  X0Q001-HZ et  X0Q002-HZ 
				// modif faite aussi dans make_uc()
				/*if idracine2.search(0,G_regexpIDTronq) > 0 {
		//cout << format("idracine2 = %1\n",idracine2);
		idracine2 = idracine2.replace(0,G_regexpIDTronq,"");
		//cout << format("idracine2 = %1\n",idracine2);
		}
		*/
				var idracine2 = idracine;
				var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
				//cout << format("//////////////--->fichier a copier vers : %2: %1\n",fichier,dest);
				// si le dossier n'existe pas, on le cree
				// on fait un mkdir -p par manque de tmps
				// 19/10/2012 : correction MB suite pb ecrasement repertoires

				if !fileAccess(filePath(env("ELA_LIV"),"www")){
					makeDir(filePath(env("ELA_LIV"),"www"));
				}

				if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere)) {
					makeDir(filePath(env("ELA_LIV"),"www",sMatiere));
				}
				
				if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage)) {
					makeDir(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage));
				}
				
				if !fileAccess(dest) {
					makeDir(dest);
				}

				//fichier = filePath(path,fichier);
				cout << format("//////////////--->fichier a copier vers : %2: %1\n",fichier,dest);
				if fileAccess(dest) {
					// copier le fichier image dans dest s'il n y est pas
					//if !fileAccess(filePath(dest,fileToCopy)){
					if fileAccess(fichier) {
						fileCopy(fichier, dest);
						//fileRemove(fichier);
					}
					else {
						//12/10/2012 MB : si fichier inexistant ==> regarder la version en minuscule (ce qui arrive à la saisie !)
						fileToCopy = fileToCopy.transcript(LowerCase);
						fichier = filePath(path,fileToCopy.transcript(LowerCase));
						if fileAccess(fichier) {
							fileCopy(fichier, dest);
							//fileRemove(fichier);
						}
					}
					//}
					if fileAccess(filePath(dest,fileToCopy))
					fichiers_icons << format("%1##%2 ---> %3 Ok *\n",fichier,format,filePath(dest,fileToCopy)); 
					else
					fichiers_icons << format("%1##%2##%4 ---> %3 NOk *\n",basename,fichier,format,filePath(dest,fileToCopy)); 
				}
			}
		}





		// 23/10/2012 MB : traitement du cas des syntheses : livrer le pdf (ID)
		//             
		// 
		//cout << format(" GIRACINE = %1 sMatier = %2 \n",giracine,sMatiere);
		if giracine == "ETD-OPTJ" && sMatiere == "CD15" {
			//cout << format("-------> CAS giracine == DP15 +  ETD-OPTJ\n");

			var fichier = attr["ID",racine].transcript(LowerCase);
			var format = "pdf";
			fichier = format("%1.%2",fichier,format);
			path = pdf_path;
			fileToCopy = fichier;
			fichier = filePath(path,fichier);
			
			var idracine2 = idracine; 
			var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
			//cout << format("//////////////--->fichier %1 a copier vers %2\n",fichier,dest);

			// si le dossier n'existe pas, on le cree
			// on fait un mkdir -p par manque de tmps
			// 19/10/2012 : correction MB suite pb ecrasement repertoires
			if !fileAccess(filePath(env("ELA_LIV"),"www")){
				makeDir(filePath(env("ELA_LIV"),"www"));
			}

			if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere)) {
				makeDir(filePath(env("ELA_LIV"),"www",sMatiere));
			}
			
			if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage)) {
				makeDir(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage));
			}

			if !fileAccess(dest) {
				makeDir(dest);
			}

			//fichier = filePath(path,fichier);
			cout << format("//////////////--->fichier %1 a copier vers %2/%3",fichier,dest,fileToCopy);
			//	    if (fileCopy(fichier, filePath(dest,fileToCopy))) cout << " Succes copie\n";
			if (fileCopy(fichier, dest)) cout << " Succes copie\n";
			else cout << " Echec copie\n";

			if fileAccess(dest) {
				// copier le fichier image dans dest s'il n y est pas
				//if !fileAccess(filePath(dest,fileToCopy)){
				if fileAccess(fichier) {
					fileCopy(fichier, dest);
					fileRemove(fichier);
				}
				else {
					//12/10/2012 MB : si fichier inexistant ==> regarder la version en minuscule (ce qui arrive à la saisie !)
					fichier = filePath(path,fileToCopy.transcript(LowerCase));
					if fileAccess(fichier) {
						fileCopy(fichier, dest);
						fileRemove(fichier);
					}
				}
				//}
				if !fileAccess(filePath(dest,fileToCopy))
				fichiers_icons << format("%1##%2 ---> %3 Ok *\n",fichier,format,filePath(dest,fileToCopy)); 
				else
				fichiers_icons << format("%1##%2##%4 ---> %3 NOk *\n",basename,fichier,format,filePath(dest,fileToCopy)); 
			}
		}


		// le cas general
			
		// 17/08/2016 alazreg Evolution Preventeur
		// Ajout des elements LINK qui contiennent des renvois vers PDF
		
		// for fichier_icon in searchElemNodes(racine,"FIG","FICHIER"){
			// var fichier = attr["FICHIER",fichier_icon];
			// var format = "pdf";
			// if hasAttr(fichier_icon,"FORMAT") format = attr["FORMAT",fichier_icon];
		// }

		for fichier_icon in searchElemNodes(racine,List("FIG","LINK")) {
			var fichier = "";
			var format = "";
			if GI(fichier_icon) == "FIG"{
				fichier = attr["FICHIER",fichier_icon];
				format = "pdf";
				if hasAttr(fichier_icon,"FORMAT") format = attr["FORMAT",fichier_icon];
			}
			
			if GI(fichier_icon) == "LINK" && hasAttr(fichier_icon,"TYPE") && Set("PDF","GIF","JPEG").knows(attr["TYPE",fichier_icon].transcript(UpperCase)){
				fichier = attr["TARGET",fichier_icon];
				format = attr["TYPE",fichier_icon];
			}
			
			format = format.transcript(LowerCase);
			
			// les pdf sont a prendre de www/pdf : 1 pdf a le format vide (par defaut)
			// les autres sont a prendre de www/html/icons : ajouter le format comme extension
			if fichier == "" continue;
			// Si l'extension est absente alors ajouter l'extension de lattribut FORMAT ou TYPE
			// Exemple 1 : <FIG FICHIER="q5fpro201001" FORMAT="PDF">  => fichier = q5fpro201001.pdf
			// Exemple 2 : <LINK TARGET="q5fpro201001" TYPE="PDF">    => fichier = q5fpro201001.pdf
			//----------------------------------------------------------------------------------------------
			if fichier.rsearch(0,G_regexpIcon) == nothing  {
				if format != "" fichier = format("%1.%2",fichier,format.transcript(LowerCase));
			}
			
			// si extension .pdf ou extension absente ==> path = pdf_path = ./www/pdf/
			// si extension autre que .pdf            ==> path = icon_path = ./www/html/icons/
			//----------------------------------------------------------------------------------------------
			if  fichier.rsearch(0,G_regexpPdf) != nothing || format == "pdf" path = pdf_path;
			else path = icon_path;
			
			fileToCopy = fichier;
			fichier = filePath(path,fichier);
			
			// regle3 : destination. copier dans : www/MATIERE/TYPEOUVRAGE/ID/fichier
			//                       supprimer de la source (trop volumineux pour la synchro)
			//----------------------------------------------------------------------------------------------
			
			var idracine2 = idracine;
			if idracine2.search(0,G_regexpIDTronq) > 0 {
				//cout << format("idracine2 = %1\n",idracine2);
				idracine2 = idracine2.replace(0,G_regexpIDTronq,"");
				//cout << format("idracine2 = %1\n",idracine2);
			}
			
			var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
			// si le dossier n'existe pas, on le cree
			// on fait un mkdir -p par manque de tmps

			if !fileAccess(filePath(env("ELA_LIV"),"www")) {
				makeDir(filePath(env("ELA_LIV"),"www"));
			}

			if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere)) {
				makeDir(filePath(env("ELA_LIV"),"www",sMatiere));
			}

			if !fileAccess(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage)){
				makeDir(filePath(env("ELA_LIV"),"www",sMatiere,typeOuvrage));
			}

			if !fileAccess(dest) {
				makeDir(dest);
			}
			
			if fileAccess(dest) {
				// copier le fichier image dans dest s'il n y est pas
				//if !fileAccess(filePath(dest,fileToCopy)){
				if fileAccess(fichier) {
					fileCopy(fichier, dest);
					//fileRemove(fichier);
				}
				else {
					//12/10/2012 MB : si fichier inexistant ==> regarder la version en minuscule (ce qui arrive à la saisie !)
					fileToCopy = fileToCopy.transcript(LowerCase);
					fichier = filePath(path,fileToCopy.transcript(LowerCase));
					if fileAccess(fichier) {
						fileCopy(fichier, dest);
						//fileRemove(fichier);
					}
				}
				//}
				if fileAccess(filePath(dest,fileToCopy))
				fichiers_icons << format("%1##%2 ---> %3 Ok\n",fichier,format,filePath(dest,fileToCopy)); 
				else 
				fichiers_icons << format("%1##%2##%3 ---> %4 NOk\n",basename,fichier,format,filePath(dest,fileToCopy)); 
			}
		}
		
	}  

	var filetrace = FileStream(Hulk_icons_Ok_Nok,"w");
	for elem in fichiers_icons
	filetrace << elem ;
	close(filetrace); 
}

/*

*/


/*
23/10/2012 MB : ajout fct pour verifier les sous-dossiers www/(CD|GP)XX avec les pdf/img/png...
				on reprend ce qui est fait dans la fonction  alim_CD_static_file en enlevant les copies de fichiers
*/

function verif_CD_static_files(listfilesgm){

	// Cette fonction verifie pour les CD|GP dans Hulk la presence/absence des images/pdf statiques
	// ces fichiers sont livres dans ELnet dans www/pdf et www/html/icons
	// pour Hulk il faudra les livrer dans www/CDxx/ETD/z2025/z2025.pdf         
	// le resultat est dans le fichiers de log : $ELA_TMP_IDX/Hulk_icons_Ok_Nok.trace

	cout << format("Je suis dans la fonction verif_CD_static_files\n");

	var ret,racine,giracine,dtd,doc,basename;

	var sIdLogique = nothing;
	var sFdoc = nothing;
	var sMatiere,giNode,idracine,sId;

	var sCode,sType,sNum,sNiv,sExnum,sSaisie, sAutre;
	var typeOuvrage = nothing;

	var fichiers_icons = List();

	var pdf_path = filePath(env("ELA_LIV"),"www","pdf");    
	var icon_path = filePath(env("ELA_LIV"),"www","html","icons"); 
	var path = pdf_path;
	var fileToCopy = ""; 


	for filesgm in eSort(listfilesgm){
		basename = fileBaseName(filesgm);
		cout << ".";
		flush(cout);
		if basename.rsearch(0,G_regexpToc) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}
		if Set("cdcoll.sgm","subscription.sgm","type_source_lpveille.sgm").knows(basename){
			continue;
		}
		
		dtd = filePath(env("ELA_DTD"),getDtdBaseNameFromFileName(filesgm));
		
		// 23/05/2012 AL
		// Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
		// Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;
		
		if G_bUnicode{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		else{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
			// 04/07/2012 MB : en deplacant les traitements du patch apres utf8 ==> pb avec les entites num > 255
			// solution : NMTOKEN remplaces par CDATA
		}
		
		if ret.status != 0{
			G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,fileBaseName(dtd));
			continue;
		}
		
		//G_fileLog << format("PARSE OK %1 %2\n",filesgm,fileBaseName(dtd));
		
		doc = ret.document;
		racine = root(doc);
		
		giracine = GI(racine);
		sMatiere = attr["MATIERE",racine];
		// 24/10/2012 MB : pour les txt/jrp l'attribut MATIERE = "ELNET", il faudra mettre "SOURCES" à la place
		//                 idealement, il faudra changer l'attribut MATIERE qui est egal à "ELNET" en "SOURCES" (+tard car utilise ailleurs)
		if sMatiere == "ELNET" sMatiere = "SOURCES";

		// MB 13/06/2012 : la racine des listes n'a pas d'ID (../data/dp46_lst-code.optj.sgm, ...)
		// ajouter un test
		if hasAttr(racine,"ID")
		idracine = attr["ID",racine];
		
		typeOuvrage = getTypeOuvrage(sMatiere,giracine, idracine,basename);
		
		// MB 10/10/2012 : ajout repartition des fichiers images, html et pdf dans www/MATIERE/TYPEOUVRAGE/ID/fichier.pdf 
		//                 a faire apres l'etape d'ajout du typeouvrage
		// 
		// commencer par traiter le cas particulier du gp20
		if giracine == "QR-OPTJ" && sMatiere == "GP20" {
			//cout << format("-------> CAS giracine == QR-OPTJ\n");
			for fichier_icon in searchElemNodes(racine,"GP20","IDFICHE") {
				//cout << format("------->GP20 IDFICHE\n");
				var fichier = attr["IDFICHE",fichier_icon].transcript(LowerCase);
				var format = "pdf";
				fichier = format("%1.%2",fichier,format);
				path = pdf_path;
				fileToCopy = fichier;
				fichier = filePath(path,fichier);
				
				var idracine2 = idracine;
				if idracine2.search(0,G_regexpIDTronq) > 0 {
					//cout << format("idracine2 = %1\n",idracine2);
					idracine2 = idracine2.replace(0,G_regexpIDTronq,"");
					//cout << format("idracine2 = %1\n",idracine2);
				}
				
				var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
				//cout << format("//////////////--->fichier a copier vers : %2: %1\n",fichier,dest);

				//fichier = filePath(path,fichier);
				cout << format("//////////////--->fichier a copier vers : %2: %1\n",fichier,dest);
				// copier le fichier image dans dest s'il n y est pas
				if fileAccess(filePath(dest,fileToCopy)) || fileAccess(filePath(dest,fileToCopy.transcript(LowerCase)))
				fichiers_icons << format("%1##%2 ---> %3 Ok *\n",fichier,format,filePath(dest,fileToCopy)); 
				else
				fichiers_icons << format("%1##%2##%4 ---> %3 NOk *\n",basename,fichier,format,filePath(dest,fileToCopy)); 
			}
		}



		// 24/10/2012 MB : traitement du cas des syntheses : livrer verifier si le pdf (ID) est livre
		//             
		// 
		//cout << format(" GIRACINE = %1 sMatier = %2 \n",giracine,sMatiere);
		if giracine == "ETD-OPTJ" && sMatiere == "CD15" {
			cout << format("-------> CAS giracine == DP15 +  ETD-OPTJ\n");
			
			var fichier = attr["ID",racine].transcript(LowerCase);
			var format = "pdf";
			fichier = format("%1.%2",fichier,format);
			path = pdf_path;
			fileToCopy = fichier;
			fichier = filePath(path,fichier);
			
			var idracine2 = idracine; 
			var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
			cout << format("//////////////--->fichier a copier vers : %2: %1\n",fichier,dest);
			
			if fileAccess(filePath(dest,fileToCopy)) || fileAccess(filePath(dest,fileToCopy.transcript(LowerCase)))
			fichiers_icons << format("%1##%2 ---> %3 Ok *\n",fichier,format,filePath(dest,fileToCopy)); 
			else
			fichiers_icons << format("%1##%2##%4 ---> %3 NOk *\n",basename,fichier,format,filePath(dest,fileToCopy));
			
		}
	}



	// le cas general
	for fichier_icon in searchElemNodes(racine,"FIG","FICHIER") {
		var fichier = attr["FICHIER",fichier_icon];
		var format = "pdf";
		if hasAttr(fichier_icon,"FORMAT")
		format = attr["FORMAT",fichier_icon];
		
		
		// les pdf sont a prendre de www/pdf : 1 pdf a le format vide (par defaut)
		// les autres sont a prendre de www/html/icons : ajouter le format comme extension
		if fichier == "" continue;
		// regle1 : extension. si le fichier n'a pas d'extension ==> prendre celle de format si presente
		//----------------------------------------------------------------------------------------------
		if fichier.search(0,G_regexpIcon) < 0  {
			if format != ""  
			fichier = format("%1.%2",fichier,format.transcript(LowerCase));
		}
		
		// regle2 : chemin du fichier. si extension = "pdf" ou absence extension ==> path = pdf_path
		//----------------------------------------------------------------------------------------------
		if  fichier.search(0,G_regexpPdf) > 0 || format == "pdf" {
			path = pdf_path;
		}
		else {
			path = icon_path;  
		}
		
		fileToCopy = fichier;
		fichier = filePath(path,fichier);
		
		// regle3 : destination. copier dans : www/MATIERE/TYPEOUVRAGE/ID/fichier
		//                       supprimer de la source (trop volumineux pour la synchro)
		//----------------------------------------------------------------------------------------------
		
		var idracine2 = idracine;
		if idracine2.search(0,G_regexpIDTronq) > 0 {
			//cout << format("idracine2 = %1\n",idracine2);
			idracine2 = idracine2.replace(0,G_regexpIDTronq,"");
			//cout << format("idracine2 = %1\n",idracine2);
		}
		
		var dest = filePath(env("ELA_LIV"),"www",sMatiere ,typeOuvrage, idracine2);  
		if fileAccess(filePath(dest,fileToCopy)) || fileAccess(filePath(dest,fileToCopy.transcript(LowerCase)))
		fichiers_icons << format("%1##%2 ---> %3 Ok *\n",fichier,format,filePath(dest,fileToCopy)); 
		else
		fichiers_icons << format("%1##%2##%4 ---> %3 NOk *\n",basename,fichier,format,filePath(dest,fileToCopy));
		
	}  

	var filetrace = FileStream(Hulk_icons_Ok_Nok,"w");
	for elem in fichiers_icons
	filetrace << elem ;
	close(filetrace); 


}


function hulk_getJrpLogicID_prepa(listfilesgm){
	// ---------------------------------------------------------------------------
	// Cette fonction recense les renvois aux JRP
	// Cela prepapre le terrain pour le id logiques vers les jrp
	// ---------------------------------------------------------------------------

	cout << "Je suis dans la fonction hulk_getJrpLogicID_prepa\n";

	var ret,doc,dtd,racine,giracine,idracine,gielem;
	var setIdInternes = Set();
	var setIdrefExternes = Set();
	var mapIdrefExternes = Map();
	var mapIdrefExternesExemples = Map();

	var basenameSgm = nothing;
	var dtdBasename = nothing;


	// ---------------------------------------------------------------------------
	// On commence par creer une Map de tous les IDREF disponibles
	// On edite/imprime la Map pour avoir une vision claire de tous les IDREF externes classes par racine
	// ---------------------------------------------------------------------------
	var filenameIdLogiques = "/mac.public/tmp/hulk_idlogique_jrp_prepa.csv";
	var file = FileStream(filenameIdLogiques,"w");

	for filesgm in eSort(listfilesgm){
		basenameSgm = fileBaseName(filesgm);
		cout << ".";
		flush(cout);
		if basenameSgm.rsearch(0,G_regexpToc) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}

		if basenameSgm.rsearch(0,G_regexpTap) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}

		// Certaines racines n'ont pas d'UC
		if Set("cdcoll.sgm","subscription.sgm").knows(basenameSgm) continue;

		dtdBasename = getDtdBaseNameFromFileName(filesgm);

		// ---------------------------------------------------------------------------
		// 26/02/2013 alazreg
		// ---------------------------------------------------------------------------
		// j'ajoute ici une verification de la dtd avant de parser sinon le programme plante sur aaelnet_txtbloc_200812.optj.sgm
		// et les fichiers qui viennent apres ne sont pas traites
		// ---------------------------------------------------------------------------
		if dtdBasename == nothing{
			//cout << format("dtdBasename = %1 pour %2\n",dtdBasename,filesgm);
			set_exitval(2);
			continue;
		}

		dtd = filePath(env("ELA_DTD"),dtdBasename);

		// 01/06/2012 AL
		// Certaines DTD n'ont pas d'UC
		// Exemple : les listels.dtd
		if Set("listels.dtd","src.dtd").knows(dtdBasename) continue;

		// Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
		// Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;
		if G_bUnicode{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		else{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}

		if ret.status != 0{
			//G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,fileBaseName(dtd));
			G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,dtd);
			continue;
		}

		doc = ret.document;
		racine = root(doc);
		giracine = GI(racine);
		idracine = attr["ID",racine];

		for jrp in searchElemNodes(racine,List("jrp")){
			file << format("%1\t",idracine);
			dumpSubTree(jrp,file,Map());
			file << "\n";
		}
	}

	close(file);
	
	cout << format("Consulter le fichier %1\n",filenameIdLogiques);

	return 0;
}



/*
05/11/2012 MB : ajout fct pour recuperer la juridiction 
				==> utilisee pour la construction des id logiques dans le cas particulier du comjrp
*/

function getAbrevJuridicFromBaseRef(idJrp){

	if G_mapBaseRefIdDpm.knows(idJrp)
	if G_mapBaseRefAbrevNames.knows(G_mapBaseRefIdDpm[idJrp])
	return  G_mapBaseRefAbrevNames[G_mapBaseRefIdDpm[idJrp]];
	else
	return ""; 

}



/*------------------------------------------------------------------------------------------------------

24/04/2013 MB : ajout fonction qui calcule le nom DZ du source a partir du typeOuvrage
				utilisee pour renseigner la partie statsPivot dans les URFs

liste des typeOuvrage possibles :   "ADLIST", "CCOL", "CODE", "COMJRP", "CORNAFCC", "DECRYPTAGES", "DOCAM", "DPFORM", "DPFORM2", "DPFORM23"
"ETD","FP","FPAIE","FPRO","FRISQ","IDXJRP","IDXTXT","JRP","LISTE","LISTE23","QR","RECAPNEW","RECMAJ","TAG","TAG23","TXT"

------------------------------------------------------------------------------------------------------*/

function getDzSourceName(typeOuvrage){

	switch(typeOuvrage){

	case "CODE":
		return "LEGIS/CODIFIE";
		break;

	case "TXT":
		return "LEGIS/NON-CODIFIE";
		break;

		/* 10/06/2013 MB : a voir si on rajoute la docam
	case "DOCAM":
	return "LEGIS/NON-CODIFIE";
	break;
	*/

	case "JRP":
		return "JURIS";
		break;

	case "CCOL":
		return "CCOL";
		break;

	default:
		return "";
		break;

	}

}


/*------------------------------------------------------------------------------------------------------

24/04/2013 MB : ajout fonction qui calcule le type ouvarge DZ a partir du gi de la racine du document 
				utilisee pour renseigner la partie statsPivot dans les URFs

------------------------------------------------------------------------------------------------------*/


function getDzTypeOuvrage(giracine,typeOuvrage) {

	//10/06/2013 MB : suite retours tests, modifier le TypeOuvrage : mettre la valeur de TypeOuvrage mise dans les ua/ur
	//                specs : le type d’ouvrage est le meme que celui alimentant dejà les UR et les UA.
	/*var v = "";
if G_mapClassementZapetteHulk2.knows(giracine) {
	// G_mapClassementZapetteHulk2 = Map(
	//				     "COMJRP-OPTJ","ZAPETTE/CATEGORIE/Conventions collectives"
	//				     ,"DECISBLOC-OPTJ","ZAPETTE/CATEGORIE/Jurisprudence/@norme"
	//				     ,"TXTBLOC-OPTJ","ZAPETTE/CATEGORIE/Textes non codifi&eacute;s/@norme"
	
	v = G_mapClassementZapetteHulk2[giracine];
	if v.search("ZAPETTE/CATEGORIE/") >= 0  v = v.replace(0,"ZAPETTE/CATEGORIE/","");
	if  v.search("ZAPETTE/THEMATIQUE/") >= 0  v = v.replace(0,"ZAPETTE/THEMATIQUE/","");
	if  v.search("/") >= 0 
	v = v.replace(0,RegExp("/.*$"),"");
	cout << format("*********debug 24/04/2013 MB **** getDzTypeOuvrage(%3) ******>> tOuvrage DZ extrait de %1 = %2\n",G_mapClassementZapetteHulk2[giracine],v,giracine);
}
else 
	cout << format("********* warning 24/04/2013 MB **** getDzTypeOuvrage(%1) ******>> inconnu de la map\n",giracine);

return v;
*/
	return typeOuvrage;

}


/*------------------------------------------------------------------------------------------------------

24/04/2013 MB : ajout fonction qui calcule le code produit DZ  
				dans un 1er temps elle retourne meme chose que getDzTypeOuvrage 
				(à modifier si necessaire apres retours tests ou precisions specs)
10/06/2013 MB : suite retours tests, modifier le codeproduct : mettre sFdoc/idracine (ex. : "EL/CD21/ETD/X1048")
------------------------------------------------------------------------------------------------------*/

function getDzProductNode(giracine,idracine,sFdoc) {
	// 12/01/2014 MB : pour les jrp/txt ==> remplacer idracine par : ELJURISP/ELTXT
	//return format("%1",/*sFdoc,*/idracine);

	if idracine.search(0,"DECISBLOC") >=0 return ("ELJURISP");
	else if idracine.search(0,"TXTBLOC") >=0 return ("ELTXT");
	else return format("%1",idracine);
}




/*
05/11/2012 MB : ajout fct pour charger les maps a partir des extractions BR 
				==> utilisee pour la construction des id logiques dans le cas particulier du comjrp
*/
function chargerMapsfromBaseRef() {

	// cette fonction charge dans deux maps à partir des 
	// deux fichiers les plus recents de la baseref :

	// exr_iddpm_quot_%YYmmdd_[0-9]+\\.txt$ : pour la correspondance entre les ids dpm des txt et les noms des juridictions
	// exr_juridictions_%YYmmdd_[0-9]+\\.txt$ : pour la correspondance entre les noms des juridictions et leurs abreviations  

	// les abreviations sont utilises pour le calcul des id logiques des jrp pour le cas des comjrp (non exploite jusque la)

	G_mapBaseRefIdDpm = Map();
	G_mapBaseRefAbrevNames = Map();

	var date = timeFormat(timeCurrent(), "%Y%m%d");
	// ex. de fichier : exr_iddpm_quot_20121012_17425.txt
	var regexp1 = format("exr_iddpm_quot_%1_[0-9]+\\.txt$",date);
	var regexp2 = format("exr_juridictions_%1_[0-9]+\\.txt$",date);

	// Charger la 1ere map : iddpm <-> nom juridiction
	var listf = listfilesgmShort(baseRefFilesPath,regexp1);
	if listf.length() == 0  return 1;
	// il ne doit y avoir qu'un seul fichier dans cette liste !
	var file1 = filePath(baseRefFilesPath,listf[0]);
	var file = FileStream(file1,"r");
	var line,tokens,juridicName,idDpm,abgevName;
	
	if file != nothing{
		while(true){
			line = readLine(file);
			if line == nothing break;
			if line != nothing{
				line = line.replace(0,"\r","");
				line = line.replace(0,"\f","");
			}
			//cout << format("line=%1\n",line);
			tokens = line.explode("\t");
			idDpm = tokens[0]; 
			idDpm =  trimBoth(idDpm," ");
			
			if tokens.length() >=3 juridicName = tokens[3];
			juridicName = trimBoth(juridicName," ");
			//cout << format("\tchamp1=%1\tchamp2=%2\n",idDpm,juridicName);
			if idDpm != nothing && idDpm.length() >= 3
			G_mapBaseRefIdDpm[idDpm] = juridicName;      
		}   
		close(file);
	}
	else {
		cerr << format("Erreur : le fichier %1 n'a pas pu etre ouvert.\n",filePath(baseRefFilesPath,listf[0]));
	}

	// Charger la 2eme map : nom juridiction <-> nom abrege
	listf = listfilesgmShort(baseRefFilesPath,regexp2);
	if listf.length() == 0  return 1;
	// il ne doit y avoir qu'un seul fichier dans cette liste !
	file1 = filePath(baseRefFilesPath,listf[0]);
	file = FileStream(file1,"r");
	
	if file != nothing{
		while(true){
			line = readLine(file);
			if line == nothing break;
			if line != nothing{
				line = line.replace(0,"\r","");
				line = line.replace(0,"\f","");
			}
			
			tokens = line.explode("\t");
			juridicName = tokens[0];
			juridicName = trimBoth(juridicName," ");
			abgevName = tokens[1];
			abgevName = trimBoth(abgevName," ");
			if juridicName != nothing && juridicName.length() >= 3
			G_mapBaseRefAbrevNames[juridicName] = abgevName;
		}   
		close(file);
	}
	else {
		cerr << format("Erreur : le fichier %1 n'a pas pu etre ouvert.\n",filePath(baseRefFilesPath,listf[0]));
	}
	//for c,v in G_mapBaseRefAbrevNames
	// cout << format("|%1| ==> |%2|\n",c,v);
}

function getPackTitle(packId) {
	if (G_mapPackTitles.length() == 0) {
		G_mapPackTitles = Map();
		cout << format("Chargement de la MAP des Packs...\n");
		G_mapPackTitles = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"packOpeNames.map"));		
	}
	if G_mapPackTitles.knows(packId) return G_mapPackTitles[packId];

	return nothing;
}

function getPackDocuments(packId) {
	// cout << format("debugsf func getPackDocuments packId = '%1'\n", packId);
	if(G_mapPackDocuments.length() == 0) {
		G_mapPackDocuments = Map();
		cout << format("Chargement de la MAP des documents Pack ope...\n");
		G_mapPackDocuments = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"packOpeDocuments.map"));
	}
	// cout << format("debugsf func getPackDocuments packId = '%1'\n", packId);
	if G_mapPackDocuments.knows(packId) return G_mapPackDocuments[packId];
	
	return nothing;
}

// 26/04/2016 SF ajout fct pour charger la map des documents Preventeur
function getMapPreventeurDocuments() {
	if (G_mapPreventeurDocuments.length() == 0) {
		G_mapPreventeurDocuments = Map();
		cout << format("Chargement de la MAP des documents Preventeur...\n");
		G_mapPreventeurDocuments = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"preventeurDocuments.map"));
	}
	
	return 0;
}

// 23/11/2016 SF ajout pour migration GB : fct pour charger la map des renvois GB
function getMapRenvoisGB() {
	if (G_mapRenvoisGB.length() == 0) {
		G_mapRenvoisGB = Map();
		cout << format("Chargement de la MAP des renvois GB...\n");
		G_mapRenvoisGB = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"renvoisGB.map"));
	}
	
	return 0;
}

/*
21/11/2012 MB : ajout fct make_urf()
traite que les et dans un 1er temps
*/

// 25/02/2016 SF modif packope
// function make_urf(docxml,codeMatiere,elem,typeOuvrage,giracine,idracine,idelem,titreElem,libMatiere,sFdoc,basenameSgm,sUri,titreOuv){
function make_urf(racine,docxml,codeMatiere,elem,typeOuvrage,giracine,idracine,idelem,titreElem,libMatiere,sFdoc,basenameSgm,sUri,titreOuv){
	var gendoc = nothing ;
	//cout << format("******* 1 ************ dans make_urf() **************codeMatiere = %1\n",codeMatiere);
	// Ne pas generer d'urf dans le cas du lp35
	if codeMatiere == "LP35" return gendoc;
	// 32/12/2013 MB : pour cause ajout URFs niveau document ==> on modifie ici
	// sUri = sUri.replace(0,"#/UC$","#/urf.xml");
	sUri = sUri.replace(0,"#/UC","#/urf.xml");

	// extrait des specs : 20121116_AT_EditionsLegislatives_PGP_Plan-Marquage_V5.xls (\\nas)
	// Sources		219
	// Jurisprudence		220
	// Codes enrichis		221
	// Codes secs		222
	// Docam		223
	// Bofip		224

// les cas a traiter : ADLIST-OPTJ ; CCOL-OPTJ ; CDCOLL ; CODE-OPTJ ; COMJRP-OPTJ ; CORNAFCC-OPTJ ; DECISBLOC-OPTJ ; DOCAM-OPTJ ; DPFORM-OPTJ ; DPFORM2-OPTJ ; ETD ; ETD-OPTJ ; FP-OPTJ ; FPRO-OPTJ ; IDX-OPTJ ; IDXET-OPTJ ; LISTE ; QR-OPTJ ; RECMAJ-OPTJ ; REFDOCINFOS ; SRC ; TABLESOURCES ; TAG-OPTJ ;  TXTBLOC-OPTJ ; 
//
	//var numCD = "SOURCES"; // 25/11/2015 MB : par defaut, on met 100 (a confirmer par AM) pour ne pas faire planter l'outil de stats.
	var numCD = "100";
	if giracine.search(0,"DECISBLOC") >=0 numCD = "220";
	else if giracine.search(0,"TXTBLOC") >=0 numCD = "219";
	// cas code (utile ?)
	// 02/07/2013 MB : les codes doivent etre codes en codes secs : 222
	// 12/01/2014 MB : le else gene visiblement car pour les codes, bofip et docam, il rentre dans "SOURCES" ==> je l'enleve
	else if giracine.search(0,"CODE") >=0 numCD = "222";
	// pour la liste des bofip
	else if basenameSgm.search(0,"bofip") >= 0 {
	    numCD = "224";
	    //cout << format("[[[[[[[ ========DOCAM bofip =======> numCD = %4 basenameSgm = %1 codeMatiere = %1  giracine = %3\n",basenameSgm,codeMatiere,giracine,numCD);
	}
	// pour les docs docam ou bofip
	else if giracine.search(0,"DOCAM") >=0 {
		// cas docam EL
		numCD = "223";
		// cas docam Bofip
	  if basenameSgm.search(0,"bofip") >= 0 {
		numCD = "224";
	    //cout << format("[[[[[[[ ========DOCAM bofip =======> numCD = %4 basenameSgm = %1 codeMatiere = %1  giracine = %3\n",basenameSgm,codeMatiere,giracine,numCD);
	  }
		// pour le reste des cas ==> prendre la partie numerique du code produit
	}

	// 25/11/2015 MB : ajout cas lst bofip
	else if giracine.search(0,"BOFIP")>=0 {
	  numCD = "224";
	  //cout << format("[[[[ BOFIP [[[ ======== numCD = %4 =======> basenameSgm = %1 codeMatiere = %1  giracine = %3\n",basenameSgm,codeMatiere,giracine,numCD);
	}

	// 07/07/2015 SF mantis 11198
	// pour le GP95 le numCD est egal a 27
	// 31/07/2017 SF mail A. Mai lun. 31/07/2017 09:57 le numCD doit être égal à 12
	else if codeMatiere == "GP95" numCD = "12";
	else if codeMatiere.search(0,"EN23") >=0 numCD = "23";

	// 25/11/2015 MB : Attn. Verifier qu il s agit bien d'une matiere EL : GPxx, DPxx  ==> pour ne pas avoir de texte ("UR") en lieu des chiffres.
	//else
	else if (codeMatiere.search(0,"CD") >=0 || codeMatiere.search(0,"GP") >=0 ) {
	  var long = codeMatiere.length();
	  numCD = trimLeft(codeMatiere.extract(2,codeMatiere.length()),"0");
	  
	  //cout << format("++++++++++++++ 2 +++++++ codeMatiere.length() = %4 ++++ codeMatiere = %5 ++++>> creation urf pour ID = %1 numCD = %2 giracine = %3\n",idracine,numCD,giracine,codeMatiere.length(),codeMatiere);
	}
	
	// YE 26/06/2014 Mantis 7230
	// Pour les formules on prend le titre de l'encart racine si on est sur la racine 
	//
	if Set("DPFORM2-OPTJ").knows(giracine){	
		if giracine == GI(elem) titreElem = getTitreElem(elem,giracine,idracine);
		else {							
			if enclosed(elem,"TIF1") != nothing titreElem = content(enclosed(elem,"TIF1"));
			if enclosed(elem,"TIF2") != nothing titreElem = content(enclosed(elem,"TIF2"));
			if enclosed(elem,"TIF3") != nothing titreElem = content(enclosed(elem,"TIF3"));
			if enclosed(elem,"TIF4") != nothing titreElem = content(enclosed(elem,"TIF4"));
			if enclosed(elem,"TIF5") != nothing titreElem = content(enclosed(elem,"TIF5"));				
		}			
		var titreNormalise = "";
		titreNormalise = entity2char(titreElem);
		// YE Le 20/02/2015 Mantis EL 0009964- mantis Jouve 8949
		titreNormalise = titreNormalise.replace(0,"\"","");		
		titreNormalise = char2NoAccent(titreNormalise);
		titreNormalise = trimBoth(titreNormalise," ");
		titreNormalise = titreNormalise.replace(0,G_regexpSpace,"_");
		titreElem = titreNormalise;
		
	}
	// Fin modif YE 26/06/2014 mantis 7230 
	
	// 27/05/2015 MB : 0008849: document Q5F010 ne s'ouvre pas ==> on encode les "
	//                 si c'est pas suffisant, il faut voir pour le faire passer par : titreNormalise ==> char2NoAccent ==> trimBoth " " et replace(0,G_regexpSpace,"_")
	titreElem = titreElem.replace(0,"\"","");
	// 27/05/2015 MB : fin ajout 
	
	// 
	//cout << format("\n--DEBUG MB avant appel  09/12/2015 MB\n");
	
	//23/02/2016 SF ajout pack ope
	var packTitle = nothing;
	if (hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
		var packid = attr["PACKID", racine];
		packTitle = entityHtml2EntityNum(char2entity(getPackTitle(packid)));
	}
	// cout << format("#### debugsf fct make_urf packTitle : %1\n", packTitle);
	var listeRes = hulk_getChaineChapitres(racine,giracine,idracine,idelem,titreElem,codeMatiere,basenameSgm,titreOuv,G_mapJrpinfo,packTitle);
	var chaineChap = listeRes[0];
	titreElem = listeRes[1];	

	// 31/01/2013 MB : ajout element SEO 
	// recuperer les infos du SEO separes par #
	var chaineSEO = hulk_getSEOinfos(giracine,idracine,idelem,titreElem,codeMatiere,basenameSgm);
	//cout << format("+++++++++++++ MB 27/05/15 ++++++++++++++++>> creation urf pour ID = %1 numCD = %2 giracine = %3 chaineChap = %4 \n====>titreElem=%5",idracine,numCD,giracine,chaineChap,titreElem);
	var titleSEO = "", descriptionSEO = "";
	var lst = chaineSEO.explode("@"); // YE 30/10/2013 changement du separateur
	if lst.length() > 1 {
		titleSEO = lst[0];
		descriptionSEO = lst[1];
	}

	if chaineChap != "" {
		gendoc = Node(docxml,"gendoc",false);
		// addAttr(gendoc,"uri",format("EL/%1/%2/%3/#/urf.xml",codeMatiere,typeOuvrage,idracine));
		addAttr(gendoc,"uri",format("%1",sUri));
		// var chaineChap = format("Etudes::Etude_%1::::%2_Etude_%1",titreElem,idracine);
		var chaineChapitres = format("<![CDATA[%1]]>",chaineChap);
		var xtpage = Node(docxml,"xtpage",Node(docxml,"#CDATA",chaineChapitres));
		//cout << format("\n--DEBUG MB du 09/12/2015 MB : ---\nxtpage = %1\n",xtpage);
		var xtn2 = Node(docxml,"xtn2",Node(docxml,"#CDATA",numCD));
		//cout << format("\n-----\nxtn2 = %1",xtn2);
		var xiti = Node(docxml,"xiti",xtn2);
		insertSubTree(xiti,-1,xtpage);
		
		// 18/12/2013 MB : ajout <xtclic pour le LOT2
		// concerne les ET, les Y et les FORM :
		// 24/11/2015 MB : ajout cas FP, BOFIP, DOCAM, FPRO, JRP, TXT
		//if Set("ETD-OPTJ","DPFORM2-OPTJ","DPFORM-OPTJ").knows(giracine) {
		// // les cas a traiter : ADLIST-OPTJ ; CCOL-OPTJ ; CDCOLL ; CODE-OPTJ ; COMJRP-OPTJ ; CORNAFCC-OPTJ ; DECISBLOC-OPTJ ; DOCAM-OPTJ ; DPFORM-OPTJ ; DPFORM2-OPTJ ; ETD ; ETD-OPTJ ; FP-OPTJ ; FPRO-OPTJ ; IDX-OPTJ ; IDXET-OPTJ ; LISTE ; QR-OPTJ ; RECMAJ-OPTJ ; REFDOCINFOS ; SRC ; TABLESOURCES ; TAG-OPTJ ;  TXTBLOC-OPTJ ; 
//
		if Set("ETD-OPTJ","DPFORM2-OPTJ","DPFORM-OPTJ","DECIS-OPTJ","IDX-OPTJ","IDXET-OPTJ","TXT-OPTJ","TXTBLOC-OPTJ","DOCAM-OPTJ","LISTE","FP-OPTJ","FPRO-OPTJ","QR-OPTJ","CODE-OPTJ","CCOL-OPTJ","DECIS-OPTJ","DECISBLOC-OPTJ","COMJRP-OPTJ","TAG-OPTJ","RECMAJ-OPTJ").knows(giracine) { 
		  var chaineClic = hulk_getXtclic(giracine,idracine,idelem,titreElem,codeMatiere,basenameSgm);
		  //	cout << format("\n--- Debug MB 25/11/2015 -- hulk_getXtclic(giracine=%1,idracine=%2,idelem=%3,titreElem=%4,codeMatiere=%5,basenameSgm=%6) == %7\n",giracine,idracine,idelem,titreElem,codeMatiere,basenameSgm,chaineClic);
			var xtclic = Node(docxml,"xtclic",Node(docxml,"#CDATA",chaineClic));
			insertSubTree(xiti,-1,xtclic);
		}


		// YE Le 05/12/2014 
		var bookTypeLabel = Node(docxml,"bookTypeLabel",Node(docxml,"#CDATA",chaineChap.explode("::")[0]));
		insertSubTree(xiti,-1,bookTypeLabel);
		
		var bookLabel = Node(docxml,"bookLabel",Node(docxml,"#CDATA",titreElem));
		insertSubTree(xiti,-1,bookLabel);
		
		var bookID = Node(docxml,"bookID",Node(docxml,"#CDATA",idelem));
		insertSubTree(xiti,-1,bookID);
		


		// 24/04/2013 MB : ajout partie statsPivot pour Dalloz
		// 26/04/2013 MB : ajout de la partie matieres
		//                 on utilise la map : G_mapIdetLstThemesBoVeille initialisee par la fct initmapboveille... (reprise de optj2exalead)
		var matieresDzNode = Node(docxml,"matieres");
		//cout << format("********** test si %1 est ds G_mapIdetLstThemesBoVeille : %1\n",idracine,G_mapIdetLstThemesBoVeille.knows(idracine));
		if G_mapIdetLstThemesBoVeille.knows(idracine) {
			// on extrait la(es) matiere(s) : 1ers elts de : List("Social/Contrat de travail" "Social/Absences - Cong&eacute;s")
			//cout << format("\n**************\nG_mapIdetLstThemesBoVeille.knows(%1)=%2\n*************\n",idracine,G_mapIdetLstThemesBoVeille[idracine]);
			var setMats = Set();
			for elt in G_mapIdetLstThemesBoVeille[idracine] {
				var uneMat = elt;
				if elt.search("/") >= 0 {
					//cout << format("contient slasl \n");
					uneMat = elt.explode("/")[0];
				}
				setMats << uneMat; 
				//cout << format("+++++++++> lstMats = %1\n",setMats);
			}
			// on insere autant de noeuds matiere que d'elts dans lstMats:
			for uneMat in setMats {
				var matiereDzNode = Node(docxml,"matiere",Node(docxml,"#CDATA",uneMat));
				insertSubTree(matieresDzNode,-1,matiereDzNode);
				//cout << format("insertion du neud matiere : %1 dans matieresDzNode : %2 \n",matiereDzNode, matieresDzNode);
			}
		}

		var tOuvrageDzNode = Node(docxml,"typeOuvrage",Node(docxml,"#CDATA",getDzTypeOuvrage(giracine,typeOuvrage)));
		var tOuvragesDzNode = Node(docxml,"typeOuvrages",tOuvrageDzNode);

		var sourceDZNode = Node(docxml,"source",Node(docxml,"#CDATA",getDzSourceName(typeOuvrage)));
		var sourcesDZNode = Node(docxml,"sources",sourceDZNode);  
		
		// ==> dans un 1er tps, on met idem que type ouvrage DZ
		var productCodeNode = Node(docxml,"productCode",Node(docxml,"#CDATA",getDzProductNode(giracine,idracine,sFdoc)));
		// il n y a plus qu'a inserer les elements crees :
		var statPivot = Node(docxml,"statPivot",matieresDzNode);
		insertSubTree(statPivot,-1,tOuvragesDzNode);
		insertSubTree(statPivot,-1,sourcesDZNode);
		insertSubTree(statPivot,-1,productCodeNode);
		// 24/04/2013 MB : fin ajouts MB


		var statistics =  Node(docxml,"statistics",xiti);
		// 24/04/2013 MB : on insere le sous-arbre statPivot cree :
		insertSubTree(statistics,-1,statPivot);

		var urf = Node(docxml,"urf",statistics);

		// 31/01/2013 ajout partie seo
		var pageTitle = Node(docxml,"pageTitle",Node(docxml,"#CDATA",format("<![CDATA[%1]]>",titleSEO)));
		var descriptionMeta = Node(docxml,"descriptionMeta",Node(docxml,"#CDATA",format("<![CDATA[%1]]>",descriptionSEO)));			 
		var seo = Node(docxml,"seo",pageTitle);
		insertSubTree(seo,-1,descriptionMeta);
		insertSubTree(urf,-1,seo);

		insertSubTree(gendoc,-1,urf);
		//cout << format("\n-----\ngendoc = %1",gendoc);
	}

	return gendoc;
}


function getGendocUci(docxml,formt,titreRacine,idracine,idFormt,typeOuvrage,codeMatiere){
	var labelMatiere = "MATIERE";
	if G_ELNETcodeProdShortTitleMap.knows(codeMatiere) labelMatiere = G_ELNETcodeProdShortTitleMap[codeMatiere];

	var gendoc = Node(docxml,"gendoc",false);
	// <gendoc uri="EL/CD02/DPFORM2/Z2M007/#/UCI/Z2M007001">
	addAttr(gendoc,"uri",format("EL/%1/%2/%3/#/UCI/%4",codeMatiere,typeOuvrage,idracine,idFormt));

	var uc = Node(docxml,"uc",false);
	addAttr(uc,"type","document");
	addAttr(uc,"uri",attr["uri",gendoc]);
	insertSubTree(gendoc,-1,uc);

	var donnees = Node(docxml,"donnees",false);
	insertSubTree(uc,-1,donnees);
	// 05/12/2012 AL
	// Pour le titre on indique le titre de l'ancart en contenu
	// + le titre de la formule dans un attribut titre@item
	var titreDonnees = Node(docxml,"titre",Node(docxml,"#CDATA",titreRacine));
	// 11/12/2012 AL : l'ajout de l'attribut titre@item doit se faire uniquement dans hulk/livtest/ et pas sur hulk/liv/
	// 01/07/2013 AL : on passe en prod l'attribut titre@item
	addAttr(titreDonnees,"item",getTitreElem(formt,GI(formt),idFormt));
	insertSubTree(donnees,-1,titreDonnees);

	// 21/12/2012 AL
	// Dans le cas de LP35 le contenu de support est "Support LP"
	//var support = Node(docxml,"support",Node(docxml,"#CDATA","Support DP"));
	var support = Node(docxml,"support",false);
	// 14/12/2012 MB : pour le LP35 attribut code=LP
	//addAttr(support,"code","DP");
	if codeMatiere == "LP35"{
		addAttr(support,"code","LP");
		insertSubTree(support,-1,Node(docxml,"#CDATA","Support LP"));
	}
	else{
		addAttr(support,"code","DP");
		insertSubTree(support,-1,Node(docxml,"#CDATA","Support DP"));
	}
	
	insertSubTree(donnees,-1,support);
	
	var matiere = Node(docxml,"matiere",Node(docxml,"#CDATA","Matiere "+codeMatiere));
	addAttr(matiere,"code",codeMatiere);
	insertSubTree(donnees,-1,matiere);
	//  gendoc@support = gendoc/uc/donnees/support/@code
	//addAttr(
	
	var presentation = Node(docxml,"presentation",false);
	insertSubTree(uc,-1,presentation);

	var action = Node(docxml,"action",false);
	addAttr(action,"type","html-header");
	insertSubTree(presentation,-1,action);

	var parametre = Node(docxml,"parametre",false);
	insertSubTree(action,-1,parametre);

	var linkNode = Node(docxml,"link",true);
	addAttr(linkNode,"rel","stylesheet");
	addAttr(linkNode,"type","text/css");
	addAttr(linkNode,"href",format("@@mgoStatic{uri:'/EL/#TRANSVERSE/www/html/css/el-form_di_ex_%1.css'}",codeMatiere));
	insertSubTree(parametre,-1,Node(docxml,"#CDATA","<![CDATA["));
	insertSubTree(parametre,-1,linkNode);
	insertSubTree(parametre,-1,Node(docxml,"#CDATA","]]>"));

	var bloc = Node(docxml,"bloc",false);
	addAttr(bloc,"type","document");
	insertSubTree(uc,-1,bloc);

	insertSubTree(bloc,-1,Node(docxml,"titre",Node(docxml,"#CDATA","Vous souhaitez")));
	
	var info = Node(docxml,"info",false);
	insertSubTree(info,-1,Node(docxml,"#CDATA","<![CDATA["+labelMatiere+"<br/>"+titreRacine+"]]>"));
	insertSubTree(bloc,-1,info);
	
	var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","<![CDATA[Afficher la version de consultation]]>")));
	addAttr(lien,"info-bulle","Afficher la version de consultation");
	addAttr(lien,"id-picto","consultation");
	addAttr(lien,"cible","vue");
	
	// YE Le 17/02/2014 Mantis 5304 
	// Ajout du picto afficher la version de consultation		
	addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/afficher_la_version_de_consultation.png'}");	
	
	// 24/09/2012 AL
	// On ne precise pas le fdoc de la version interactive vers la version de consultation
	// cf mail armand betton du 20/09/2012 16:14
	addAttr(lien,"url",format("@@exaDoclink{dockey:'%1'}",idFormt));

	insertSubTree(bloc,-1,lien);

	
	/*
	12/12/2012 MB : ajout des attributs suivants aux gendoc :
	gendoc@fid = id de la formule
	gendoc@version = gendoc/form?/@version du fichier .mfi.xml
	gendoc@matiere = gendoc/uc/donnees/matiere/@code
	gendoc@support = gendoc/uc/donnees/support/@code
	gendoc@tit = gendoc/uc/donnees/titre/@item
	gendoc@tuc = "fid"
	*/

	// -------------------------------------
	// 20/03/2014 alazreg
	// mantis 6486 MFI LP35
	// on passe par la cdcoll.sgm + chaine jouve comme les autres produits
	// ne pas ajouter les attributs specifiques au LP35
	// -------------------------------------
	var tuc = "fid";
	if codeMatiere == "LP35"{
		addAttr(gendoc,"fid",idFormt);
		addAttr(gendoc,"version",attr["version",formt]);
		addAttr(gendoc,"matiere",attr["code",matiere]);
		addAttr(gendoc,"support",attr["code",support]);
		addAttr(gendoc,"tit",attr["item",titreDonnees]);
		addAttr(gendoc,"tuc",tuc);
	}

	// fin modifs mb 12/12/2012
	



	return gendoc;
}

function corrigerCaracteres(filenamexml){
	var file = FileStream(filenamexml,"r");
	if file != nothing{
		var sContenu = readAll(file);
		close(file);

		// On remplace "&amp;" par "&"
		var matches = sContenu.rsearch(0,"&amp;([#a-zA-Z0-9]+;)");
		if matches != nothing{
			sContenu = sContenu.mreplace(matches,"&\\1");
		}

		// remplacer <br/> et <img/>
		sContenu = sContenu.replace(0,"&lt;img","<img");
		sContenu = sContenu.replace(0,"&lt;br","<br");
		sContenu = sContenu.replace(0,"/&gt;","/>");
		sContenu = sContenu.replace(0,"&lt;h3&gt;","<h3>");
		sContenu = sContenu.replace(0,"&lt;/h3&gt;","</h3>");
		sContenu = sContenu.replace(0,"&lt;h4&gt;","<h4>");
		sContenu = sContenu.replace(0,"&lt;/h4&gt;","</h4>");

		// YE mantis 5992
		sContenu = sContenu.replace(0,"&lt;/a&gt;","</a>");
		sContenu = sContenu.replace(0,"&lt;a ","<a ");
		
		// remplacer <span style"...">
		/*matches = sContenu.rsearch(0,"&lt;span(.+)&gt;");
	if matches != nothing{
		cout << format("Recherche span : %1 resultats\n",matches.length());
		for m in matches cout << format("m = '%1'\n",m.value);
		sContenu = sContenu.mreplace(matches,"<span\\1>");
		}*/
		sContenu = sContenu.replace(0,"&lt;span","<span");
		sContenu = sContenu.replace(0,"\"&gt;","\">");
		sContenu = sContenu.replace(0,"&lt;/span&gt;","</span>");

		// remplacer <![CDATA[...]]>
		sContenu = sContenu.replace(0,"&lt;!\\[CDATA\\[","<![CDATA[");
		sContenu = sContenu.replace(0,"]]&gt;","]]>");

		var file2 = FileStream(filenamexml,"w");
		file2 << sContenu;
		close(file2);
	}
	
	return 0;
}

function make_mfi_new(){
	// ---------------------------------------------------------------------------
	// Cette fonction genere les fichier *.mfi.xml a partir des fichiers 
	// ./elnet/liv/xml/*.tmp.xml
	// 05/12/2012 al
	// Cette version construit les uci.xml en meme temps que les mfi.xml
	// ---------------------------------------------------------------------------

	//var dirXmlIn = filePath(env("ELA_CDROM"),"elnet","liv","xml");
	var dirXmlIn = filePath(G_ela_liv,"xml");

	// 10/12/2012 AL
	// On genere les *.mfi.xml *.uci.xml dans le dossier en entree $ELA_LIV/xml
	// Cela permet de rester coherent avec les fichiers *.(uc|ucx|urx).xml qui sont generes dans le dossier $ELA_LIV/sgm
	// 
	//var dirXmlOut = filePath(G_ela_liv,"xml","mfi");
	var dirXmlOut = filePath(G_ela_liv,"xml");

	if fileType(dirXmlOut) != "dir" makeDir(dirXmlOut);

	//var regexp = RegExp("z9m01.+\\.tmp\\.xml$");
	var regexp = RegExp("\\.tmp\\.xml$");
	var racine = nothing;
	var filexmlOutMfi = nothing;
	var filexmlOutUci = nothing;
	var file = nothing;
	var docxml = newCoreDocument();
	var ret = nothing;
	var codeMatiere = nothing;

	var giracine,idracine,titreRacine,sMatiere,typeOuvrage,idFormt,gendocNodeMfi,gendocNodeUci;

	for filexml in iSort(fileReadDir(dirXmlIn,true)){
		if filexml.rsearch(0,regexp)== nothing continue;
		//if fileBaseName(filexml).search(0,"w5m") >=0 cout << format("traitement %1\n",filexml);
		
		ret = parseDocument(filexml, ThisProcess, Map("generator", "xml","keep",true));
		if ret.status != 0{
			cout << format("ERREUR PARSE %1\n",filexml);
			continue;
		}
		
		//racine = Node(docxml,"gendocs",false);
		docxml = ret.document;
		racine = root(docxml);
		giracine = GI(racine);
		idracine = attr["id",racine].transcript(UpperCase);
		titreRacine = content(enclosed(racine,"ti0"));
		
		sMatiere = "MATIERE";
		//if hasAttr(racine,"MATIERE") sMatiere = attr["MATIERE",racine];
		sMatiere = getCodeProdJouve(idracine.extract(0,2));
		// 22/11/2012
		if sMatiere == "CD35" sMatiere = "LP35";
		codeMatiere = sMatiere;
		
		typeOuvrage = getTypeOuvrage(sMatiere,giracine, idracine,"");
		
		var gendocsNodeMfi = Node(docxml,"gendocs",false);
		var gendocsNodeUci = Node(docxml,"gendocs",false);
		
		var dateAAAAMMJJ = "vide";
		var attrDatExplosed = List();

		// 22/11/2012 MB : les xml du lp35 contiennent des refid au lieu des id:
		//                 ==> les .mfi crees sont vides !
		//                 solution : ajouter l'atribut id (identique au refid)
		//for formt in searchElemNodes(racine,List("formt1","formt2","formt3","formt4","formt5","formt6","formt7"),"id",nothing){
		for formt in searchElemNodes(racine,List("formt1","formt2","formt3","formt4","formt5","formt6","formt7"),nothing,nothing){  
			// ajout MB : si pas de ID et pas de REFID
			if !hasAttr(formt,"id") && ! hasAttr(formt,"refid") continue;
			if !hasAttr(formt,"id") && hasAttr(formt,"refid") addAttr(formt,"id",attr["refid",formt]);
			else if !hasAttr(formt,"refid") && hasAttr(formt,"id") addAttr(formt,"refid",attr["id",formt]);
			// fin ajout MB
			
			//if fileBaseName(filexml).search(0,"w5m") >=0 
			//cout << format("Je traite fichier : %1 formt : %2\n",fileBaseName(filexml),echo(formt));
			idFormt = attr["id",formt].transcript(UpperCase);

			// deplacee de plus bas 
			if !hasAttr(formt,"version"){
				if hasAttr(formt,"dat"){
					attrDatExplosed = attr["dat",formt].explode("/");
					dateAAAAMMJJ = format("%1%2%3",attrDatExplosed[2],attrDatExplosed[1],attrDatExplosed[0]);
				}
				else dateAAAAMMJJ = "vide";

				addAttr(formt,"version",dateAAAAMMJJ);
			}	  

			// 05/12/2012 AL
			// On appelle ici la fonction qui genere les gendoc pour les uci
			gendocNodeUci = getGendocUci(docxml,formt,titreRacine,idracine,idFormt,typeOuvrage,sMatiere);
			if gendocNodeUci != nothing insertSubTree(gendocsNodeUci,-1,gendocNodeUci);

			gendocNodeMfi = Node(docxml,"gendoc",false);
			addAttr(gendocNodeMfi,"uri",format("EL/%1/%2/%3/#/FORM/%4",sMatiere,typeOuvrage,idracine,idFormt));
			
			/*
		12/12/2012 MB : ajout des attributs suivants aux gendoc mfi :
		gendoc@fid = id de la formule
		gendoc@version = gendoc/form?/@version du fichier .mfi.xml
	*/

			// -------------------------------------
			// 20/03/2014 alazreg
			// mantis 6486 MFI LP35
			// on passe par la cdcoll.sgm + chaine jouve comme les autres produits
			// ne pas ajouter les attributs specifiques au LP35
			// -------------------------------------
			var tuc = "fid";
			if codeMatiere == "LP35"{
				addAttr(gendocNodeMfi,"fid",idFormt);
				addAttr(gendocNodeMfi,"version",attr["version",formt]);
			}
			// fin ajouts mb 12/12/2012

			surroundSubTree(formt,1,gendocNodeMfi);
			insertSubTree(gendocsNodeMfi,-1,gendocNodeMfi);
		}
		
		filexmlOutMfi = filePath(dirXmlOut,fileBaseName(filexml).explode(".")[0]+".mfi.xml");
		file = FileStream(filexmlOutMfi,"w");
		//cout << format("Je tente d'ecrire %1\n",echo(racine));
		if file != nothing{
			// if fileBaseName(filexml).search(0,"w5m") >=0 cout << format("ecriture %1\n",filexmlOutMfi);
			file << G_enteteXml+"\n";
			dumpSubTree(gendocsNodeMfi,file,Map("FORMAT","XML"));
			close(file);
			
			// iconv -f ISO8859-1 -t UTF-8 $f > ! $f.iconv
			system(format("iconv -f ISO8859-1 -t UTF-8 %1 > %1.iconv",filexmlOutMfi));
			fileRename(filexmlOutMfi+".iconv",filexmlOutMfi);
		}
		else cout << format("Probleme de creation %1\n",filexmlOutMfi);
		
		if children(gendocsNodeUci).length() >0{
			filexmlOutUci = filePath(dirXmlOut,fileBaseName(filexml).explode(".")[0]+".uci.xml");
			file = FileStream(filexmlOutUci,"w");
			//cout << format("Je tente d'ecrire %1\n",echo(racine));
			if file != nothing{
				// if fileBaseName(filexml).search(0,"w5m") >=0 cout << format("ecriture %1\n",filexmlOutUci);
				file << G_enteteXml+"\n";
				dumpSubTree(gendocsNodeUci,file,Map("FORMAT","XML"));
				close(file);
				
				corrigerCaracteres(filexmlOutUci);
				
				system(format("iconv -f ISO8859-1 -t UTF-8 %1 > %1.iconv",filexmlOutUci));
				fileRename(filexmlOutUci+".iconv",filexmlOutUci);
			}
			else cout << format("Probleme de creation %1\n",filexmlOutUci);
		}
	}
	
	return 0;
}

function getCodeMatiere(node){
	var codeMatiere = "MATIERE";
	if hasAttr(node,"MATIERE") codeMatiere = attr["MATIERE",node];
	if codeMatiere == "ELNET" codeMatiere = "SOURCES";
	return codeMatiere;
}


function make_mapTxtVersion(){
	// Cette fonction prepare une Map pour le versionning des textes.
	
	cout << "function make_mapTxtVersion\n";
	
	
	G_mapTxtMDF =nothing;
	var filenameMap = filePath(G_ela_liv,"miscdata","mapMDF.map");
	G_mapTxtMDF = loadObjectFromFile(filenameMap);
	if G_mapTxtMDF == nothing || !isaMap(G_mapTxtMDF){
		G_mapTxtMDF = Map();
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		return 1;
	}

	// 07/10/2013 ajoust MB : on charge la map mapTxtVersionning car necessaire pour corriger le pb des versions anterieurs des txt
	filenameMap = filePath(G_ela_liv,"miscdata","mapTxtversionning.map");
	G_maptxtversionning = loadObjectFromFile(filenameMap);
	if G_maptxtversionning == nothing || !isaMap(G_maptxtversionning){
		G_maptxtversionning = Map();
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		cout << format("PROBLEME PROBLEME avec lecture %1\n",filenameMap);
		return 1;
	}
	// fin ajouts MB
	
	return 0;
}

function make_urx(docxml,elem2,gielem2,giracine,codeMatiere,typeOuvrage,idelem,titreLien,sFdoc){

	// -----------------------------------------------------------------------
	// Cette fonction cree les papidoc URX
	// Les URX viennent completer les papidoc UR des sources (txt, codes, jrp)
	// Les papidoc UR sont genere par le programme optj2exalead_hulk.bal
	// -----------------------------------------------------------------------
	// 17/09/2012 AL
	// cf mail Yann 13/09/2012 18:01
	// - supprimer l'ajout via les URX de la meta fdoc EL/CDXX/... sur les UR de codes (secs/full/enrichis) + jrp ?
	// - supprimer l'ajout dans les UR de textes EL de la même meta fdoc ?
	// -----------------------------------------------------------------------

	//cout << format("DEBUGAL je suis dans la fonction make_urx pour %1\n",gielem2);

	// uri-cible=DZ/CODES/CASF/#/UC/CODE_CASF_ARTI_L121-3
	var sUriPapidoc = "";
	var idcodeDz = "";
	var sFdocCible = "";

	if gielem2 == "XACODE"{
		idcodeDz = attr["CODE",elem2];
		// 17/04/2013 MB : maintenant pour transformer en code Dalloz, 2 conditions :
		// 1) le code doit avoir une correspondancee Dalloz
		// 2) le code ne doit pas etre dans le set des codes a maintenir en EL :  setCodesANePasPatcher     
		// 
		if !setCodesANePasPatcher.knows(attr["CODE",elem2]) && mapCorrespCodesEL_DZ.knows(attr["CODE",elem2]) idcodeDz = mapCorrespCodesEL_DZ[attr["CODE",elem2]];
		// uri-cible=DZ/CODES/CASF/#/UC/CODE_CASF_ARTI_L122-2
		sUriPapidoc = format("DZ/CODES/%1/#/URX/%2",idcodeDz,attr["IDREF",elem2]);

		// -----------------------------------------------------------------------
		// 20/08/2013 alazreg
		// le chemin uri est different pour les codes EL
		// -----------------------------------------------------------------------
		if setCodesANePasPatcher.knows(attr["CODE",elem2]) sUriPapidoc = format("EL/SOURCES/CODE/%1/#/URX/%2",idcodeDz,attr["IDREF",elem2]);

		sFdocCible = format("FDOC/EL/%1/CODES",codeMatiere);
		
	}
	
	if gielem2 == "JRP"{
		// uri-cible=DZ/JURISPRUDENCE/CAA_NANTES_20091109_09NT00132/#/UC
		// 22/06/2012 AL/MB
		// Voir mail de Manuel GONCALVES du 31/05/2012 15:46
		// Dans le cas des JRP remplacer URX par UDX
		//sUriPapidoc = format("DZ/JURISPRUDENCE/%1/#/URX",attr["IDREF",elem2]);
		// Mantis 8677
		// sUriPapidoc = format("DZ/JURISPRUDENCE/%1/#/UDX",attr["IDREF",elem2]);
		// 20/08/2012 AL/MB
		// Dans le cas des jrp EL ilfaut pointer sur une URI EL
		// 03/10/2012 AL/MB
		// if G_setIdjrpToBuild.knows(attr["IDREF",elem2]){
			// -----------------------------------------------------------------------
			// 25/06/2013 alazreg
			// demande de jouve de remplacer UDX par URX pour les jrp EL
			// -----------------------------------------------------------------------
			// 08/07/2013 alazreg
			// ajouter idjrp apres URX/
			// -----------------------------------------------------------------------
			//sUriPapidoc = format("EL/SOURCES/JRP/%1/%2/#/UDX",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
			//sUriPapidoc = format("EL/SOURCES/JRP/%1/%2/#/URX",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
			// sUriPapidoc = format("EL/SOURCES/JRP/%1/%2/#/URX/%2",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
		// }      
		// YE le 30/09/2014 Mantis 8677
		if G_map_id_logique_jrp_dalloz == nothing G_map_id_logique_jrp_dalloz = get_map_id_logique_jrp_dalloz();
		// 22/03/2017 alazreg simplification du test pour DZ ID LOGIQUE
		// if !G_map_id_logique_jrp_dalloz.knows(attr["IDREF",elem2].transcript(UpperCase)) && !G_mapJrpinfo.knows(attr["IDREF",elem2].transcript(LowerCase))
		if G_setAllDZJrp.knows(attr["IDREF",elem2]){			
			sUriPapidoc = format("DZ/JURISPRUDENCE/%1/#/UDX",attr["IDREF",elem2]);					
		}else if G_mapJrpinfo.knows(attr["IDREF",elem2].transcript(LowerCase)){
			sUriPapidoc = format("EL/SOURCES/JRP/%1/%2/#/URX/%2",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
		}	
		sFdocCible = format("FDOC/EL/%1/JRP",codeMatiere);
	}
	
	if gielem2 == "TJR"{
		// uri-cible=EL/SOURCES/TXT/ELNETTXTBLOC200607/T175499/#/UC/T175499
		sUriPapidoc = format("EL/SOURCES/TXT/%1/%2/#/URX/%2",getTxtBlocName(attr["IDREF",elem2],G_maptxtdata),attr["IDREF",elem2]);
		sFdocCible = format("FDOC/EL/%1/TXT",codeMatiere);
	}
	
	var papi_docNode = Node(docxml,"PAPI_document",false);
	addAttr(papi_docNode,"uri",sUriPapidoc);
	
	// -----------------------------------------------------------------------
	// 17/09/2012 AL
	// cf mail Yann 13/09/2012 18:01
	// -----------------------------------------------------------------------
	//var papi_metaNode = nothing;
	var papi_metaNode = Node(docxml,"PAPI_meta",false);
	addAttr(papi_metaNode,"name","classement");
	//insertSubTree(papi_docNode,-1,papi_metaNode);
	if papi_docNode != nothing insertSubTree(papi_docNode,-1,papi_metaNode);
	//insertSubTree(papi_metaNode,-1,Node(docxml,"#CDATA",format("%1",sFdocCible)));
	var sSearchScope = sFdocCible.replace(0,"FDOC/","SEARCH-SCOPE/");
	insertSubTree(papi_metaNode,-1,Node(docxml,"#CDATA",format("%1",sSearchScope)));

	// YE 02/07/2014
	// Mantis 7258
		
	if giracine == "ETD-OPTJ" && codeMatiere == "CD05" && (gielem2 == "TJR" || gielem2 == "JRP"||(gielem2 == "XACODE" && attr["CODE",elem2]!= "BOFIP")){
		var mapBlocsNameRjOfOuvrageId = "";
		// charger la map qu'une fois 
		if G_idelemlast != idelem {		
			G_idelemlast = idelem;
			// G_mapBlocsNameRjOfOuvrageId = getMapBlocsNameRjOfOuvrageId(idelem);		
			G_mapBlocsNameRjOfOuvrageId = Map();
			var blocnum = "";
			for elem in G_mapBlocsRj {
				blocnum = elem;
				for elem2 in G_mapBlocsRj[elem]{
					if G_mapBlocsRj[elem][elem2].knows(idelem.transcript(UpperCase)) {				
						blocnum = format("RJ%1",elem);
						G_mapBlocsNameRjOfOuvrageId[blocnum] = elem2;
					}
				}	
			}			
			//cout << format("G_mapBlocsNameRjOfOuvrageId - %1 \n",G_mapBlocsNameRjOfOuvrageId);
		}
		var sSearchScopeDj = "";		
		for bloc in G_mapBlocsNameRjOfOuvrageId {
			var papi_metaNode = Node(docxml,"PAPI_meta",false);
			addAttr(papi_metaNode,"name","classement");			
			if papi_docNode != nothing insertSubTree(papi_docNode,-1,papi_metaNode);			
			sSearchScopeDj = format("SEARCH-SCOPE/EL/%1/%2",codeMatiere,bloc) ;			
			insertSubTree(papi_metaNode,-1,Node(docxml,"#CDATA",format("%1",sSearchScopeDj)));			
		}		
	}

	

	// -----------------------------------------------------------------------
	// Ajouter les thematiques hulk sur les etudes
	// -----------------------------------------------------------------------
	// 26/02/2013 alazreg
	// Vu avec Yann
	// Ne plus generer les thematiques pour les sources dans les urx
	// A faire en test d'abord puis en prod
	// -----------------------------------------------------------------------
	// G_mapMeta["document-id"] = Z9001
	
	var setThmesDejaInseres = Set();


	// 20/07/2012 AL
	// Dans le cas des codes Dalloz on duplique le noeud papidoc pour les codes secs
	// 01/08/2012 AL
	// On duplique aussi pour la partie enrichie des codes
	// -----------------------------------------------------------------------
	// 20/08/2013 ALAZREG
	// Ne pas faire le traitement dessous pour les codes EL
	// ajout de la condition !setCodesANePasPatcher.knows(attr["CODE",elem2])
	// -----------------------------------------------------------------------
	var listePapidoc = List(papi_docNode);
	if gielem2 == "XACODE" && !setCodesANePasPatcher.knows(attr["CODE",elem2]){
		for e in List("CODES-SECS","CODES-ENRICHIS"){
			var papidocCodeSec = copy(papi_docNode);
			attr["uri",papidocCodeSec] = attr["uri",papidocCodeSec].replace(0,"CODES",e);
			listePapidoc << papidocCodeSec;
		}
	}
	return listePapidoc;
}

var G_mapUcxCompteur = Map();

function make_ucx(racine,docxml,elem2,gielem2,idracine,giracine,codeMatiere,typeOuvrage,idelem,titreLien,sFdoc){
	// Ici elem2 est le renvoi XACODE TJR ou JRP
	// Cette fonction cree un noeud <gendoc> pour les UCX liens remontants
	//cout << "\nJe suis dans la fonction make_ucx\n";
	//cout << format("giracine = %1,gielem2 = %2, idelem = %3\n",giracine,gielem2,idelem);

	// 20/08/2012 AL
	// Mettre en place un compteur pour les UCX
	// Cela permet d'avoir des URI uniques pour les  UCX et facilite la fusion chez Jouve

	if !G_mapUcxCompteur.knows(idracine) G_mapUcxCompteur[idracine] = 1;
	else G_mapUcxCompteur[idracine]+=1;
	// YE 05/03/2014 Mantis 6404
	// var sUriUcx = format("EL/%1/%2/%3/#/UCX/%4",codeMatiere,typeOuvrage,idelem,G_mapUcxCompteur[idracine]);
	// Dans le cas de la racine => il ne faut pas rajouter idelem
	// YE 01/10/2014 Mantis 0008676, corriger la forme de l'URI pour les fp, fpro, dpform des GB
	// var sUriUcx = format("EL/%1/%2/%3/#/UCX/%4",codeMatiere,typeOuvrage,idelem,G_mapUcxCompteur[idracine]);
	// if idracine != idelem sUriUcx = format("EL/%1/%2/%3/%4/#/UCX/%5",codeMatiere,typeOuvrage,idracine,idelem,G_mapUcxCompteur[idracine]);
	
	var sUriUcx = "";
	// YE Le 09/10/2014 Mantis 8676	
	if Set("FP-OPTJ","FPRO-OPTJ","DPFORM2-OPTJ").knows(giracine){
		sUriUcx = format("EL/%1/%2/%3/#/UCX/%4",codeMatiere,typeOuvrage,idelem,G_mapUcxCompteur[idracine]);
		if idracine != idelem sUriUcx = format("EL/%1/%2/%3/#/UCX/%5/%4",codeMatiere,typeOuvrage,idracine,idelem,G_mapUcxCompteur[idracine]);
	}else {
		sUriUcx = format("EL/%1/%2/%3/#/UCX/%4",codeMatiere,typeOuvrage,idelem,G_mapUcxCompteur[idracine]);
	if idracine != idelem sUriUcx = format("EL/%1/%2/%3/%4/#/UCX/%5",codeMatiere,typeOuvrage,idracine,idelem,G_mapUcxCompteur[idracine]);
	}
	
	// 18/05/2016 ajout pack ope mantis 13343
	if (giracine == "FP-OPTJ" && hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
		sUriUcx = format("EL/PACKOPE/%1/FP/%2/#/UCX/%3",attr["PACKID", racine],idelem,G_mapUcxCompteur[idracine]);
	}
	
	var uriCible = "";
	var idcodeDz = "";
	
	var gendoc = Node(docxml,"gendoc",false);
	// YE le 30/09/2014
	//Ajouter le control sur l'uricible
	var buriCible = false;

	if gielem2 == "XACODE"{
		idcodeDz = attr["CODE",elem2];
		// 17/04/2013 MB : maintenant pour transformer en code Dalloz, 2 conditions :
		// 1) le code doit avoir une correspondancee Dalloz
		// 2) le code ne doit pas etre dans le set des codes a maintenir en EL :  setCodesANePasPatcher     
		// 
		if !setCodesANePasPatcher.knows(attr["CODE",elem2]) && mapCorrespCodesEL_DZ.knows(attr["CODE",elem2]) idcodeDz = mapCorrespCodesEL_DZ[attr["CODE",elem2]];
		// uri-cible=DZ/CODES/CASF/#/UC/CODE_CASF_ARTI_L122-2
		uriCible = format("DZ/CODES/%1/#/UC/%2",idcodeDz,attr["IDREF",elem2]);

		// -----------------------------------------------------------------------
		// 20/08/2013 alazreg
		// le chemin uri est different pour les codes EL
		// -----------------------------------------------------------------------
		if setCodesANePasPatcher.knows(attr["CODE",elem2]) uriCible = format("EL/SOURCES/CODE/%1/#/UC/%2",idcodeDz,attr["IDREF",elem2]);
	}
	
	if gielem2 == "JRP"{
		// Debut commentaire pour la fiche 8179
	//	uriCible = format("DZ/JURISPRUDENCE/%1/#/UC",attr["IDREF",elem2]);
		//	20/08/2012 AL/MB
		//	Dans le cas des jrp EL ilfaut pointer sur une URI EL
		//	03/10/2012 AL/MB
	//	if G_setIdjrpToBuild.knows(attr["IDREF",elem2]){
	//		uriCible = format("EL/SOURCES/JRP/%1/%2/#/UC",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
	//	}   
		// Fin commentaire pour 8179
		// YE Le 29/09/2014 mantis 8179 GP69
		// on initialise la map des jrp dalloz au premier appel
		if G_map_id_logique_jrp_dalloz == nothing G_map_id_logique_jrp_dalloz = get_map_id_logique_jrp_dalloz();
		// cout << format("La taile de la map est '%1' \n",G_map_id_logique_jrp_dalloz.length());
		// cout << format("attr[IDREF] = '%1'\n", attr["IDREF",elem2]);
		if !G_map_id_logique_jrp_dalloz.knows(attr["IDREF",elem2].transcript(UpperCase)) && !G_mapJrpinfo.knows(attr["IDREF",elem2].transcript(LowerCase)){
		// uri-cible=DZ/JURISPRUDENCE/CAA_NANTES_20091109_09NT00132/#/UC
		uriCible = format("DZ/JURISPRUDENCE/%1/#/UC",attr["IDREF",elem2]);
			buriCible = true;
		}else if G_mapJrpinfo.knows(attr["IDREF",elem2].transcript(LowerCase)){
			uriCible = format("EL/SOURCES/JRP/%1/%2/#/UC",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
			buriCible = true;					
		}   
	// cout << format("*********\n");
	// cout << format("Le bloc du jrp est : '%1' \n",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo));
	// cout << format("uriCible : '%1' \n",uriCible);
	
		if G_setAllDZJrp.knows(attr["IDREF",elem2]) uriCible = format("DZ/JURISPRUDENCE/%1/#/UC",attr["IDREF",elem2]);
		else if G_mapJrpinfo.knows(attr["IDREF",elem2].transcript(LowerCase)) uriCible = format("EL/SOURCES/JRP/%1/%2/#/UC",getJrpBlocId(attr["IDREF",elem2],G_mapJrpinfo),attr["IDREF",elem2]);
	}
	
	if gielem2 == "TJR"{
		// uri-cible=EL/SOURCES/TXT/ELNETTXTBLOC200607/T175499/#/UC/T175499
		// 16/07/2012 MB : mail AB jouve : "on est passe d'UC de type "sgml-id", avec des URI en T197227/#/UC/T197227 
		// à des UC de type "ouvrage" avec des URI en T197227/#/UC ; l'URI cible donnee en exemple n'existe plus".
		//uriCible = format("EL/SOURCES/TXT/%1/%2/#/UC/%2",getTxtBlocName(attr["IDREF",elem2]),attr["IDREF",elem2]);
		uriCible = format("EL/SOURCES/TXT/%1/%2/#/UC",getTxtBlocName(attr["IDREF",elem2],G_maptxtdata),attr["IDREF",elem2]);
	}
	
	addAttr(gendoc,"uri",sUriUcx);
	addAttr(gendoc,"uri-cible",uriCible);

	var uc = Node(docxml,"uc",false);
	// YE 11/03/2014 Mantis 6404
	// L utilisation d UC de type "document" peut maintenant etre utilise a la place du type "sgml-id"
	// addAttr(uc,"type","sgml-id");
	addAttr(uc,"type","document");
	// 11/07/2012 MB : mettre type ouvrage pour les txt/jrp
	// 
	if (gielem2 == "JRP" || gielem2 == "TJR") addAttr(uc,"type","ouvrage");

	// 09/05/2014 YE/MB : les liens remontants ne marchent pas pour les codes (a partir des etudes)
	// on change le type dans les blocs uc de l'ucx de l'etude
	// if gielem2 == "XACODE" addAttr(uc,"type","ouvrage");
	// ----------------------------------------------------
	// 15/05/2014 alazreg
	// vu avec armand il faut indiquer sgml-id pour les articles de codes
	if gielem2 == "XACODE" addAttr(uc,"type","sgml-id");
	

	addAttr(uc,"uri",attr["uri-cible",gendoc]);
	insertSubTree(gendoc,-1,uc);

	var bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Voir aussi")));
	addAttr(bloc,"type","voir_aussi");
	insertSubTree(uc,-1,bloc);
	
	var liens = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Commentaires")));
	addAttr(liens,"info-bulle","Commentaires");
	
	// YE Le 17/02/2014 Mantis 5304 
	// Ajout du picto commentaires	
	//addAttr(liens,"id-picto","liencommentaire");	
	addAttr(liens,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/commentaires.png'}");		
	insertSubTree(bloc,-1,liens);
 
	// 18/10/2012 MB :
	// deplace apres le calcul du pnum
	// var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",titreLien)));
	// addAttr(lien,"cible","vue");
	// addAttr(lien,"info-bulle","");

	//var idPnumOuFp = "vide";
	var idPnumOuFp = idracine;
	if giracine == "ETD-OPTJ"{
		var pnum = ancestor(elem2,"PNUM");
		if pnum != nothing && hasAttr(pnum,"ID"){
			idPnumOuFp = attr["ID",pnum];
			// 18/10/2012 MB : ajouter pnum au titre de l'etude (commentaire partie droite)	
			if pnum != nothing {
				var nodeNA = enclosed(pnum,"NA");
				if nodeNA != nothing {
					if hasAttr(nodeNA,"NUM") {
						titreLien = format("%1 - n&#176; %2",titreLien,attr["NUM",nodeNA]);
						// 19/10/2012 ajouter titre pnum pour avoir : Nom de l'etude  no xxx  libelle du pnum
						var tiNA = enclosed(pnum,"TI");
						var sTi = "";
						if tiNA != nothing 
						sTi = content(tiNA);
						titreLien = format("%1 - %2",titreLien,sTi);
					}
					
				}
				// fin ajout MB
		}
	}
		// YE Le 12/11/2015
		// Mantis 12165
	
		var annexe = ancestor(elem2,"AN");
		if annexe != nothing && hasAttr(annexe,"TYPE","AN"){
			var titleAnnexe = content(child(ancestor(elem2,"AN"), "T-BEF"));
			idPnumOuFp = attr["ID",annexe];		
			titreLien = format("%1 %2",titreLien,titleAnnexe);			
		}
	}	
	// YE/AL 16/01/2014 Passer en prod 30/06/2014 + Mantis 3648
	// Mantis 5802 renvois commentaires dans JRP KO
	// Modifier la valeur de l id dans le lien vers les FPBLOC 
	// --------------------------------------------------------------
	// 25/08/2015 alazreg mantis 11229
	// pour les QRBLOC pointer vers la QR et non vers la racine QRBLOC
	// if giracine == "FP-OPTJ"
	if Set("FP-OPTJ","QR-OPTJ").knows(giracine){
		//cout << format("gielem 2 '%1' - sUriUcx ='%2'\n", idelem,uriCible);
		idPnumOuFp = idelem; //attr["ID",anc];//idelem;
	}

	var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",titreLien.transcript(g_map_entites_nommees_to_unicode))));
	addAttr(lien,"cible","vue");
	addAttr(lien,"info-bulle","");




	// 17/08/2012 AL
	// Dans les cas autres que PNUM il faut recuperer un IDREF
	// Sinon on obtient dockey:'vide'
	//var listeAncestors = ancestors(elem2,List("ETD-OPTJ","PNUM","FP-OPTJ","QR-OPTJ","FPRO-OPTJ"));

	addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",idPnumOuFp,sFdoc));
	insertSubTree(liens,-1,lien);
	
	//cout << format("Je cree %1\n",echo(gendoc));
	
	// 20/07/2012 AL
	// Dans le cas des codes Dalloz on duplique le noeud papidoc pour les codes secs
	// 01/08/2012 AL
	// On duplique aussi pour la partie enrichie des codes
	// -----------------------------------------------------------------------
	// 20/08/2013 ALAZREG
	// Ne pas faire le traitement dessous pour les codes EL
	// ajout de la condition !setCodesANePasPatcher.knows(attr["CODE",elem2])
	// -----------------------------------------------------------------------
	var listeGendoc = List(gendoc);
	if gielem2 == "XACODE" && !setCodesANePasPatcher.knows(attr["CODE",elem2]){
		for e in List("CODES-SECS","CODES-ENRICHIS"){
			var gendocCode = copy(gendoc);
			attr["uri-cible",gendocCode] = attr["uri-cible",gendocCode].replace(0,"CODES",e);
			// YE Le 09/10/2014 Mantis 8676
			//attr["uri",gendocCode] = attr["uri",gendocCode]+e;
			if Set("FP-OPTJ","FPRO-OPTJ","DPFORM2-OPTJ").knows(giracine){	
				var matches = attr["uri",gendocCode].rsearch(0,RegExp("#/UCX/[0-9]+"));
				var valuem = "";
				if matches != nothing {
					valuem =  matches[0].value;				
					attr["uri",gendocCode] = attr["uri",gendocCode].replace(0,RegExp("#/UCX/[0-9]+"),format("%1%2",valuem,e));							
				}				
			} else attr["uri",gendocCode] = attr["uri",gendocCode]+e;
			
			var uc = child(gendocCode,"uc");
			if uc != nothing{
				attr["uri",uc] = attr["uri-cible",gendocCode];
			}
			listeGendoc << gendocCode;
		}
	}

	//return gendoc;
	return listeGendoc;
}

function getLabelMatiere(codeMatiere){
	var slabel = "";
	if G_ELNETcodeProdShortTitleMap.knows(codeMatiere) slabel = G_ELNETcodeProdShortTitleMap[codeMatiere];
	return slabel;
}

function get_editeur(racine){
	// 24/07/2014 alazreg
	// mantis 7241 formules dalloz
	var editeur = "EL";
	if hasAttr(racine,"VERSION") && attr["VERSION",racine].transcript(UpperCase) == "DZ" editeur = "DZ";
	return editeur;
}

function get_lib_matiere(racine){
	var libMatiere = "Sources";
	var codeMatiere = attr["MATIERE",racine];
	if G_ELNETcodeProdShortTitleMap.knows(codeMatiere) libMatiere = G_ELNETcodeProdShortTitleMap[codeMatiere];
	if get_editeur(racine) == "DZ"{
		if (attr["ID",racine].search(0,"FORMPCIV")==0) libMatiere = "Formulaire de proc&eacute;dure civile";
	}
	return libMatiere;
}

function creerTitre1(docxml,racine,idracine,giracine,codeMatiere){
	// 11/03/2013
	// cette fonction cree un element uc/donnees/titre1 qui sera utilise
	// pour les documents el dans dalloz.fr

	var titre1 = nothing;
	// 17/03/2014 alazreg
	// mantis 3085 : ajout cas TAG-OPTJ

	if Set("TAG-OPTJ","ETD-OPTJ","DPFORM2-OPTJ","DPFORM-OPTJ","LISTE").knows(giracine){
		var slabel = "";
		if (giracine == "ETD-OPTJ") slabel = "&Eacute;tudes";
		if (giracine == "LISTE" && idracine.rsearch(0,"LSTET$") != nothing) slabel = "&Eacute;tudes";
		if (giracine == "TAG-OPTJ" && idracine.rsearch(0,"RECAPNEW") == nothing) slabel = "TAG";

		// 13/09/2013 MB : modifier titre1 pour avoir Modeles pour Dalloz (pour les portail EL, c'est fait ailleurs)
		// if (giracine == "DPFORM2-OPTJ") slabel = "Formulaires";
		// if (giracine == "DPFORM-OPTJ") slabel = "Formulaires";
		if (giracine == "DPFORM2-OPTJ") slabel = "Mod&egrave;les";
		if (giracine == "DPFORM-OPTJ") slabel = "Mod&egrave;les";

		slabel << " &mdash; ";
		slabel << "Dictionnaire permanent";
		slabel << " ";
		slabel << getLabelMatiere(codeMatiere);

		// 28/07/2014 alazreg
		// mantis 7241 formules dalloz
		// titre1 specifique pour formules dalloz
		if giracine == "DPFORM2-OPTJ" && get_editeur(racine) == "DZ" slabel = get_lib_matiere(racine);

		titre1 = Node(docxml,"titre1",false);
		insertSubTree(titre1,-1,Node(docxml,"#CDATA","<![CDATA["));
		insertSubTree(titre1,-1,Node(docxml,"#CDATA",slabel));
		insertSubTree(titre1,-1,Node(docxml,"#CDATA","]]>"));
	}

	if (giracine == "TAG-OPTJ" && idracine.rsearch(0,"RECAPNEW") != nothing) titre1 = nothing;

	return titre1;
}

function creerBlocDonnees(docxml,codeMatiere,typeOuvrage,racine,idracine,giracine,idelem,gielem,titreElem,filenametoc){
	var donnees = Node(docxml,"donnees",Node(docxml,"titre",Node(docxml,"#CDATA",titreElem)));

	// ------------------------------------------------------------------
	// 11/03/2013 alazreg
	// ajouter un element uc/donnees/titre1 pour les document el dans dalloz.fr
	// ------------------------------------------------------------------

	var titre1 = creerTitre1(docxml,racine,idracine,giracine,codeMatiere);
	if titre1 != nothing insertSubTree(donnees,-1,titre1);
	
	// ------------------------------------------------------------------
	// Declarer une TOC si elle existe
	// La TOC sera inseree a gauche
	// ------------------------------------------------------------------
	//cout << format("DEBUGAL Dans bloc creerBlocDonnees ==> filenametoc = %1\n",filenametoc);
	// 12/07/2012 MB : il ne rentre jammais dans le if !
	// il manque le path au fichier !

	////////////////////////////////////////////////
	// 22/03/2013 alazreg
	// en mode debug on utilise parfois des dossiers temporaires dans lesquels on copie les fichiers *.sgm
	// En plus du dossier $ELA_lIV/sgm on verifie maintenant aussi en local si le fichier *.toc existe
	////////////////////////////////////////////////

	if ( (fileType(filePath(G_ela_liv,"sgm",fileBaseName(filenametoc))) == "file") || (fileType(filenametoc) == "file") ){
		//cout << format("\t-->fileType(filenametoc) est bon \n");
		var table = Node(docxml,"table",true);
		addAttr(table,"type","TOC");
		addAttr(table,"nom","Table des mati&#232;res");
		var uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,idelem);
		if Set("DECIS-OPTJ","TXT-OPTJ").knows(gielem) uriToc = format("EL/%1/%2/%3/%4/#/TOC",codeMatiere,typeOuvrage,idracine,idelem);
		if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,idracine);

		//if Set("COMJRP-OPTJ").knows(giracine) uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,"Y5COMJRP");
		if Set("RECMAJ-OPTJ").knows(giracine) uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,"RECMAJ");

		// YE 12/03/2014 Mantis 6404
		// Pour les fp fpro qr la structure est deffirente => 
		// Si la racine alors pas d'ouvrage
		// Si non on specifier l'ouvrage
		if gielem == "FPRO-OPTJ" {
			if idelem == idracine uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,idracine);
			else uriToc = format("EL/%1/%2/%3/#/TOC/%4",codeMatiere,typeOuvrage,idracine,idelem);
		}
		if gielem == "FP-OPTJ" {		
			if idelem == idracine uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,idracine);
			else uriToc = format("EL/%1/%2/%3/#/TOC/%4",codeMatiere,typeOuvrage,idracine,idelem);			
		}
		if gielem == "QR-OPTJ"{
			if idelem == idracine uriToc = format("EL/%1/%2/%3/#/TOC",codeMatiere,typeOuvrage,idracine);
			else uriToc = format("EL/%1/%2/%3/#/TOC/%4",codeMatiere,typeOuvrage,idracine,idelem);				
		}
		
		addAttr(table,"uri",uriToc);

		// 21/08/2012 AL
		// Ne pas preciser la toc pour les jrp EL
		// 08/11/2012 al
		// Ne pas preciser la toc pour les elements QR-OPTJ/GP20

		// ancien test avant ajout qr-opt/gp20
		// if !Set("DECISBLOC-OPTJ").knows(giracine){
		// insertSubTree(donnees,-1,table);
		// }

		// -----------------------------------------------------------
		// 09/11/2012 MB : ne pas creer de lien vers toc pour les QR des GPxx (plus de sommaire car toc vide)
		//                 utiliser le set G_setDicoGP declare dans dico.var
		// -----------------------------------------------------------
		// 14/11/2012 MB : sauf le GP20 
		//if ((giracine == "DECISBLOC-OPTJ") || (giracine=="QR-OPTJ" && gielem=="GP20"))
		// -----------------------------------------------------------
		// 04/10/2013 alazreg
		// mantis 3863 : generer toc pour les qr
		// => je retire qr-optj + gp20 + G_setDicoGP du test dessous
		// => toutes les qr ont maintenant une toc
		// -----------------------------------------------------------
		//if ((giracine == "DECISBLOC-OPTJ") || (giracine=="QR-OPTJ"  && codeMatiere != "GP20" &&  G_setDicoGP.knows(codeMatiere)))
		if (giracine == "DECISBLOC-OPTJ"){
			;// ne rien faire
		}
		else{
			insertSubTree(donnees,-1,table);
		}
	}

	// -----------------------------------------------------------
	// 11/07/2013 alazreg
	// mantis 3999
	// Pour le formulaire ECLAIR - ecrire a la redaction
	// ajouter les elements uc/donnees/matiere@code=CDXX + uc/eclair@class=tcontactredac
	// Attention la feuille de style HTMLP doit contenir la meme class que l'element eclair => "tcontactredac"
	// -----------------------------------------------------------
	if (gielem == "ETD-OPTJ"){
		var sTitreMatiere = codeMatiere; // par defaut on ecrit le code matiere CDXX
		// if G_ELNETcodeProdShortTitleIsoMap.knows(codeMatiere) sTitreMatiere = G_ELNETcodeProdShortTitleIsoMap[codeMatiere];
		if G_ELNETcodeProdShortTitleMap.knows(codeMatiere) sTitreMatiere = G_ELNETcodeProdShortTitleMap[codeMatiere];
		var matiere = Node(docxml,"matiere",Node(docxml,"#CDATA",sTitreMatiere));
		addAttr(matiere,"code",codeMatiere);
		insertSubTree(donnees,-1,matiere);
	}
	
	return donnees;
}

function getNodeMetadata(docxml,codeMatiere,titreElem){
	// Exemple :
	// <metadata><![CDATA[
	// {
		// "title": "Accident du travail : d&#233;finition",
		// "creation-date": "2017-06-30",
		// "update-date": "2017-06-30",
		// "subject": {
			// "name": "Social",
			// "theme": "CD02",
			// "icon": "CD02"
			// }
	// }
	// ]]></metadata>

	var metadata = Node(docxml,"metadata",false);
	var libMatiere = codeMatiere;
	if G_ELNETcodeProdShortTitleMap.knows(codeMatiere) libMatiere = G_ELNETcodeProdShortTitleMap[codeMatiere];
	// 18/07/2017 SIECDP-274 279 280 remplacement des guillemets americains par des guillemets francais car ca pose probleme dans la livraison
	// insertSubTree(metadata,-1,Node(docxml,"#CDATA",format("<![CDATA[{\"title\":\"%1\",\"creation-date\":\"%2\",\"update-date\":\"%2\",\"subject\":{\"name\":\"%3\",\"theme\":\"%4\",\"icon\":\"%4\"}}]]>",entity2char(entityNum2EntityHtml(titreElem).replace(0,"&laquo;","«").replace(0,"&raquo;","»")),timeFormat(timeCurrent(),"%Y-%m-%d"),entity2char(entityNum2EntityHtml(libMatiere)),codeMatiere)));
	// 27/11/2017 ALAZREG https://jira.els-gestion.eu/browse/SIEHULKPHO-15323

	insertSubTree(metadata,-1,Node(docxml,"#CDATA",format("<![CDATA[{\"title\":\"%1\",\"creation-date\":\"%2\",\"update-date\":\"%2\",\"subject\":{\"name\":\"%3\",\"theme\":\"%4\",\"icon\":\"%4\"},\"back-to-top\":{\"ua-sequence\": 1}}]]>",entity2char(entityNum2EntityHtml(titreElem).replace(0,"&laquo;","«").replace(0,"&raquo;","»")),timeFormat(timeCurrent(),"%Y-%m-%d"),entity2char(entityNum2EntityHtml(libMatiere)),codeMatiere)));
	
	// cout << format("debugsf titreElem AVANT = %1\n", titreElem);
	// cout << format("debugsf titreElem APRES = %1\n", entity2char(entityNum2EntityHtml(titreElem)));
	
	return metadata;
}

function getNomPictoMatiere(codeMatiere){
	// Cette fonction retourne le nom du picto pour la matiere donnee en parametre
	// Exemple : puceMat02.png pour la matiere CD02
	return "puceMat"+codeMatiere.extract(2,-1)+".png";
}

function creerBlocRecherche(docxml,giracine,idracine){
	// 11/03/2013 alazreg
	// Cette fonction cree le bloc recherche qui sera insere dans uc/recherche
	var recherche = nothing;

	// ---------------------------------------------------------------------
	// 11/03/2013 alazreg
	// le champs recherche evolue
	// ---------------------------------------------------------------------
	// Pas de champ recherche dans certains documents
	// Dans tous les autres documents le champ recherche doit figurer
	if !Set("LISTE","IDX-OPTJ","IDXET-OPTJ","RECMAJ-OPTJ","TAG-OPTJ").knows(giracine) recherche = Node(docxml,"recherche",false);
	
	// si le document possede un element uc/recherche alors le completer
	// avec des sous element uc/recherche/champ
	if (recherche != nothing){
		addAttr(recherche,"id","el");

		var champ = Node(docxml,"champ",true);
		// ---------------------------------------------------------------------
		// 28/03/2013 alazreg
		// pour les codes el 
		// on surcharge le libelle par defaut "Acces direct paragraphe" par ceci :
		// "Acces direct article" ou "Un numero d'article"
		//
		// ATTENTION
		// dans le cas des codes le noeud recherche/champ contiendra un fils <libelle>
		// Il faut donc modifier Node(docxml,"champ",false) en Node(docxml,"champ",true)
		// ---------------------------------------------------------------------
		// 16/04/2013 alazreg
		// idem pour les ccol : renseigner le libelle "Un numero d'article" comme pour les codes 
		// ---------------------------------------------------------------------

		if Set("CCOL-OPTJ","CODE-OPTJ").knows(giracine){
			champ = Node(docxml,"champ",false);
			var libelle = Node(docxml,"libelle",Node(docxml,"#CDATA","Un numero d'article"));
			insertSubTree(champ,-1,libelle);	  
		}

		// -----------------------------------------------
		// 28/03/2013 alazreg
		// chez jouve le champ paragraphe est ajoute par defaut meme si absent des donnees EL
		// pour ne pas afficher le champ paragraphe, il faut le creer et  indiquer actif="false"
		// -----------------------------------------------
		addAttr(champ,"id","paragraphe");

		// Les etudes et ccol doivent avoir un champ pour acceder au paragraphe
		if Set("ETD-OPTJ","CCOL-OPTJ","CODE-OPTJ").knows(giracine) addAttr(champ,"actif","true");
		else addAttr(champ,"actif","false");

		insertSubTree(recherche,-1,champ);

		// Tous les documents (sauf les quelques exceptions dessus) doivent avoir un champ
		// pour rechercher du texte
		champ = Node(docxml,"champ",true);
		addAttr(champ,"id","text-integral");
		addAttr(champ,"actif","true");
		insertSubTree(recherche,-1,champ);
	}
	
	return recherche;
}

/*

Ajouter bloc presentation pour les uc de type "ouvrage" :

 <presentation>
   <action type="thematic-class">
     <parametre>matiereCD02</parametre>
   </action>
 </presentation>

*/


function creerBlocPresentation(docxml, giracine, idracine, codeMatiere){

  var parametre = Node(docxml,"parametre", Node(docxml,"#CDATA", "matiere" + codeMatiere));
  var action = Node(docxml, "action", parametre);
  addAttr(action,"type","thematic-class");
  var presentation = Node(docxml,"presentation",action);
  
  // si le document possede un element uc/presentation alors le completer
  // avec des sous element uc/presentation/action@type et uc/presentation/action/parametre
  
  /* presentation = Node(docxml,"presentation",false);
  
  var action = Node(docxml,"action",false);
  addAttr(action,"type","thematic-class");
  insertSubTree(presentation,-1,action);
  
  var parametre = Node(docxml,"parametre",false);
  insertSubTree(action,-1,parametre);*/
		
  return presentation;
}


function get_map_alerte_redaction(){
	// --------------------------------------------------------------------------
	// cette fonction lit le fichier alerte_redaction.xml et le transforme en Map
	// --------------------------------------------------------------------------

	// cout << format("%1 Generation de la Map Alerte Redaction\n",what_time_is_it());

	// --------------------------------------------------------------------------
	// 23/08/2013 alazreg
	// alerte redaction a traiter pour z2169
	// attention pour hulk il faut prendre le fichier /mac.public/hulk/alerte_redaction/www/alerte_redaction.xml
	// --------------------------------------------------------------------------
	//    var filepath_alerte_redaction_xml = filePath(env("ELA_CDROM"),"elnet","liv","www","alerte_redaction.xml");
	var filepath_alerte_redaction_xml = filePath("/mac.public/hulk/alerte_redaction/www/alerte_redaction.xml");

	if fileDate(filepath_alerte_redaction_xml) == nothing {
		cout << format("Fichier inexistant %1\n",filepath_alerte_redaction_xml);
		return 1;
	}

	// cout << format("Fichier existe %1\n",filepath_alerte_redaction_xml);

	// parse du fichier xml
	var ret = parseDocument(filepath_alerte_redaction_xml, Map("keep",true,"generator", "xml"));
	if ret.status != 0{
		cout << format("Parse NOK %1\n",filepath_alerte_redaction_xml);
		return 1;
	}

	//cout << format("Parse OK\n");

	var doc = ret.document;
	var alerts = root(doc);

	//cout << format("%1\n",echo(alerts));

	var mapAlerteRedaction = Map();
	// pour chaque ALERT recuperer le fils REFID ou KEYBOOK
	var refid_or_keybook = nothing;
	var content_refid_or_keybook = nothing;
	var id = nothing;
	var uri = nothing;
	for alert in searchElemNodes(alerts,"ALERT","ID",nothing){
		refid_or_keybook = child(alert,"REFID");
		if refid_or_keybook == nothing refid_or_keybook = child(alert,"KEYBOOK");
		if refid_or_keybook == nothing{
			cout << format("ERREUR PAS de ID SGML pour %1\n",echo(alert));
			continue;
		}

		// Renseigner la Map des Alerte Redaction
		content_refid_or_keybook = content(refid_or_keybook);
		id = attr["ID",alert];
		//uri = "EL/#TRANSVERSE/www/html/msgOptionnelDocId-AL15redac2.htm";
		uri = format("EL/#TRANSVERSE/www/html/msgOptionnelDocId-%1.htm",id);
		if !mapAlerteRedaction.knows(content_refid_or_keybook){
			//mapAlerteRedaction[content_refid_or_keybook] = id;
			mapAlerteRedaction[content_refid_or_keybook] = uri;
		}
	}

	// cout << format("%1 Fin generation de la Map Alerte Redaction\n",what_time_is_it());

	/*
	for k,v in mapAlerteRedaction{
	cout << format("%1\t%2\n",k,v);
	}
	*/

	return mapAlerteRedaction;
}


function getLienAlerteRedaction(docxml,idelem){
	// -----------------------------------------------------------
	// Cette fonction retourne un element <lien> pour Alerte Redaction
	// -----------------------------------------------------------

	// Si la Map n'a pas encore ete chargee alors la charger maintenant
	if G_mapAlerteRedaction == nothing G_mapAlerteRedaction = get_map_alerte_redaction();

	// Si la Map contient l'id alor creer un node <lien> et le renvoyer
	if (isaMap(G_mapAlerteRedaction) && G_mapAlerteRedaction.knows(idelem)){
		// exemple :
		// <lien type="AlerteRedaction" info-bulle="Alerte de la Redaction" uri="EL/#TRANSVERSE/www/html/msgOptionnelDocId-AL15redac2.htm">
		//      <libelle>Alerte de la Redaction</libelle>
		// </lien>
		
		// 28/05/2015 MB : modifier le libellé pour le faire tenir sur une seule ligne
		//var lienAlertRedaction = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Alerte de la R&#233;daction")));
		var lienAlertRedaction = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Alerte R&#233;daction")));
		
		addAttr(lienAlertRedaction,"type","AlerteRedaction");
		addAttr(lienAlertRedaction,"auto-open","true");
		addAttr(lienAlertRedaction,"info-bulle","Alerte de la R&#233;daction");
		addAttr(lienAlertRedaction,"uri",G_mapAlerteRedaction[idelem]);
		return lienAlertRedaction;
	}

	return nothing;
}

function get_lienAccueilMat(codeMatiere){
	//YE 5992 06/02/2014
	var lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACC%2fAccueilMatiere_wide.html",codeMatiere,"%2");
	// Pour les GB59, 23, 25, 66, 67, 68, 74 l' addresse est diff
	if Set("GP59", "EN23I", "GP25", "GP66", "GP67", "GP68","GP69", "GP74", "GP76","GP95","GP114").knows(codeMatiere) lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACC%2fwidgetGuideRh_wide.html",codeMatiere,"%2");	
// YE 20/03/2015 0010450 
	
	////SF/MB : 26/02/2014 Pour le dp15, mettre ACCRSS a la place de ACC (car pb version page accueil des CC)
	// if (codeMatiere == "CD15") lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACCRSS%2fAccueilMatiere_wide.html",codeMatiere,"%2");

	if (codeMatiere == "CD15") lienAccueilMat = format("@@exaDocLink{thmuri:'EL/%1/ACCRSS/AccueilMatiere_wide.html', fdoc:['EL/CD15/ACC']}",codeMatiere,"%2");
	
	// YE Le 13/06/2014 Mantis 7540
	if (codeMatiere == "CD14") lienAccueilMat = format("@@exaDocLink{thmuri:'EL/CD14/ACC/AccueilMatiere_wide.html', fdoc:['EL/CD14/ACC']}");
	
	return lienAccueilMat;
}

function creerBlocDocument(docxml,codeMatiere,racine,idracine,giracine,elem,idelem,titreElem,filenametoc,libMatiere,sFdoc){
	// Inserer le bloc Document a droite
	var bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Document")));
	addAttr(bloc,"type","document");

	// Inserer le titre matiere + titre ouvrage sous Document a droite
	// 03/08/2012 AL
	// Ajout de la puce de la couleur devant la matiere
	// Attention il faut se coordonner avec Martin FONTANEL sur les dossiers + noms des images
	// Exemple de notation dbMango pour les images
	// <img src="@@mgostatic{uri:'EL/GP68/ACC/img/widgets/mini-guide/guide-recrutement-et-gestion-des-carrieres.jpg'}" alt="" />

	// Attention dans le cas des sources textes il n'y a pas d'image devant le titre "Sources"
	var titreBlocDocument = "<![CDATA[<h3>";
	var tailleTitreMax = 100;   
	// YE Le 03/09/2014 Mantis 7257 
	
	if codeMatiere == "CD05" {		
		if (idracine.rsearch(0,"ET1") != nothing || idracine.rsearch(0,"LSTFORM1") != nothing || idracine.rsearch(0,"RECAPNEW1") != nothing )
			libMatiere = "Commercial";
		if (idracine.rsearch(0,"ET2") != nothing || idracine.rsearch(0,"LSTFORM2") != nothing || idracine.rsearch(0,"RECAPNEW2") != nothing ) 
			libMatiere = "Concurrence, distribution, consommation";
		if (idracine.rsearch(0,"ET3") != nothing || idracine.rsearch(0,"LSTFORM3") != nothing || idracine.rsearch(0,"RECAPNEW3") != nothing ) 
			libMatiere = "Propri&#233;t&#233; industrielle - intellectuelle";
		if (idracine.rsearch(0,"ET4") != nothing || idracine.rsearch(0,"LSTFORM4") != nothing || idracine.rsearch(0,"RECAPNEW4") != nothing ) 
			libMatiere = "Soci&#233;t&#233;s et dirigeants";	
		}
		
	if codeMatiere != "SOURCES" && codeMatiere != "ELNET"{
		var nomImage = getNomPictoMatiere(codeMatiere);
		
		var lienAccueilMat = get_lienAccueilMat(codeMatiere);
		// YE Le 03/09/2014 Mantis 7257 
		// YE Le 15/09/2014 Mantis 8152 Adapter les liens vers les acc de blocs 
		if codeMatiere == "CD05" {				
			if (idracine.rsearch(0,"ET1") != nothing || idracine.rsearch(0,"LSTFORM1") != nothing || idracine.rsearch(0,"RECAPNEW1") != nothing )
				lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACCDJ%2f2%2fAccueilMatiere_wide_2.html",codeMatiere,"%2");
			if (idracine.rsearch(0,"ET2") != nothing || idracine.rsearch(0,"LSTFORM2") != nothing || idracine.rsearch(0,"RECAPNEW2") != nothing )
				lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACCDJ%2f3%2fAccueilMatiere_wide_3.html",codeMatiere,"%2");
			if (idracine.rsearch(0,"ET3") != nothing || idracine.rsearch(0,"LSTFORM3") != nothing || idracine.rsearch(0,"RECAPNEW3") != nothing )
				lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACCDJ%2f4%2fAccueilMatiere_wide_4.html",codeMatiere,"%2");
			if (idracine.rsearch(0,"ET4") != nothing || idracine.rsearch(0,"LSTFORM4") != nothing || idracine.rsearch(0,"RECAPNEW4") != nothing )
				lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL%2f%1%2fACCDJ%2f1%2fAccueilMatiere_wide_1.html",codeMatiere,"%2");		
		}
		
		// 19/07/2016 SF ajout preventeur
		// if (codeMatiere == "GP95") {
		if (Set("GP95","GP166").knows(codeMatiere)) {
			// 17/10/2016 SF : mantis 14411 Preventeur UC document : Renvoi vers l'ancienne PA
			// lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL/preventeurs/accueil_preventeur.html");
			// lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL/GP95/ACC/widgetGuideRh_wide.html");
			lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL/%1/ACC/widgetGuideRh_wide.html", codeMatiere);
		}
		
		//titreBlocDocument << format("<img src=\"@@mgostatic{uri:'EL/%1/ACC/img/menu/%2'}\"/>&nbsp;",codeMatiere,nomImage);
		
		// 08/01/2016 SF modif packope
		if (hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
			var h4_content = tronquer_et_garder_entites(titreElem,tailleTitreMax);
			var packid = attr["PACKID", racine];
			var packTitle = entityHtml2EntityNum(char2entity(getPackTitle(packid)));
			// 08/03/2016 ajout packope
			// calcul des uri des pages d'accueils
			lienAccueilMat = format("/documentation/DocumentStatic?StaticDoc=EL/Pack_Ope/%1/%1.html", packid);
			if (packTitle != nothing)
				titreBlocDocument << format("<a title=\"%1\" href=\"%3\">%1</a></h3><h4>%2</h4>]]>",packTitle,h4_content,lienAccueilMat);
		}
		else {
		titreBlocDocument << format("<img src=\"@@mgostatic{uri:'EL/PUCESMAT/%2'}\"/>",codeMatiere,nomImage);
		
		//YE 5992 06/02/2014
		// Ajouter un lien vers accueil de la matiere 	
		var h4_content = tronquer_et_garder_entites(titreElem,tailleTitreMax);
		titreBlocDocument << format("<a title=\"%1\" href=\"%3\">%1</a></h3><h4>%2</h4>]]>",libMatiere,h4_content,lienAccueilMat);
		}
		// ----------------------------------------------------
		// ----------------------------------------------------
		// mantis 7241 formule dalloz
		// le titre matiere DZ est different du titre matiere EL
		// voir function get_lib_matiere(racine)
		// La presentation du bloc Document est aussi différente
		// ----------------------------------------------------
		var editeur = get_editeur(racine);
		if giracine == "DPFORM2-OPTJ" && editeur == "DZ" titreBlocDocument = format("<![CDATA[<h3>%1</h3><h4>%2</h4>]]>",libMatiere,titreElem);
	}
	else {
		titreBlocDocument << format("%1</h3><h4>%2</h4>]]>",libMatiere,tronquer_et_garder_entites(titreElem,tailleTitreMax));
	}

	// -----------------------------------------------------------------
	// 23/12/2013 alazreg
	// pour les fiches du gp20 => elements qr-optj//gp20
	// -----------------------------------------------------------------
	//var info = Node(docxml,"info",Node(docxml,"#CDATA",titreBlocDocument));
	//insertSubTree(bloc,-1,info);
	//YE 12/03/2014 Mantis 6404 
	// Ne pas inserer info pour les ouvrages dans les blocs
	
	// if (giracine == "QR-OPTJ" && GI(elem) == "GP20") ;// ne pas inserer l'element <info>
	if (giracine == "QR-OPTJ" && GI(elem) == "GP20") || (GI(elem) == "FP-OPTJ" && idracine != idelem) 	;
	else if (GI(elem) == "FPRO-OPTJ" && idracine != idelem) 	; 
	//else if (GI(elem) == "QR-OPTJ" && idracine != idelem) 	;  // a verifier
	else{
		var info = Node(docxml,"info",Node(docxml,"#CDATA",titreBlocDocument));
		insertSubTree(bloc,-1,info);
	}

	// 20/07/2012 AL
	// Pour les textes anterieurs ajouter un lien vers le texte courant
	// Si on ouvre le texte anterieur en pop-up on pourra supprimer ce lien
	// A valider avec Marketting EL + Jouve
	if idracine.search(0,"ELNETTXTANTBLOC") == 0{
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Version courante du texte")));
		insertSubTree(bloc,-1,lien);
		addAttr(lien,"info-bulle","Retour &#224; la version courante du texte");
		addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",idelem.explode("-")[0],sFdoc));
		// YE Le 17/02/2014 Mantis 5304 
		// Ajout du picto version courante du texte	
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/version_courante_du_texte.png'}");	
		
	}

	// Inserer un lien vers le sommaire complet TOC
	// 22/06/2012 AL/MB
	// Vu avec Aude DEFRETIERE
	// Le lien sommaire complet est abandonne
	//if fileType(filenametoc) == "file"{
	//var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Sommaire complet")));
	//addAttr(lien,"info-bulle","Sommaire complet");
	//addAttr(lien,"url",format("@@exaDoclink{dockey:'TOC%1',fdoc:['%2']}",idracine,sFdoc));
	//insertSubTree(bloc,-1,lien);
	//}

	// Inserer un lien vers la table alphabetique - cas des etudes
	var tapNode = enclosed(elem,"TAP");
	if tapNode != nothing{
		var idTap = attr["ID",tapNode];
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Table alphab&#233;tique")));
		insertSubTree(bloc,-1,lien);

		// ----------------------------------
		// 04/06/2013 alazreg
		// Certains liens doivent s'ouvrir dans la meme fenetre du navigateur (pas de nouvelle fenetre)
		// Exemple le lien à droite des etudes "Table alphabetique" doit ouvrir la TAP dans la meme fenetre
		// Pour cela indiquer <lien type="InnerLink">
		// ----------------------------------
		// 18/12/2013 MB : pour le type du <lien> de la "Table alphabetique", indiquer table alphabetique (mail MG : 11/12/2013 18:43)
		//addAttr(lien,"type","InnerLink");
		addAttr(lien,"type","TableAlpha");

		addAttr(lien,"info-bulle","Table alphab&#233;tique");
		//------------------------------
		// YE/SF Le 15/01/2014 Mantis 5304
		// Ajout de l attr id-picto="@@mgostatic{uri:'EL/PUCESMAT/puceMat05.png'}", pour la gestion des pictos dans la colonne de droite	
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/table_alphabetique.png'}");//table_alphabetique.png
		addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",idTap,sFdoc));
	}

	// ----------------------------------------------------------------
		// 04/10/2012 AL creer un lien depuis etude vers recapnew
		// Pas de Nouveaute du mois sur le syntheses du dp15
	// ----------------------------------------------------------------
	// 10/08/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-6 : ajouter Nouveautés de la quinzaine sur les études du dp15
	// Je retire le test if codeMatiere != "CD15"
	// ----------------------------------------------------------------
	// 30/08/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5 : ajouter Nouveautés de la quinzaine sur les ccol (texte intégral) du dp15
	// ----------------------------------------------------------------
	if giracine == "ETD-OPTJ"{
	// if (giracine == "ETD-OPTJ") || (giracine == "CCOL-OPTJ" && codeMatiere == "CD15")
		var label_Nouveautes_mois_quinzaine = "Nouveaut&#233;s du mois";
		if codeMatiere == "CD15" label_Nouveautes_mois_quinzaine = "Nouveaut&#233;s de la quinzaine";
		if codeMatiere != "CD15"{
			var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",label_Nouveautes_mois_quinzaine)));
			insertSubTree(bloc,-1,lien);
			addAttr(lien,"info-bulle",label_Nouveautes_mois_quinzaine);
			// YE/SF Le 15/01/2014 Mantis 5304
			// Picto nouveautes du mois
			addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/nouveaute_du_mois.png'}"); //
			
			var idCible = idelem.extract(0,2)+"RECAPNEW";
			var sFdocCible = getFdocPath(codeMatiere,"TAG-OPTJ",idCible);
			var idet = idracine.explode("-")[0];
			if idet.rsearch(0,"^CC") != nothing idet = idet.replace(0,"CC","Y5");
			if G_mapIdEtIdRecapnew.knows(idet){
				//idCible = G_mapIdEtIdRecapnew[idet];
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",G_mapIdEtIdRecapnew[idet],sFdocCible));
			}
			//else addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",idCible,sFdocCible));
			//else addAttr(lien,"url",format("@@mgostatic{uri:'%1'}","EL/#TRANSVERSE/www/html/recapnew.htm",sFdocCible));
			// 19/10/2012 AL
			//else addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}","EL/#TRANSVERSE/www/html/recapnew.htm",sFdocCible));
			// ----------------------------------------------
			// 29/01/2014 alazreg
			// mantis 2661 + 2737 popup nouveautés du mois
			// demande de sophie-charlotte journet :
			// supprimer le lien à droite vers le popup html si 'il n'y a pas de nouveautes du mois pour l'étude concernee
			// ----------------------------------------------
			//else{
			//	addAttr(lien,"url",format("@@exaDoclink{uri:'%1'}","EL/#TRANSVERSE/www/html/recapnew.htm",sFdocCible)); // supprimer le fdoc
			//	addAttr(lien,"cible","popup"); // ajouter lien@cible=popup
			//}
		}
	}

	// ---------------------------------------------------------------------
	// 03/06/2013 alazreg
	// ajouter un lien Alerte Redaction sur les etudes (et syntheses du dp15)
	// ---------------------------------------------------------------------
	// 20/12/2013 alazreg
	// ajout cas des fiches gp20 exemple x0q158 dans x0qp1z3
	// je commente le test if ETD-OPTJ
	// ---------------------------------------------------------------------
	//if giracine == "ETD-OPTJ"{
	//cout << format("lienAlerteRedaction = getLienAlerteRedaction(docxml,%1);\n",idelem);
	//system("sleep 10");

	var lienAlerteRedaction = getLienAlerteRedaction(docxml,idelem);
	if lienAlerteRedaction != nothing insertSubTree(bloc,-1,lienAlerteRedaction);
	//}
	
	// Creer un lien depuis une FP QR FPRO vers la liste respective des FP QR FPRO
	// YE 12/03/2014 Mantis 6404
	// Creer le lien vers la liste que pour la racine
	// if Set("FP-OPTJ","QR-OPTJ","FPRO-OPTJ").knows(giracine) {
	// 08/01/2016 SF modif packope
	// if Set("FP-OPTJ","QR-OPTJ","FPRO-OPTJ").knows(giracine) && idelem ==idracine {
	if Set("FP-OPTJ","QR-OPTJ","FPRO-OPTJ").knows(giracine) && idelem ==idracine && !hasAttr(racine,"PACKOPE") {

		var sLibelle = "";
		
		if giracine == "FP-OPTJ" sLibelle = "Liste des fiches pratiques";
		if giracine == "QR-OPTJ" sLibelle = "Liste des questions-r&#233;ponses";
		if giracine == "FPRO-OPTJ" {
			sLibelle = "Liste des fiches professions";
			// 07/11/2012 MB : traitt cas fiches risques gp59
			// 01/02/2017 Migration GB DUP changement libelle en Liste des fiches risques
			// if codeMatiere == "GP59" sLibelle = "Liste des fiches risques";	
			if Set("GP59","GP166","GP181").knows(codeMatiere) sLibelle = "Liste des fiches risques";	
			// 15/12/2014 MB : traitt cas fiches risques gp95 Mantis 9240 
			if codeMatiere == "GP95"{
				sLibelle = "Liste des fiches risques";
				if hasAttr(elem,"TYPEDOC") && attr["TYPEDOC",elem] == "FREG" sLibelle = "Liste des fiches r&#233;glementaires";
			}
			// YE Le 03/09/2014 Mantis 7916 ajout du cas GP69
			if codeMatiere == "GP68" ||codeMatiere == "GP69" sLibelle = "D&#233;cryptages";
			
		}
		// 29/10/2012 MB : pour le gp68 ==> mettre "Decryptages"
		//if codeMatiere == "GP68" sLibelle = "D&#233;cryptages";
		
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",sLibelle))); 

		// 08/11/2012 al
		// Pour le GP20 il n'existe pas de liste des QR
		// Ne pas creer le lien retour à la Liste des QR
		// 14/11/2012 MB : changer le test suite pb absence liste sur les FP de GP25/68/67...
		//if giracine == "QR-OPTJ" && codeMatiere != "GP20"{
		if !(giracine == "QR-OPTJ" && codeMatiere == "GP20"){
			insertSubTree(bloc,-1,lien);
		}
		// addAttr(lien,"info-bulle",sLibelle);
		addAttr(lien,"info-bulle",sLibelle.transcript(g_map_entites_nommees_to_unicode));
		//addAttr(lien,"id-picto","acceder");
		// YE Le 02/04/2014 Mantis 5304 
		// Ajout du picto fiches_pratiques pour les cas suivants:
		// Liste des fiches pratiques, Liste des questions-r&#233;ponses, Liste des fiches risques, D&#233;cryptages
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/fiches_pratiques.png'}");	
		
		addAttr(lien,"cible","vue");

		// Attention on donne le ID + FDOC de la cible => LISTE	
		var idCible = idelem.extract(0,2)+"LST"+giracine.explode("-")[0];
		if codeMatiere == "GP95" && giracine == "FPRO-OPTJ" && hasAttr(elem,"TYPEDOC") && attr["TYPEDOC",elem] == "FREG" idCible = "Q5LSTFREG";
		var sFdocCible = getFdocPath(codeMatiere,"LISTE",idCible);
		// 07/11/2012 MB : attn cas du gp25 : le sommaire est la fiche speciale x5fpro79 et non une liste
		if giracine == "FPRO-OPTJ" &&  codeMatiere == "GP25" {//idelem == "X5FPRO799" {
			idCible = "X5FPRO799";	
			sFdocCible = getFdocPath(codeMatiere,"FPRO",idCible);
			addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['%2']}",idCible,sFdocCible));
			//cout << format("\nDEBUGMB : dockey=%1  sFdocCible=%2\n\n",idCible,sFdocCible);
		}
		else
		// fin ajouts MB
		// le premier attribut /LISTE@ID se termine par "-1". Il faut donc ajouter "-1" dans la valeur de dockey
		addAttr(lien,"url",format("@@exaDocLink{dockey:'%1-1',fdoc:['%2']}",idCible,sFdocCible));
	}

	// YE Le 25/04/2014 Mantis 7053
	// Ajouter le nouveau lien "Mode Liseuse"
	
	var bModeLiseuse = true; // Cette variable vas nous servire dans le cas ou on voulais exclure qlq cas comme les listes indexe etc cf mantis 7053 
	
	if bModeLiseuse {		
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Mode Liseuse")));
		insertSubTree(bloc,-1,lien);		
		addAttr(lien,"type","ReaderMode");
		addAttr(lien,"info-bulle","Mode Liseuse");						
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/reader-mode.png'}");		
	}



	return bloc;
}

function copier_fichier_ANX(fichier,dossier_anx){
	// cette fonction deplace un fichier annexe dans le dossier adequat
	// creer le dossier ANX s'il n'existe pas
	if fileType(dossier_anx) != "dir" system("mkdir -p "+dossier_anx);
	if fileType(fichier) == "file" system(format("\mv %1 %2",fichier,dossier_anx));
}

function getLien_exaDocLink(docxml,libelle,dockey,fdoc){
	// Exemple
	// <lien info-bulle="Nouveaut&#233;s de la quinzaine"
			// rang="2"
			// id-picto="@@mgostatic{uri:'EL/PUCESMAT/nouveaute_du_mois.png'}"
			// url="@@exaDoclink{dockey:'Y5RECAPNEW-2',fdoc:['EL/CD15/RECAPNEW']}">
			// <libelle>Nouveaut&#233;s de la quinzaine</libelle>
	// </lien>
	var libelle_unicode = libelle.transcript(g_map_entites_nommees_to_unicode);
	// var nodeLien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",libelle)));
	var nodeLien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",libelle_unicode)));
	addAttr(nodeLien,"info-bulle",libelle_unicode);
	if g_map_voir_aussi_ccol.knows(libelle) addAttr(nodeLien,"rang",g_map_voir_aussi_ccol[libelle]);
	if G_map_picto.knows(libelle) addAttr(nodeLien,"id-picto",format("@@mgostatic{uri:'EL/PUCESMAT/%1'}",G_map_picto[libelle]));
	addAttr(nodeLien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",dockey,fdoc));
	return nodeLien;
}

function creerBlocVoirAussi(docxml,elem,racine,idracine,giracine,idelem,titreElem,filenametoc,codeMatiere,libMatiere,sFdoc){
	var bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Voir aussi")));
	addAttr(bloc,"type","voir_aussi");
	// YE 09/04/2015 mantis 10498
	// Ajout de l attr tri 
	addAttr(bloc,"tri","RANG");
	
	var bBlocVoirAussi = false;
	var idCible = "";
	var editeur = get_editeur(racine);
	var rang = 1; 

	// YE le 01/04/2014 Fiche mantis 6506	
	
	if giracine == "ETD-OPTJ" && idracine == "Z6021" {
		bBlocVoirAussi = true;
		// Creer un lien vers la version pdf de la synthese
		// 15/09/2017 MB : le libelle est insere directement dans le xml de l'uc ==> il doit etre encod頨entit頮um鲩que)
		//var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Guide B&eacute;n&eacute;fices agricoles")));
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Guide B&#233;n&#233;fices agricoles")));
		
		addAttr(lien,"url",format("@@exaDoclink{uri:'%1'}","EL/CD06/PDF/ba.pdf"));
		addAttr(lien,"cible","popup");		
		//YE Mantis 5304
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");	
		insertSubTree(bloc,-1,lien);
	}
	
	if giracine == "QR-OPTJ" && codeMatiere == "GP20" && hasAttr(elem,"IDFICHE"){
		bBlocVoirAussi = true;
		// Creer un lien vers la version pdf de la synthese
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Consulter la version PDF")));
		//addAttr(lien,"url",format("@@exaDoclink{uri:'%1/%2/#/ANX/%3.pdf'}",sFdoc,idelem,idelem.transcript(LowerCase)));
		//var uriPdf = format("%1/#/ANX/%3.pdf",sFdoc,idelem,attr["IDFICHE",elem].transcript(LowerCase));
		
		// 12/11/2012 MB : garder l'id de la racine tel quel (les pdf des fiches x0q001 et x0q02 ne s'affichent pas ) : X0Q001-HZ et X0Q002-HZ
		//var uriPdf = format("EL/%1/FPAIE/%2/#/ANX/%3.pdf",codeMatiere,idracine.explode("-")[0],attr["IDFICHE",elem].transcript(LowerCase));
		var uriPdf = format("EL/%1/FPAIE/%2/#/ANX/%3.pdf",codeMatiere,idracine,attr["IDFICHE",elem].transcript(LowerCase));
		//cout << format("id racine '%1'\n",idracine);
		//var uriPdf = format("%1/#/ANX/%3.pdf",sFdoc,idelem,attr["IDFICHE",elem].transcript(LowerCase));
		addAttr(lien,"url",format("@@exaDoclink{uri:'%1'}",uriPdf));
		addAttr(lien,"cible","popup");
		// SF Le 23/01/2014 Mantis 5304 
		// Ajout du picto PDF
		//addAttr(lien,"id-picto","pdf");
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");	
		insertSubTree(bloc,-1,lien);
	}

	// 11/01/2016 SF ajout packope
	if ((giracine == "FP-OPTJ" || giracine == "DPFORM2-OPTJ") && hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
		// cout << format("idracine = %1\n", idracine, attr["PACKID", racine]);
		bBlocVoirAussi = true;
		var listDocs = getPackDocuments(attr["PACKID", racine]);
		// cout << format("debugsf listDocs = %1\n", listDocs);
		
		var liensModeles = nothing;
		var liensOutils = nothing;
		var liensDocumentation = nothing;
		var lienProcedure = nothing;
			
		for doc in listDocs {
			// ne pas ajouter le document courant dans la zone voir aussi
			var idDoc = doc.id;
			if (doc.type == "FICHE" || doc.type == "MODELE") {
				if idDoc.ssearch(0,2,"EL") == nothing
					idDoc = format("%1%2", doc.id, attr["PACKID", racine]);
				// if idDoc == idracine continue;
				if idDoc == idelem continue;
			}
			else if (doc.type == "OUTIL") {
				// cout << format("debugsf doc.type = %1\n", doc.type);
				idDoc = format("http://www.editions-legislatives.fr/MC20/MC20.html?animation=%1", idDoc);
			}
			
			// "@@exaDocLink{dockey:'http://test.editions-legislatives.fr/MC20/MC20.html?animation=animation34', fdoc:['EL/OUTILS/CALC/CALC34']}"
			
			
			var libelle = entityHtml2EntityNum(doc.alternativeTitle);
			if libelle == "" libelle = entityHtml2EntityNum(doc.title);
			var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",libelle)));
			addAttr(lien,"rang",format("%1",doc.ordre));
			addAttr(lien,"cible","vue");
			// addAttr(lien,"info-bulle",format("%1",libelle));
			addAttr(lien,"info-bulle",format("%1",libelle.transcript(g_map_entites_nommees_to_unicode)));
			switch(doc.type) {
				case "MODELE":
					if liensModeles == nothing {
						liensModeles = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Mod&#232;les")));
						addAttr(liensModeles,"tri","RANG");
						// addAttr(liensModeles,"rang","1");
						addAttr(liensModeles,"rang","2");
						addAttr(liensModeles,"info-bulle","Cliquez pour acc&#233;der aux mod&#232;les");
						addAttr(liensModeles,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/formulaires.png'}");
					}
					
					if idDoc.ssearch(0,2,"EL") != nothing
						addAttr(lien,"url",format("@@exaDocLink{uri:'%1',fdoc:['EL/PACKOPE/%2/DPFORM2']}", idDoc, attr["PACKID",racine]));
					else
						// addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/PACKOPE/%2/DPFORM2']}", idDoc, attr["PACKID",racine]));
						addAttr(lien,"url",format("@@exaDocLink{formkey:'%1',fdoc:['EL/PACKOPE/%2/DPFORM2']}", idDoc, attr["PACKID",racine]));
					insertSubTree(liensModeles,-1,lien);
				break;
				case "FICHE":
					if liensDocumentation == nothing {
						liensDocumentation = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Documentation")));
						addAttr(liensDocumentation,"tri","RANG");
						addAttr(liensDocumentation,"rang","1");
						addAttr(liensDocumentation,"info-bulle","Cliquez pour acc&#233;der aux fiches pratiques");
						addAttr(liensDocumentation,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/fiches_pratiques.png'}");
					}					
					addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/PACKOPE/%2/FP']}", idDoc, attr["PACKID",racine]));
					insertSubTree(liensDocumentation,-1,lien);
				break;
				case "OUTIL":
					if liensOutils == nothing {
						liensOutils = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Outils")));
						addAttr(liensOutils,"tri","RANG");
						addAttr(liensOutils,"rang","3");
						addAttr(liensOutils,"info-bulle","Cliquez pour acc&#233;der aux outils");
						addAttr(liensOutils,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/outils.png'}");
					}
					addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/PACKOPE/%2/OUTILS']}", idDoc, attr["PACKID",racine]));
					insertSubTree(liensOutils,-1,lien);
				break;
				case "PROCEDURE":
					if lienProcedure == nothing {
						lienProcedure = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Proc&#233;dure")));
						addAttr(lienProcedure,"cible","popup");
						addAttr(lienProcedure,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/procedure.png'}");
						addAttr(lienProcedure,"rang","4");
						addAttr(lienProcedure,"url",format("@@exaDoclink{prockey:'%1',fdoc:['EL/OUTILS/MPI35']}", idDoc));
						
					}
				break;
			}			
		}
		
		
		
		if liensDocumentation != nothing insertSubTree(bloc,-1,liensDocumentation);
		if lienProcedure != nothing insertSubTree(bloc,-1,lienProcedure);
		if liensOutils != nothing insertSubTree(bloc,-1,liensOutils);
		if liensModeles != nothing insertSubTree(bloc,-1,liensModeles);
	}

	// 07/04/2015 sfouzi mantis 9698
	// ajout "consulter la version PDF" pour GP76 et GP95
	// 09/06/2015 alazreg
	// ne pas creer le lien PDF sur les racine FPBLOC FPROBLOC QRBLOC
	// sinon lien en double avec les FP FPRO QR
	// cout << format("debug 09/06/2015 %1\n",echo(elem));
	// ---------------------------------------------------
	// 04/12/2015 alazreg mantis 12357
	// ne pas creer de lien a droite pour les DP
	// ---------------------------------------------------
	var gielem = GI(elem);
	// cout << format("debug on est avant Consulter la version PDF %1\n",gielem);
	// if (Set("FP-OPTJ","FPRO-OPTJ","QR-OPTJ","DPFORM2-OPTJ").knows(gielem) || gielem.search(0,"FORMT")==0) && idelem.rsearch(0,"BLOC") == nothing
	if (Set("FP-OPTJ","FPRO-OPTJ","QR-OPTJ","DPFORM2-OPTJ").knows(gielem) || gielem.search(0,"FORMT")==0) && idelem.rsearch(0,"BLOC") == nothing && codeMatiere.rsearch(0,"^CD") == nothing{
		var fic_pdf = filePath(env("ELA_LIV"),"www","pdf",idelem.transcript(LowerCase)+".pdf");

		// 11/06/2015 alazreg
		// deplacer le pdf dans le dossier ANX de l'ouvrage
		var dossier_anx = filePath(env("ELA_LIV"),sFdoc.replace(0,"EL/","www/")+"/"+idracine);
		var fic_pdf_anx = filePath(dossier_anx,fileBaseName(fic_pdf));
		// cout << format("debug appel a la fonction copier_fichier_ANX(%1,%2)",fic_pdf,dossier_anx);
		copier_fichier_ANX(fic_pdf,dossier_anx);
		
		// 23/10/2015 alazreg creer le lien vers PDF seulement si la cible existe
		if fileDate(fic_pdf_anx) != nothing{
			bBlocVoirAussi = true;
			// Creer un lien vers la version pdf de la synthese
			var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Consulter la version PDF")));
			var uri = format("%1/%2/#/ANX/%3",sFdoc,idracine,fileBaseName(fic_pdf));
			addAttr(lien,"url",format("@@exaDoclink{uri:'%1'}",uri));
			addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");
			addAttr(lien,"cible","popup");
			insertSubTree(bloc,-1,lien);
		}
		
		// cout << format("****** debugsf code matiere = %1\n", codeMatiere);
		// 01/12/2016 SF ajout migration GB
		if (Set("EN23I","GP25","GP59","GP66","GP67","GP68","GP69","GP74","GP76","GP95","GP114","GP166","GP181").knows(codeMatiere)) {
			bBlocVoirAussi = true;
			// getMapPreventeurDocuments();
			getMapRenvoisGB();
			// cout << format("debug idelem = %1\n", idelem);
			if G_mapRenvoisGB.knows(idelem) {
				var listDocs = G_mapRenvoisGB[idelem];				
				var liensDocumentation = nothing;
				var liensOutils = nothing;
				
				for doc in listDocs {
					var idLien = doc.id;
					var libelleLien = entityHtml2EntityNum(doc.title).transcript(g_map_entites_nommees_to_unicode);
					var typeLien = doc.type;
					var rangLien = doc.order;
					var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",libelleLien)));
					addAttr(lien,"rang",format("%1",rangLien));
					addAttr(lien,"cible","vue");
					addAttr(lien,"info-bulle",format("%1",libelleLien));
					// cout << format("debug typeLien = %1\n", typeLien);
					switch(typeLien) {
						case "FP":
						case "FPRO":
						case "FORM2":
						case "QR":
						case "ETD":
						case "FRISQ":
						case "FREG":
						case "DECR":
						case "ACCUEIL":
							if liensDocumentation == nothing {
								liensDocumentation = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Documentation")));
								addAttr(liensDocumentation,"tri","RANG");
								addAttr(liensDocumentation,"rang","1");
								addAttr(liensDocumentation,"info-bulle","Cliquez pour acc&#233;der aux fiches pratiques");
								addAttr(liensDocumentation,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/fiches_pratiques.png'}");
							}
							
							if (typeLien == "ACCUEIL") {
								addAttr(lien,"url",format("@@exaDocLink{thmuri:'%1'}", idLien));
							}
							else {
								// 20/12/2016 SF mantis 14715 : correction fdoc des liens Voir aussi
								// addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['%2']}", idLien, sFdoc));
								addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/%2/%3']}", idLien, getCodeProdJouve(idLien.transcript(LowerCase).extract(0,2)).replace(0,"CD36","CD08"), typeLien.replace(0,"FORM2","DPFORM2")));
							}
							insertSubTree(liensDocumentation,-1,lien);
						break;
						case "OTL":
							if liensOutils == nothing {
								liensOutils = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Outils")));
								addAttr(liensOutils,"tri","RANG");
								addAttr(liensOutils,"rang","2");
								addAttr(liensOutils,"info-bulle","Cliquez pour acc&#233;der aux outils");
								addAttr(liensOutils,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/outils.png'}");
							}
							addAttr(lien,"url",format("@@exaDocLink{dockey:'%1',fdoc:['%2']}", idLien, sFdoc));
							insertSubTree(liensOutils,-1,lien);
						break;
					}
				}
				
				if liensDocumentation != nothing insertSubTree(bloc,-1,liensDocumentation);
				if liensOutils != nothing insertSubTree(bloc,-1,liensOutils);
		
			}
		}
		
	}

	// 14/01/2013 al
	// Ne pas ajouter les liens PDF + Texte integral sur les deux études dp15 ABC et GEN
	if giracine == "ETD-OPTJ" && codeMatiere == "CD15" && ! Set("Y5ABC","Y5GEN").knows(idracine){
		bBlocVoirAussi = true;
		// ------------------------------------------------
		// 25/01/2016 alazreg
		// mantis 5860 12590
		// les archive ne sont pas gerees correctement
		// ------------------------------------------------
		var suffixe_archive = "";
		if hasAttr(elem,"ARCHIVE") suffixe_archive = "ARCHIVE"+attr["ARCHIVE",elem];
		var idsynt = idelem.replace(0,"ARCHIVE[0-9]+","");

		// Creer un lien vers la version pdf de la synthese
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se (format pdf)")));
		//addAttr(lien,"url",format("@@exaDoclink{uri:'%1/%2/#/ANX/%3.pdf'}",sFdoc,idelem,idelem.transcript(LowerCase)));
		// rang = 2;
		// addAttr(lien,"rang",dec(rang));		
		addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse (format pdf)"]);

		// YE 17/04/2015 
		if idelem.search(0,"ARCHIVE")  <= 0 {
		addAttr(lien,"url",format("@@exaDoclink{uri:'%1/#/ANX/%3.pdf'}",sFdoc,idelem,idelem.transcript(LowerCase)));
		}else {
			addAttr(lien,"url",format("@@exaDoclink{uri:'EL/CD15/ETD/%1/#/ANX/%3.pdf'}",idelem,idelem,idelem.transcript(LowerCase)));
		}
		// YE Le 22/01/2014 Mantis 5304 
		// Ajout du picto PDF
		//addAttr(lien,"id-picto","pdf");
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");	
		addAttr(lien,"cible","popup");
		// YE 09/04/2015 mantis 10498
		insertSubTree(bloc,-1,lien);		

	
		// Creer un lien vers la convention collective (texte integrale)
		// exemple : y5002 => cc002

		// 16/10/2012 MB il peut y avoir +ieurs ccol pour une synthese
		// Hulk_getCClistFromSynt(lacc)
		// Traiter ce cas plus tard ...
		// *** //
		// var lstCC = Hulk_getCClistFromSynt(idelem.transcript(UpperCase)); 
		var lstCC = Hulk_getCClistFromSynt(idsynt.transcript(UpperCase)); 
		// rang = 10;
		for cc in lstCC {       // *** //
			// cout << format("debugsf cc = %1\n", cc);
			//var idCible = idelem.replace(0,"Y5","CC");
			var idCible = cc;

			if idsynt == "Y5110" cout << format("debug idsynt %1 cc %2\n",idsynt,cc);

			var texteToAdd = "";
			// 12/07/2017 SF/PD SIECDP-271 SIECDP-272
			// if G_HulkCCmapLibelle.knows(idCible) texteToAdd =  G_HulkCCmapLibelle[idCible];
			if G_HulkCCmapLibelle.knows(idCible) texteToAdd =  entityHtml2EntityNum(G_HulkCCmapLibelle[idCible]);
			var texteDuLien = "Texte int&#233;gral";
			if texteToAdd != "" texteDuLien = format("Texte int&#233;gral %1",texteToAdd) ;

			lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",texteDuLien)));

			// YE 09/04/2015 mantis 10498
			// 19/10/2016 alazreg mantis14253 utilisation de la Map g_map_voir_aussi_ccol au lieu de valeurs en dur
			// if rang == 10 rang++;
			
			// 06/04/2016 SF modification de l'ordre des renvois cc pour la synthese Y5482
			// if idsynt == "Y5482" {
				// if idCible == "CC482" rang = 11;
				// if idCible == "CC666" rang = 12;
				// if idCible == "CC665" rang = 13;				
			// }
			
			// addAttr(lien,"rang",dec(rang));

			addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral"]);
			
			// 10/10/2016 alazreg mantis 14253
			if texteDuLien.search(0,"ouvriers") > 0 addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral (ouvriers)"]);
			if texteDuLien.search(0,"ETAM") > 0 addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral (ETAM)"]);
			if texteDuLien.search(0,"cadres") > 0 addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral (cadres)"]);
			if texteDuLien.search(0,"employ&#233;s") > 0 addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral (employés)"]);
			
			// YE Le 22/01/2014 Mantis 5304 
			// Ajout du picto Texte integral
			addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/texte_integral.png'}");
			// 15/10/2012 MB : traitt cas speciaux : y5982 ==> cc282
			// 
			if idCible == "CC982" idCible = "CC282";
			
			var sFdocCible = getFdocPath(codeMatiere,"CCOL-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
			addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}",idCible,suffixe_archive,sFdocCible));
			
			insertSubTree(bloc,-1,lien);		
		}

		// Creer un lien vers les derniers textes integres : fichier recmaj.optj.sgm du dp15
		// exemple : y5002 => cc002
		//lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Derniers textes int&#233;gr&#233;s")));
		//idCible = "DOC12-1";
		//sFdocCible = getFdocPath(codeMatiere,"RECMAJ-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
		//addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
		//insertSubTree(bloc,-1,lien);	

		////////////////////////////////////////////////
		// 02/04/2013 alazreg
		// ajouter un lien à droite "Jurisprudence"
		// qui pointe vers le dp15_comjrp.optj.sgm
		////////////////////////////////////////////////
		
		// on charge la Map ccolinfo si elle n'a pas encore ete chargee
		// if (G_mapCcolInfo == nothing) G_mapCcolInfo = loadObjectFromFile(filePath(env("ELA_LIV"),"miscdata","ccolinfo.map"));
		if (G_mapCcolInfo == nothing) G_mapCcolInfo = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"ccolinfo.map"));
		
		// si le chargement echoue alors on initialise avec une map vide
		if (G_mapCcolInfo == nothing) G_mapCcolInfo = Map();
		
		var idcc = idelem.replace(0,"Y5","CC").transcript(LowerCase);
		
		if G_mapCcolInfo.knows(idcc) {
			if G_mapCcolInfo[idcc].IDjrp != "" {
				idCible = G_mapCcolInfo[idcc].IDjrp;
				var sFdocCible = getFdocPath(codeMatiere,"COMJRP-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Jurisprudence")));
				// rang = 30;
				// addAttr(lien,"rang",dec(rang));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Jurisprudence"]);
				
				// YE Le 22/01/2014 Mantis 5304 
				// Ajout du picto jurisprudence
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/jurisprudence.png'}");	    
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
				insertSubTree(bloc,-1,lien); 
			}
		}
		
		
		// -----------------------------------------------------------------------
		// 11/08/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5 désactiver le lien à droite des synthèses dp15 "Derniers textes intégrés"
		// -----------------------------------------------------------------------
		// 02/04/2013 alazreg
		// ajouter un lien à droite "Derniers textes integres" qui pointe vers le recmaj.optj.sgm
		// -----------------------------------------------------------------------
		// if (G_mapRecmaj == nothing) G_mapRecmaj = initMapRecmaj();

		var IDCC = idcc.transcript(UpperCase);
		// if G_mapRecmaj.knows(IDCC) {
			// idCible = G_mapRecmaj[IDCC];
			// var sFdocCible = getFdocPath(codeMatiere,"RECMAJ-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
			// var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Derniers textes int&#233;gr&#233;s")));
			// -----------------------------------------------------------------------
			// 21/09/2016 alazreg mantis 14215 ordonner les éléments lien
			// -----------------------------------------------------------------------
			// addAttr(lien,"rang",g_map_voir_aussi_ccol["Derniers textes intégrés"]);
		
			// -----------------------------------------------------------------------
			// YE Le 17/02/2014 Mantis 5304 
			// Ajout du picto derniers textes integres	
			// -----------------------------------------------------------------------
			// addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/derniers_textes_integres.png'}");				
			// addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
			// insertSubTree(bloc,-1,lien); 
		// }
			
		// YE 25/03/2015 Mantis 0010498 Lien alerte CC
		// YE 17/04/2015  ajout une exception pour les archives 
		if idelem.search(0,"ARCHIVE")  <= 0  {
			var idAlert = idelem.replace(0,"Y5","15w");		
			var link_alert = format("@@exaDoclink{ url:'${vp-home}/logon.do?forward=/cc/selection/actu.do?theme=%1'}",idAlert);
			var lienAlertCC = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Alerte CC")));
				// rang = 1;
				// addAttr(lienAlertCC,"rang",dec(rang));
				addAttr(lienAlertCC,"rang",g_map_voir_aussi_ccol["Alerte CC"]);
			addAttr(lienAlertCC,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/versions_anterieurs.png'}");				
			// addAttr(lienAlertCC,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
			addAttr(lienAlertCC,"url",link_alert);
			
			// 09/11/2016 SF mantis jouve 12985 : LAB15 ajout du flux RSS de l'alerte CC
			// ceci est fait au moyen des attributs type et datasource-uri"
			// Exemple :
			// type="RssFeed" 
			// datasource-uri="@@exaDoclink{url:'${vp-home}/rss.do?product=15w545'}"
			var rssAlert = format("@@exaDoclink{url:'${vp-home}/rss.do?product=%1'}",idAlert);
			addAttr(lienAlertCC,"type","RssFeed");
			addAttr(lienAlertCC,"datasource-uri",rssAlert);
			
			insertSubTree(bloc,-1,lienAlertCC); 
		}
		
		// 26/09/2016 SF LAB15 mantis 14200 : ajout outil Accords et arretes
		// 26/09/2016 SF LAB15 mantis 14200 : ajout outil Accords et arretes
		// 10/01/2017 alazreg jira https://jira.els-gestion.eu/browse/SIECDP-34
		//            Pas de lien Voir aussi/Accords sur les archives du CD15
		if idelem.search(0,"ARCHIVE")<0{
			var idLienOutil = "http://www.editions-legislatives.fr/ta/Application.html?entity=15_ACCORD&matiere=15";
			var lienOutil = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Accords et arr&#234;t&#233;s")));
			addAttr(lienOutil,"rang",g_map_voir_aussi_ccol["Accords et arrêtés"]);
			addAttr(lienOutil,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/outils.png'}");
			addAttr(lienOutil,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/OUTILS/TA/15-ACCORD']}", idLienOutil));
			insertSubTree(bloc,-1,lienOutil);
		}
	}
	
	// 19/08/2016 SF mantis 13715 LAB15 : ajout du code NAF en version PDF dans la zone Voir aussi
	if giracine == "CORNAFCC-OPTJ" && codeMatiere == "CD15" {
		bBlocVoirAussi = true;
		
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Consulter la version PDF")));
		addAttr(lien,"url",format("@@exaDoclink{uri:'%1'}","EL/CD15/CORNAFCC/NAF2008/sharp_/ANX/NAF.pdf"));
		addAttr(lien,"cible","popup");		
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");	
		insertSubTree(bloc,-1,lien);
	}
	
	// 02/10/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5 : évolution CD15 "Nouveautés de la quinzaine"
	if ((giracine == "ETD-OPTJ" || giracine == "CCOL-OPTJ") && codeMatiere == "CD15"){
		// if codeMatiere == "CD15"
		var idet = idracine.explode("-")[0];
		if idet.rsearch(0,"^CC") != nothing idet = idet.replace(0,"CC","Y5");
		if G_mapIdEtIdRecapnew.knows(idet){
			bBlocVoirAussi = true;
			var idRecapnew = G_mapIdEtIdRecapnew[idet];
			var fdocCible = "EL/CD15/RECAPNEW";
			insertSubTree(bloc,-1,getLien_exaDocLink(docxml,"Nouveautés de la quinzaine",idRecapnew,fdocCible));
		}
	}
	
	////////////////////////////////////////////////
	// 20/03/2013 alazreg
	// mantis 2948
	// Il ne faut pas creer le lien à droite de OC vers Y5
	////////////////////////////////////////////////
	// 21/01/2016 SF mantis 12660 : la OC577 contient un lien vers synthese
	// if giracine == "CCOL-OPTJ" && codeMatiere == "CD15" && idelem.search(0,"CC") ==0{
	// 13/09/2016 SF mantis 13716 LAB15 ajout lien vers le PDF de la CC pour toutes les CC sans exception
	// if giracine == "CCOL-OPTJ" && codeMatiere == "CD15" && (idelem.search(0,"CC") == 0 || idelem == "OC577") {
	if giracine == "CCOL-OPTJ" && codeMatiere == "CD15" {
		bBlocVoirAussi = true;
		if (idelem.search(0,"CC") == 0 || idelem == "OC577") {
			// 22/01/2016 alazreg
			// mantis 12590 : certaines ccol archive ne pointe pas vers la bonne synthese
			// exemple : CC716ARCHIVE2015 pointe vers Y5716ARCHIVE2015 a lieu de Y5110ARCHIVE2015
			// solution : retirer la chaine "ARCHIVE[0-9]+" avant de rechercher dans la Map des exceptions
			
			// ------------------------------------------------
			// 25/01/2016 alazreg
			// mantis 5860 12590
			// les archive ne sont pas gerees correctement
			// ------------------------------------------------
			var suffixe_archive = "";
			if hasAttr(elem,"ARCHIVE") suffixe_archive = "ARCHIVE"+attr["ARCHIVE",elem];
			
			var idccol = idelem.replace(0,"ARCHIVE[0-9]+","");
			// Creer un lien vers la version pdf de la synthese
			// 15/10/2012 MB : cette partie est "explosee" en 3 cas : CAS 1., CAS 2. et CAS 3. (voir suite)

			// var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","La synth&#232;se")));
			// var idCible = idelem.replace(0,"CC","Y5");
			// var sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
			// addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
			// insertSubTree(bloc,-1,lien);

			// 15/10/2012 ajout MB : ces modifs doivent être factorisees + tard dans ccol.lib (pour le moment 
			//                       cette librairie ne marche pas .... (idem dans cdcoll_create.bal)
			if idccol == "CC282" {
				//  CAS 1.traitt cas special : la cc282 doit avoir deux syntheses (dans la partie voir ausi) : y5982 + y5282
				// on ajoute donc la y5282 + la y5982

				// cout << format("DEBUG MB CAS PARTICULIER : CC282 ==> 2 syntheses\n");
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se (champs d'application)")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse (champs d'application)"]);
				var sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ","Y5982"); 
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}","Y5982",suffixe_archive,sFdocCible));
				// YE Le 22/01/2014 Mantis 5304 
				// Ajout du picto La synth&#232;se (champs d'application)
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
				insertSubTree(bloc,-1,lien);

				lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se (accords nationaux)")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse (accords nationaux)"]);
				sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ","Y5282"); 
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}","Y5282",suffixe_archive,sFdocCible));
				// YE Le 22/01/2014 Mantis 5304 
				// Ajout du picto La synth&#232;se (accords nationaux)
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
				insertSubTree(bloc,-1,lien);
			}
			else if G_setCcolBatiment.knows(idccol) {
				//   CAS 2. traitt CC batiment
				//   synthese y5040  + y5940
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se (CCN)")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse (CCN)"]);
				var sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ","Y5040"); 
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}","Y5040",suffixe_archive,sFdocCible));
				// SF Le 23/01/2014 Mantis 5304 
				// Ajout du picto La synth&#232;se (CCN)
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
				insertSubTree(bloc,-1,lien);	

				lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se (r&#233;gions)")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse (région)"]);

				sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ","Y5940"); 
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}","Y5940",suffixe_archive,sFdocCible));
				// SF Le 23/01/2014 Mantis 5304 
				// Ajout du picto La synth&#232;se (r&#233;gions)
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
				insertSubTree(bloc,-1,lien);  
			}
			else if (G_mapCCversSynt.knows(idccol)){     // encore un cas !
				//   CAS 3. ccol avec un num different de celui de synt
				// cout << format("debug idccol = %1\n", idccol);
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse"]);
				var idCible = G_mapCCversSynt[idccol];
				var sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}",idCible,suffixe_archive,sFdocCible));
				// SF Le 23/01/2014 Mantis 5304 
				// Ajout du picto La synth&#232;se
				addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
				insertSubTree(bloc,-1,lien);
				
				// -----------------------------------------------------------------------------
				// 21/01/2016 SF mantis 12660 la cc422 contient un lien vers la jurisprudence
				// j'ajoute ce bloc pour toutes les cc qui contiennent un lien vers la jurisprudence
				// -----------------------------------------------------------------------------
				// on charge la Map ccolinfo si elle n'a pas encore ete chargee
				if (G_mapCcolInfo == nothing) G_mapCcolInfo = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"ccolinfo.map"));
				// si le chargement echoue alors on initialise avec une map vide
				if (G_mapCcolInfo == nothing) G_mapCcolInfo = Map();
				var idelem_low = idelem.transcript(LowerCase);
				if G_mapCcolInfo.knows(idelem_low) {
					// cout << format("\ndebug idcc = %1\n", idelem_low);
					if G_mapCcolInfo[idelem_low].IDjrp != "" {
						idCible = G_mapCcolInfo[idelem_low].IDjrp;
						var sFdocCible = getFdocPath(codeMatiere,"COMJRP-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
						var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Jurisprudence")));
						addAttr(lien,"rang",g_map_voir_aussi_ccol["Jurisprudence"]);
						// SF Le 23/01/2014 Mantis 5304 
						// Ajout du picto jurisprudence
						addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/jurisprudence.png'}");
						addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
						insertSubTree(bloc,-1,lien); 
					}
				}

			}
			else if (!G_setCcolSansSynt.knows(idccol)){  // les cc du set G_setCcolSansSynt (ccol.lib) sont des cc sans synthese 
				//   CAS 4. cas general
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Synth&#232;se")));
				addAttr(lien,"rang",g_map_voir_aussi_ccol["Synthèse"]);
				// 26/11/2012 MB : traiter le cas des OC 
				// 
				//var idCible = idccol.replace(0,"CC","Y5");
				var idCible = "";

				////////////////////////////////////////////////
				// 20/03/2013 alazreg
				// mantis 2948
				// Il ne faut pas creer le lien à droite de OC ver Y5
				////////////////////////////////////////////////
				//if idccol.search(0,"CC") >=0 idCible = idccol.replace(0,"CC","Y5");
				//else idCible = idccol.replace(0,"OC","Y5");

				if idccol.search(0,"CC") ==0 {
					idCible = idccol.replace(0,"CC","Y5");
					var sFdocCible = getFdocPath(codeMatiere,"ETD-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
					addAttr(lien,"url",format("@@exaDoclink{dockey:'%1%2', fdoc:['%3']}",idCible,suffixe_archive,sFdocCible));
					// SF Le 23/01/2014 Mantis 5304 
					// Ajout du picto La synth&#232;se
					addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/syntheses.png'}");
					insertSubTree(bloc,-1,lien); 
				}

				////////////////////////////////////////////////
				// 02/04/2013 alazreg
				// ajouter un lien à droite "Jurisprudence"
				// qui pointe vers le dp15_comjrp.optj.sgm
				////////////////////////////////////////////////

				// on charge la Map ccolinfo si elle n'a pas encore ete chargee
				// if (G_mapCcolInfo == nothing) G_mapCcolInfo = loadObjectFromFile(filePath(env("ELA_LIV"),"miscdata","ccolinfo.map"));
				if (G_mapCcolInfo == nothing) G_mapCcolInfo = loadObjectFromFile(filePath(env("ELA_TMP_IDX"),"ccolinfo.map"));

				// si le chargement echoue alors on initialise avec une map vide
				if (G_mapCcolInfo == nothing) G_mapCcolInfo = Map();

				var idelem_low = idelem.transcript(LowerCase);

				if G_mapCcolInfo.knows(idelem_low) {
					if G_mapCcolInfo[idelem_low].IDjrp != "" {
						idCible = G_mapCcolInfo[idelem_low].IDjrp;
						var sFdocCible = getFdocPath(codeMatiere,"COMJRP-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
						var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Jurisprudence")));
						addAttr(lien,"rang",g_map_voir_aussi_ccol["Jurisprudence"]);
						// SF Le 23/01/2014 Mantis 5304 
						// Ajout du picto jurisprudence
						addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/jurisprudence.png'}");
						addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
						insertSubTree(bloc,-1,lien); 
					}
				}

			}
			
		// -----------------------------------------------------------------------
		// 11/08/2017 alazreg https://jira.els-gestion.eu/browse/SIEDV-5 désactiver le lien à droite des synthèses dp15 "Derniers textes intégrés"
		// -----------------------------------------------------------------------
			//05/06/2014 sfouzi sortir le lien "Derniers textes integres"
		// -----------------------------------------------------------------------
			// 02/04/2013 alazreg
		// ajouter un lien à droite "Derniers textes integres" qui pointe vers le recmaj.optj.sgm
		// -----------------------------------------------------------------------
		// if (G_mapRecmaj == nothing) G_mapRecmaj = initMapRecmaj();
		// if G_mapRecmaj.knows(idelem) {
			// idCible = G_mapRecmaj[idelem];
			// var sFdocCible = getFdocPath(codeMatiere,"RECMAJ-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
			// var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Derniers textes int&#233;gr&#233;s")));
					// 21/09/2016 alazreg mantis 14215
			// addAttr(lien,"rang",g_map_voir_aussi_ccol["Derniers textes intégrés"]);

			// addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
				// YE Le 17/02/2014 Mantis 5304 
				// Ajout du picto derniers textes integres		
			// addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/derniers_textes_integres.png'}");	
			// insertSubTree(bloc,-1,lien); 
		// }
			
			// fin ajouts MB

			// Creer un lien vers les derniers textes integres : fichier recmaj.optj.sgm du dp15
			// exemple : y5002 => cc002
		//lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Derniers textes int&#233;gr&#233;s")));
			//idCible = "DOC12-1";
			//sFdocCible = getFdocPath(codeMatiere,"RECMAJ-OPTJ",idCible); // exemple de FDOC : EL/CDXX/TYPEOUVRAGE  EL/TSA/ETD/PACK2
			//addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
			//insertSubTree(bloc,-1,lien);
			
			// YE 22/04/2015 Mantis 10696 Lien alerte CC
			
			if idelem.search(0,"ARCHIVE")  < 0 && !G_setCcolSansSynt.knows(idelem)  {
				// 07/12/2016 SF mantis 14648 ajout alerte CC pour les conventions qui commencent par OC
				// var idAlert = idelem.replace(0,"CC","15w");		
				var idAlert = idelem.replace(0,"CC","15w").replace(0,"OC","15w");		
				var link_alert = format("@@exaDoclink{ url:'${vp-home}/logon.do?forward=/cc/selection/actu.do?theme=%1'}",idAlert);
				var lienAlertCC = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Alerte CC")));
				// rang = 1;
				// addAttr(lienAlertCC,"rang",dec(rang));
				addAttr(lienAlertCC,"rang",g_map_voir_aussi_ccol["Alerte CC"]);
					
				addAttr(lienAlertCC,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/versions_anterieurs.png'}");				
				// addAttr(lienAlertCC,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idCible,sFdocCible));
				addAttr(lienAlertCC,"url",link_alert);		
				insertSubTree(bloc,-1,lienAlertCC); 
			}
		}
	
		// 13/09/2016 SF mantis 13716 LAB15 ajout lien vers le PDF de la CC
		var idcc = "";
		// 10/01/2017 alazreg jira https://jira.els-gestion.eu/browse/SIECDP-34
		//            Pas de lien Voir aussi/Texte integral pdf sur les archives du CD15 avant 2016
		// Pour les archives >= 2016 on pose le lien vers le PDF
		if (idelem.search(0,"ARCHIVE")<0 || (idelem.search(0,"ARCHIVE")>0 && dec(idelem.extract(idelem.length()-4,-1)) >= 2016)){
		if hasAttr(racine,"IDCC") idcc = attr["IDCC",racine];
			// cout << format("******************** IDCC 1 = %1\n", idcc);
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Texte int&#233;gral (format pdf)")));
		addAttr(lien,"rang",g_map_voir_aussi_ccol["Texte intégral (format pdf)"]);
			// if (idcc == "") addAttr(lien,"url",format("@@exaDoclink{uri:'EL/CD15/CCOL/%1/#/ANX/%1.pdf'}",idelem.transcript(UpperCase)));	
			// else addAttr(lien,"url",format("@@exaDoclink{uri:'EL/CD15/CCOL/%2/#/ANX/%1_%2.pdf'}",idcc,idelem.transcript(UpperCase)));
			var pdf = format("%1.pdf",idelem);
			if idcc != "" pdf = format("%1_%2.pdf",idcc,idelem);
			pdf = pdf.replace(0,"ARCHIVE","archive");
			addAttr(lien,"url",format("@@exaDoclink{uri:'EL/CD15/CCOL/%1/#/ANX/%2'}",idelem,pdf));
			
		addAttr(lien,"cible","popup");		
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/pdf.png'}");	
		insertSubTree(bloc,-1,lien);
		}
		
		// 26/09/2016 SF LAB15 mantis 14200 : ajout outil Accords et arretes
		// 10/01/2017 alazreg jira https://jira.els-gestion.eu/browse/SIECDP-34
		//            Pas de lien Voir aussi/Accords sur les archives du CD15
		if idelem.search(0,"ARCHIVE")<0{
			var idLienOutil = "http://www.editions-legislatives.fr/ta/Application.html?entity=15_ACCORD&matiere=15";
			var lienOutil = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Accords et arr&#234;t&#233;s")));
			addAttr(lienOutil,"rang",g_map_voir_aussi_ccol["Accords et arrêtés"]);
			addAttr(lienOutil,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/outils.png'}");
			addAttr(lienOutil,"url",format("@@exaDocLink{dockey:'%1',fdoc:['EL/OUTILS/TA/15-ACCORD']}", idLienOutil));
			insertSubTree(bloc,-1,lienOutil);
		}
	}
	
	if Set("DPFORM2-OPTJ").knows(giracine){
		bBlocVoirAussi = true;
		// 30/07/2013 MB : mettre Modeles partout (GPxx/DPxx) : demande redaction mantis 4221
		var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Personnaliser le mod&#232;le")));
		addAttr(lien,"info-bulle","Personnaliser le mod&#232;le");
		// YE Le 17/02/2014 Mantis 5304 
		// Ajout du picto personnaliser le modele		
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/personnaliser_le_modele.png'}");	
		//addAttr(lien,"id-picto","personnaliser");
		addAttr(lien,"cible","popup");

		// ------------------------------------------------
		// 24/09/2012 AL
		// Creation de lien de la formule de consultation vers la formule interactive
		// cf mail armand betton du 20/09/2012 16:14
		// Lien avec formkey a la place de dockey
		// ------------------------------------------------
		// 24/07/2014 alazreg
		// mantis 7241 formule dalloz
		// Pour l'url des formules MFI la chaine jouve remplace le caractere "-" par "_"
		// Exemple : http://test.validation.elnet.fr/formulaires/Form/index/FORMPCIV_0031
		// Cela provoque une erreur de pointage car la cible est FORMPCIV-0031
		// Voir avec Armand BETTON attribut fid dans le delivery.xml ou ailleurs
		// Or les formules dalloz contiennent un "-" dans l'ID
		// En attendant une correction jouve je remplace pour les formules dalloz la cible "-" par "_"
		// Cela permet a dalloz d'avancer dans la recette des formules traitees via EL
		// ------------------------------------------------
		if editeur == "DZ" addAttr(lien,"url",format("@@exaDocLink{formkey:'%1',fdoc:['%2']}",idelem.replace(0,"-","_"),sFdoc));
		// 31/05/2016 ajout pack ope
		else if (hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES")
			addAttr(lien,"url",format("@@exaDocLink{formkey:'%1',fdoc:['EL/PACKOPE/%2/DPFORM2']}",idelem,attr["PACKID", racine]));
		else addAttr(lien,"url",format("@@exaDocLink{formkey:'%1',fdoc:['%2']}",idelem,sFdoc));
		//exemple : <lien info-bulle="Afficher la version interactive" id-picto="XXX" cible="vue" url="@@exaDoclink{formkey:'Z2M031001',fdoc:['EL/CD02/DPFORM2']}">
		
		// 17/12/2012 MB : lien "Personnalise la formule" seulement si formule interactive
		if !Set("FORMT4","FORMT5").knows(GI(elem)) {
			insertSubTree(bloc,-1,lien);
		}
		//else cout << format("/////////DEBUG MB 17/12/2012 Pas de bloc (Voir aussi) pour %1\n",GI(elem)); 
		// fin ajout MB
	}

	// ------------------------------------------------------------------
	// 04/07/2013 alazreg
	// les liens vers les textes anterieurs ne fonctionnent pas tres bien
	// A la demande de la MOA ces liens sont desactives temporairement
	// Fiches mantis : 2552 2621 2535 2814 3017 2837 2966 1394
	// 
	// Pour faciliter la lecture du code source j'ajoute un booleen bAjouterLienTxtAnt = false
	// ------------------------------------------------------------------
	var bAjouterLienTxtAnt = false;
	// ------------------------------------------------------------------
	// 09/08/2013 alazreg
	// Pour pouvoir debugguer activer les liens textes anterieurs sur test.validation.elnet.fr
	// ------------------------------------------------------------------
	bAjouterLienTxtAnt = true;
	
	if bAjouterLienTxtAnt && G_mapTxtMDF != nothing && G_mapTxtMDF.knows(idelem){		
		// ------------------------------------------------------------------
		// Versions anterieures des textes
		// Versionning des textes
		// ------------------------------------------------------------------
		var liens = Node(docxml,"liens",Node(docxml,"libelle",Node(docxml,"#CDATA","Versions ant&#233;rieures")));
		
		// 19/02/2014 MB : pour forcer le tri des liens dans versions ant鲩eures ==> on ajoute l'attribut tri ࠒANG
		addAttr(liens,"tri","RANG");
		addAttr(liens,"info-bulle","Cliquez pour acc&#233;der aux versions ant&#233;rieures disponibles de ce texte");
		// SF Le 23/01/2014 Mantis 5304 
		// Ajout du picto jurisprudence
		//addAttr(liens,"id-picto","versioning");
		addAttr(liens,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/versions_anterieurs.png'}");
		
		G_mapMDFduTexte = G_mapTxtMDF[idelem];
		//var srefidsToUse = getVersionningMdfctList(G_mapMDFduTexte,G_maptxtversionning);
		// cout << format("\n\n************************** G_mapMDFduTexte = %1*******************************\n\n",G_mapMDFduTexte); 
		
		var srefidsToUse = Set();
		var lrefidsToUse = List();
		var lastpos = 0;
		for key,value in G_mapMDFduTexte{
			if G_mapMDFduTexte[key].mdfpos >  lastpos lastpos = G_mapMDFduTexte[key].mdfpos;
		}
		// cout << format("\n\n************************** lastpos = %1*******************************\n\n",lastpos);

		// trier les cles
		for key in eSort(G_mapMDFduTexte, function(k) {
			/*var crittri;
			var jma;
			jma = G_mapMDFduTexte[k].mdfdate;
			var lsttokjjmmaa = jma.explode("/");
			if lsttokjjmmaa.length() == 3 {
				var jj = dec(lsttokjjmmaa[0]);
				var mm = dec(lsttokjjmmaa[1]);
				var aa = dec(lsttokjjmmaa[2]);
				crittri = format("%1$04d%2$02d%3$02d", 9999-aa, 99-mm, 99-jj);
			}
			if crittri == "" crittri = format("%1",k) ;
			return crittri ;*/
			// 16/04/2014 MB : on ne recalcule plus le tri mais on utilide directement le mdfpos deja calcul頥n amont de la chaine : mantis 2535
			return -G_mapMDFduTexte[k].mdfpos;
		}) 
		{				;
			if G_mapMDFduTexte[key].mdfpos ==  lastpos continue;
			//keytmp = keytmp.transcript(idversionneLowerCase);
			var keytmp = key.transcript(LowerCase);
			var listKey = keytmp.explode("-");
			keytmp = listKey[0] +  "-mdfct" + listKey[1];
			// cout << format("keytmp \"%1\"\n",keytmp);
			
			if G_maptxtversionning.knows(keytmp){
				// cout << format("\tdans versionning ==> keytmp \"%1\"\n",keytmp);
				// si les libelles (chaine Mod par du MDFCT sont 駡les)
				// pose pb avec le tri ==> on utilise une liste pour maintenir l'ordre
				srefidsToUse << keytmp.replace(0,"-mdfct","-").transcript(UpperCase);
				lrefidsToUse <<  keytmp.replace(0,"-mdfct","-").transcript(UpperCase);
			}							
		}
		// cout << format("\n\n********* srefidsToUse = %1**********\n***** lrefidsToUse = %2*****\n\n",srefidsToUse,lrefidsToUse);
		// 08/10/2013 MB : on filtre les IDs de mapMDF : seuls ceux du set srefidsToUse sont a mettre dans "versions anterieures"
		//for idAnt in G_mapTxtMDF[idelem]{
		
		// 09/10/2013 MB :ajouter liens si contient au moins un elt lien
		// TODO : il reste le pbde l'ordre des versions a resoudre ==> laliste des versions doit etre presentee dans l'ordre chrono.
		// 19/02/2014 MB : il faut trier les "Versions ant鲩eures" ==> je met une liste au lieu du set utilis頪usque lʉ//if srefidsToUse.length() > 0 {
		if lrefidsToUse.length() > 0 {
			insertSubTree(bloc,-1,liens);
			bBlocVoirAussi = true;
			// fin ajouts MB
			var rang = 0;
			for idAnt in lrefidsToUse {
				rang = rang + 1;
				//cout << format("\t************ idAnt = %1**************\n",idAnt);
				var libelleTxtAnt = G_mapTxtMDF[idelem][idAnt].mdftitle;
		// 23/04/2014 SF/MB : en attendant de resoudre le mystere de l entite ring (&#248;) ajoutee, on la traite ici
				libelleTxtAnt = libelleTxtAnt.replace(0,"no &ring;","n&#176;").replace(0,"no &#248;","n&#176;").replace(0,"no&nbsp;&#248;","n&#176;").replace(0,"&#248;","").replace(0,"&ring;","").replace(0,"&nbsp;&nbsp;","&nbsp;");

				// 06/11/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-399 correction bug sur lien/libelle
				var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA",format("<![CDATA[%1]]>",libelleTxtAnt))));
				// 19/02/2014 MB : desormais on tri des versions ant鲩eures des txt
				addAttr(lien,"rang",dec(rang));
				addAttr(lien,"cible","vue");
				// 06/11/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-399 correction bug sur lien@info-bulle
				// addAttr(lien,"info-bulle",libelleTxtAnt);
				addAttr(lien,"info-bulle",libelleTxtAnt.transcript(g_map_entites_nommees_to_unicode));
				// La mapMDF contient des identifiants du type ARR77001-0 ARR73552-15
				// Les textes anterieurs ont des identifiants du type ARR77001-MDFCT0 ARR73552-MDFCT15
				// Je fais un replace pour calculer l'identifiant cible du texte anteieur
				
				addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['EL/SOURCES/TXT']}",idAnt.replace(0,"-","-MDFCT")));
				// YE Le 22/01/2014 Mantis 5304 
				//addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/versions_anterieurs.png'}");
				insertSubTree(liens,-1,lien);
			}
		}

	}

	// 20/07/2012 AL
	// Creer les liens entre les index chrono/etude/thematique
	switch(giracine){
	case "IDX-OPTJ":
		bBlocVoirAussi = true;
		var lien = nothing;
		lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Classement par &#233;tude")));
		addAttr(lien,"cible","vue");
		addAttr(lien,"info-bulle","Classement par &#233;tude");
		// YE Le 22/01/2014 Mantis 5304
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/classement_par_etude.png'}");
		addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idelem.replace(0,"IDX(THEMA)?","IDXET"),getFdocPath(codeMatiere,"IDXET-OPTJ","")));
		insertSubTree(bloc,-1,lien);

		// Attention a l'index des thematique dans le cas du CD08
		// Si on est dans l'index chrono alors on cree un lien vers index par thematique
		// Si on est dans l'index par thematique alors on cree un lien vers index chrono
		if codeMatiere == "CD08" && attr["TYPE",elem] == "TXT" && attr["ID",elem].search(0,"IDXTHEMA") < 0 {
			lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Classement par th&#233;matique")));
			addAttr(lien,"cible","vue");
			addAttr(lien,"info-bulle","Classement par th&#233;matique");
			// YE Le 17/02/2014 Mantis 5304 
			// Ajout du picto Classement par thematique		
			addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/classement_par_thematique.png'}");	
			addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idelem.replace(0,"IDX","IDXTHEMA"),getFdocPath(codeMatiere,"IDX-OPTJ","")));
			insertSubTree(bloc,-1,lien);
		}

		if codeMatiere == "CD08" && attr["TYPE",elem] == "TXT" && attr["ID",elem].search(0,"IDXTHEMA") > 0 {
			lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Classement chronologique")));
			addAttr(lien,"cible","vue");
			addAttr(lien,"info-bulle","Classement chronologique");
			addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idelem.replace(0,"IDXTHEMA","IDX"),getFdocPath(codeMatiere,"IDX-OPTJ","")));
			// YE Le 17/02/2014 Mantis 5304 
			// Ajout du picto Classement chronologique		
			addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/classement_chronologique.png'}");	
			insertSubTree(bloc,-1,lien);
		}
		break;

	case "IDXET-OPTJ":
		bBlocVoirAussi = true;
		lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Classement chronologique")));
		addAttr(lien,"cible","vue");
		addAttr(lien,"info-bulle","Classement chronologique");
		addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idelem.replace(0,"IDXET","IDX"),getFdocPath(codeMatiere,"IDX-OPTJ","")));
		// YE Le 22/04/2014 Mantis 5304 
		// Ajout du picto Classement chronologique		
		addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/classement_chronologique.png'}");	
		insertSubTree(bloc,-1,lien);

		if codeMatiere == "CD08" && attr["TYPE",elem] == "TXT"{
			lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Classement par th&#233;matique")));
			addAttr(lien,"cible","vue");
			addAttr(lien,"info-bulle","Classement par th&#233;matique");
			addAttr(lien,"url",format("@@exaDoclink{dockey:'%1', fdoc:['%2']}",idelem.replace(0,"IDXET","IDXTHEMA"),getFdocPath(codeMatiere,"IDX-OPTJ","")));
			insertSubTree(bloc,-1,lien);
		}
		break;
	}

	// Le bloc Voir aussi peut etre absents dans certains cas
	// On retourne nothing
	// C'est au programme appelant de verifier le retour
	if not bBlocVoirAussi bloc = nothing;
	return bloc;
}


function creerBlocVousSouhaitez(docxml,codeMatiere,elem,idracine,giracine,idelem,titreElem,filenametoc,libMatiere,sFdoc){
	// 22/06/2012 AL/MB
	// Le bloc Vous souhaitez est abandonne dans HULK - vu avec Aude DEFRETIERE
	// Cette fonction est abandonnee
	
	var bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Vous souhaitez"))); 
	addAttr(bloc,"type","vous_souhaitez");

	var bBlocVousSouahitez = false;    
	
	if not bBlocVousSouahitez bloc = nothing;
	return bloc;
}

function creerBlocVousPouvez(docxml,elem,idracine,giracine,idelem,titreElem,filenametoc,libMatiere,sFdoc){
	// 22/06/2012 AL/MB
	// Vu avec Aude DEFRETIERE
	// Le bloc Vous pouvez est abandonne
	var bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Vous pouvez"))); 
	addAttr(bloc,"type","vous_pouvez");

	var bBlocVousPouvez = false;

	//    if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine){
	bBlocVousPouvez = true;
	var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Ins&#233;rer dans dossier"))); 
	insertSubTree(bloc,-1,lien);
	addAttr(lien,"info-bulle","Ins&#233;rer dans dossier");
	addAttr(lien,"id-picto","inserer_dossier");
	addAttr(lien,"cible","vue");
	addAttr(lien,"url",format("@@exaDocLink{dockey:'xxx%1',fdoc:['%2']}",idelem,sFdoc));
	
	lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","Ins&#233;rer note"))); 
	insertSubTree(bloc,-1,lien);
	addAttr(lien,"info-bulle","Ins&#233;rer note");
	addAttr(lien,"id-picto","inserer_note");
	addAttr(lien,"cible","vue");
	addAttr(lien,"url",format("@@exaDocLink{dockey:'xxx%1',fdoc:['%2']}",idelem,sFdoc));
	//}
	
	if not bBlocVousPouvez bloc = nothing;
	return bloc;
}

function make_uci(docxml,codeMatiere,typeOuvrage,idracine,idelem,titreElem,libMatiere,sFdoc){
	//cout << format("je suis dans make_uci(%1,%2)\n",idracine,idelem);
	var gendoc = Node(docxml,"gendoc",false);
	addAttr(gendoc,"uri",format("EL/%1/%2/%3/#/UCI/%4",codeMatiere,typeOuvrage,idracine,idelem));
	//insertSubTree(gendocsUci,-1,gendoc);
	
	var uc = Node(docxml,"uc",false);

	// 02/10/2012 AL
	// Vu avec jouve
	// Pour les formules indiquer type="sgml-id" et non "document"
	addAttr(uc,"type","document");
	//addAttr(uc,"type","sgml-id");
	addAttr(uc,"uri",attr["uri",gendoc]);
	insertSubTree(gendoc,-1,uc);
	
	var donnees = Node(docxml,"donnees",Node(docxml,"titre",Node(docxml,"#CDATA",titreElem)));
	insertSubTree(uc,-1,donnees);
	
	var support = Node(docxml,"support",Node(docxml,"#CDATA","Support DP"));
	// 14/12/2012 MB : pour LP35 ==> mettre LP
	cout << format("++++++++++++LP35++++++++++ : codeMatiere = %1\n",codeMatiere);
	if codeMatiere == "LP35"
	addAttr(support,"code","LP");
	else
	addAttr(support,"code","DP");
	
	insertSubTree(donnees,-1,support);
	
	var matiere = Node(docxml,"matiere",Node(docxml,"#CDATA","Matiere "+codeMatiere));
	addAttr(matiere,"code",codeMatiere);
	insertSubTree(donnees,-1,matiere);

	var presentation = Node(docxml,"presentation",false);
	insertSubTree(uc,-1,presentation);
	
	var action = Node(docxml,"action",false);
	addAttr(action,"type","html-header");
	insertSubTree(presentation,-1,action);
	
	var parametre = Node(docxml,"parametre",false);
	insertSubTree(action,-1,parametre);
	
	var linkNode = Node(docxml,"link",true);
	addAttr(linkNode,"rel","stylesheet");
	addAttr(linkNode,"type","text/css");
	//addAttr(linkNode,"href",format("@@mgoStatic{uri:'/EL/#TRANSVERSE/www/mfi/css/el-form_di_ex_%1.css'}",codeMatiere));
	addAttr(linkNode,"href",format("@@mgoStatic{uri:'/EL/#TRANSVERSE/www/html/css/el-form_di_ex_%1.css'}",codeMatiere));
	insertSubTree(parametre,-1,Node(docxml,"#CDATA","<![CDATA["));
	insertSubTree(parametre,-1,linkNode);
	insertSubTree(parametre,-1,Node(docxml,"#CDATA","]]>"));

	var blocDocumentUci = Node(docxml,"bloc",false);
	addAttr(blocDocumentUci,"type","document");
	insertSubTree(uc,-1,blocDocumentUci);
	
	insertSubTree(blocDocumentUci,-1,Node(docxml,"titre",Node(docxml,"#CDATA","Vous souhaitez")));
	
	var info = Node(docxml,"info",false);
	insertSubTree(info,-1,Node(docxml,"#CDATA","<![CDATA["+libMatiere+"<br/>"+titreElem+"]]>"));
	insertSubTree(blocDocumentUci,-1,info);
	
	var lien = Node(docxml,"lien",Node(docxml,"libelle",Node(docxml,"#CDATA","<![CDATA[Afficher la version de consultation]]>")));
	addAttr(lien,"info-bulle","Afficher la version de consultation");

	// YE Le 17/02/2014 Mantis 5304 
	// Ajout du picto afficher la version de consultation	
	//addAttr(lien,"id-picto","consultation");
	addAttr(lien,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/afficher_la_version_de_consultation.png'}");	
	
	addAttr(lien,"cible","vue");
	// 24/09/2012 AL
	// On ne precise pas le fdoc de la version interactive vers la version de consultation
	// cf mail armand betton du 20/09/2012 16:14
	//addAttr(lien,"url",format("@@exaDoclink{dockey:'%1',fdoc:['%2']}",idelem,sFdoc));
	addAttr(lien,"url",format("@@exaDoclink{dockey:'%1'}",idelem,sFdoc));

	//<lien info-bulle="Afficher la version de consultation" id-picto="consultation" cible="vue" url="@@exaDoclink{dockey:'Z2M031001'}">
	insertSubTree(blocDocumentUci,-1,lien);

	return gendoc;
}


function creerCheminWwwMatiereOuvrageDepuisRacine(racine){
	// 10/10/2012 AL
	// Cette fonction cree le dossier $ELA_LIV/www/CDxx/Matiere/Ouvrage/ à partir des informations contenu dans le sgml
	// Exemple pour Z2001
	// $ELA_LIV/www/CD02/ETD/Z2001/

	if hasAttr(racine,"TYPEOUVRAGE"){
		var chemin = format("",env("ELA_LIV"),attr["MATIERE",racine],attr["TYPEOUVRAGE",racine]);
	}
	else return 1;

	return 0;
}

function get_gendoc_ucx_archive_cd15(racine){
	// cette fonction retourne un element gendoc pour les archives des ccol + syntheses du cd15
	var id_racine = attr["ID",racine];
	var gi_racine = GI(racine);
	
	// pas d'archive pour les deux etudes Y5ABC Y5GEN du CD15
	if id_racine.search(0,"Y5ABC") == 0 return nothing;
	if id_racine.search(0,"Y5GEN") == 0 return nothing;
	
	//var doc = document(racine);
	var doc = newCoreDocument();
	var gendoc = Node(doc,"gendoc",false);
	//insertSubTree(gendoc,-1,Node(doc,"#COMMENT","DEBUG genere par le prog hulk.bal, function get_gendoc_ucx_archive_cd15"));
	
	// on clacule l'id de la cible CCXXX ou Y5XXX
	var id_cible = "id_cible";
	if gi_racine == "CCOL-OPTJ" id_cible = id_racine.extract(0,5);
	if gi_racine == "ETD-OPTJ" id_cible = id_racine.extract(0,5);

	var typeouvrage = "typeouvrage";
	if hasAttr(racine,"TYPEOUVRAGE") typeouvrage = attr["TYPEOUVRAGE",racine];

	var uri = format("EL/CD15/%1/%2/#/UCX/1",typeouvrage,id_racine);
	var uri_cible = format("EL/CD15/%1/%2/#/UC",typeouvrage,id_cible);
	addAttr(gendoc,"uri",uri);
	addAttr(gendoc,"uri-cible",uri_cible);
	
	var uc = Node(doc,"uc",false);
	addAttr(uc,"type","ouvrage");
	addAttr(uc,"uri",uri_cible);
	insertSubTree(gendoc,-1,uc);
	
	var bloc = Node(doc,"bloc",Node(doc,"titre",Node(doc,"#CDATA","Voir aussi")));
	addAttr(bloc,"type","voir_aussi");
	insertSubTree(uc,-1,bloc);

	// 06/03/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-34
	// Modifier le libellé des versions antérieures ccol et synt
	var sLibVersionsAnterieurs = "Synthèse (versions antérieures)";
	if gi_racine == "ETD-OPTJ" sLibVersionsAnterieurs = "Synthèse (versions antérieures)";
	if gi_racine == "CCOL-OPTJ" sLibVersionsAnterieurs = "Texte intégral (versions antérieures)";

	// 19/10/2016 alazreg mantis jouve 13189 modifier le libellé « archives » de la zone "voir aussi" des conventions collectives (texte intégral et synthèse) en « versions antérieures »
	var liens = Node(doc,"liens",Node(doc,"libelle",Node(doc,"#CDATA",sLibVersionsAnterieurs.transcript(g_map_entites_nommees_to_unicode))));
	// var rangVoirAussi = 40;
	// addAttr(liens,"rang",dec(rangVoirAussi));	
	addAttr(liens,"rang",g_map_voir_aussi_ccol[sLibVersionsAnterieurs]);
	addAttr(liens,"info-bulle",sLibVersionsAnterieurs.transcript(g_map_entites_nommees_to_unicode));
	// addAttr(liens,"id-picto","liencommentaire");
	// YE Le 02/04/2014 Mantis 5304 
	// Ajout du picto versions_anterieurs.png pour le lien Archives
	addAttr(liens,"id-picto","@@mgostatic{uri:'EL/PUCESMAT/versions_anterieurs.png'}");	
	// YE 04/04/2014 mantis 6632
	// Ajout de l attr tri 
	addAttr(liens,"tri","RANG");	
	
	insertSubTree(bloc,-1,liens);

	var titre_ouvrage = getTitreElem(racine,gi_racine,id_racine);
	
	// var lien = Node(doc,"lien",Node(doc,"libelle",Node(doc,"#CDATA",titre_ouvrage)));
	var lien = Node(doc,"lien",Node(doc,"libelle",Node(doc,"#CDATA",titre_ouvrage.transcript(g_map_entites_nommees_to_unicode))));
	addAttr(lien,"cible","vue");
	addAttr(lien,"info-bulle","");
	// YE 04/04/2014 mantis 6632
	// Ajout de l attr rang	
	// var rang = getRang(id_racine);
	var rang = getRang_archive_dp15(racine);
	if rang != nothing addAttr(lien,"rang", dec(rang));
	
	// YE 10602 Le 09/04/2015 Changer le fdoc des archive en EL/CD15/CCOL/IDDELACC  
	// var url = format("@@exaDoclink{dockey:'%1',fdoc:['EL/CD15/%2']}",id_racine,typeouvrage);
	var url = format("@@exaDoclink{dockey:'%1',fdoc:['EL/CD15/%2/%3']}",id_racine,typeouvrage,id_racine.replace(0,"ARCHIVE[0-9][0-9][0-9][0-9]",""));
	addAttr(lien,"url",url);
	insertSubTree(liens,-1,lien);

	return gendoc;
}

function get_node_bloc_document(docxml){
	var node_bloc = Node(docxml,"bloc",Node(docxml,"titre",Node(docxml,"#CDATA","Document")));
	addAttr(node_bloc,"type","document");
	return node_bloc;
}


function make_uc(listfilesgm){
	// ---------------------------------------------------------------------------
	// Version 2 de la fonction make_uc
	// Ici on part de tous les IDREF externes disponibles depuis la racine
	// Cela permet de couvrir tous les cas sans risque d'en oublier
	// ---------------------------------------------------------------------------

	var ret,doc,dtd,racine,giracine,idracine,idelem,gielem,gielem2,elemList;
	var sUri,uriToc,codeMatiere,libMatiere,typeOuvrage,titreElem,titreNormalise,titreOuv;
	var gendoc,uc,ucsgmlid,donnees,table,blocDocument,blocDocumentUci,blocVousSouhaitez,blocVoirAussi,info,liens,lien,sFdoc,recherche;
	var bBlocDocument = false;
	var bBlocVoirAussi = false;
	var bBlocVousSouhaitez = false;

	var support,matiere,presentation,action,parametre,linkNode;
	var idCible,sFdocCible;

	var setIdInternes = Set();
	var setIdrefExternes = Set();
	var mapIdrefExternes = Map();
	var mapIdrefExternesExemples = Map();

	var basenameSgm = nothing;
	var dtdBasename = nothing;

	var docxml = newCoreDocument();
	var gendocsUc = nothing;
	//var gendocsUci = nothing;
	var gendocsUcx = nothing;
	var papidocUrx = nothing;
	var gendocsUrf = nothing;  // ajout MB 21/11/2012
	// Mantis 9631
	var papidocUax = nothing ; // YE 24/12/2014 

	var setGiFormtx = Set("FORMT1","FORMT2","FORMT3","FORMT4","FORMT5","FORMT6","FORMT7");
	
	var nombre_sgm = listfilesgm.length();
	var modulo = donne_moi_un_modulo(nombre_sgm);
	var compteur_sgm_traites = 0;
	
	// ---------------------------------------------------------------------------
	// On commence par creer une Map de tous les IDREF disponibles
	// On edite/imprime la Map pour avoir une vision claire de tous les IDREF externes classes par racine
	// ---------------------------------------------------------------------------
	for filesgm in eSort(listfilesgm){
		compteur_sgm_traites++;
		// Initialisations
		gendocsUc = nothing;
		gendocsUcx = nothing;
		// Mantis 9631 YE 24/12/2014 
		papidocUax = nothing;
		//gendocsUci = nothing;
		papidocUrx = nothing;
		gendocsUrf = nothing;

		bBlocDocument = false;
		bBlocVoirAussi = false;
		bBlocVousSouhaitez = false;

		basenameSgm = fileBaseName(filesgm);
		//cout << ".";
		if (compteur_sgm_traites % modulo) == 0 printn(format("%1 fichiers traites",compteur_sgm_traites));
		flush(cout);
		if basenameSgm.rsearch(0,G_regexpToc) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}
		
		if basenameSgm.rsearch(0,G_regexpTap) != nothing{
			//cout << format("**** %1\n",filesgm);
			continue;
		}
		
		// Certaines racines n'ont pas d'UC
		if Set("cdcoll.sgm","subscription.sgm","type_source_lpveille.sgm").knows(basenameSgm) continue;
		
		dtdBasename = getDtdBaseNameFromFileName(filesgm);

		if (dtdBasename == nothing){
			cout << format("ERREUR dtdBaseName = %1 pour %2\n",dtdBasename,filesgm);
			set_exitval(2);
			continue;
		}

		dtd = filePath(env("ELA_DTD"),dtdBasename);

		// cout << format("filesgm = %1\n",filesgm);
		// cout << format("dtdBasename = %1\n",dtdBasename);
		// cout << format("dtd = %1\n",dtd);

		// 01/06/2012 AL
		// Certaines DTD n'ont pas d'UC
		// Exemple : les listels.dtd
		if Set("src.dtd").knows(dtdBasename) continue;
		
		// Parsing direct du dossier unicode ./hulk/liv/sgm_entites_nums/
		// Utiliser le parseur ubalise pour parser la version unicode avec entites numeriques unicode &#0123;
		if G_bUnicode{
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		else{
			//cout << format("parse de : %1 avec dtd : %2\n",filesgm,dtd);
			ret = parseDocument(List(env("ELA_DEC"),dtd, filesgm), ThisProcess, Map("keep", true));
		}
		
		if ret.status != 0{
			G_fileLog << format("ERREUR PARSE %1 %2\n",filesgm,dtd);
			continue;
		}

		//cout << format("Ok. traitement %1...\n",filesgm);

		// 08/06/2012 AL
		// Avant de parser le premier texte on charge la Map des version anterieures.
		//if (dtdBasename == "txtbloc-optj.dtd" && G_mapTxtVersion == nothing) make_mapTxtVersion();
		if (dtdBasename == "txtbloc-optj.dtd" && G_mapTxtMDF == nothing) make_mapTxtVersion();

		
		doc = ret.document;
		racine = root(doc);
		giracine = GI(racine);
		// on prend l'id de la racine sans la refonte pour les etudes
		// exemple id=y4001-ref059 => idracine=y4001
		idracine = nothing;
		if hasAttr(racine,"ID") idracine = attr["ID",racine].replace(0,"-REF[0-9]+$","");
		if hasAttr(racine,"KEY") idracine = attr["KEY",racine]; // cas de la racine LISTE
		
		codeMatiere = getCodeMatiere(racine);
		g_codeMatiere = codeMatiere;

		//YE LE 07/05/2014
		// Mantis 2763  
		G_setUri = Set();
		
		// ----------------------------------------------------
		// 11/02/2014 alazreg
		// mantis 5860
		// ajout cas des ucx des CCOL + SYNTHESE ARCHIVE
		// ----------------------------------------------------
		if (codeMatiere == "CD15" && Set("ETD-OPTJ","CCOL-OPTJ").knows(giracine) && hasAttr(racine,"ARCHIVE")){
			if gendocsUcx == nothing gendocsUcx = Node(docxml,"gendocs",false);
			var gendoc_ucx = get_gendoc_ucx_archive_cd15(racine);
			//cout << format("\n\n\nDEBUG je passe dans le test codeMatiere==CD15\n");
			// cout << format("\n\n\nDEBUG gendoc_ucx = %1\n",gendoc_ucx);
			if gendoc_ucx != nothing insertSubTree(gendocsUcx,-1,gendoc_ucx);
			// cout << format("\n\n\nDEBUG gendocsUcx = %1\n",gendocsUcx);
		}

		
		// 25/09/2012 AL
		// DEBUG DES MATIERE RH EN PRIORITE
		// 02/10/2012 AL/MB
		// Pour traiter toutes les matieres commenter le test dessous
		// Sinon il manque plein de fichiers UC UCX URX
		//if ! Set("CD02","CD15","GP20","EN23I","GP25","GP59","GP66","GP67","GP68").knows(codeMatiere) continue;

		typeOuvrage = getTypeOuvrage(codeMatiere,giracine, idracine,basenameSgm);
		// 24/06/2016 alazreg GP95/FREG
		if hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG" typeOuvrage = "FREG";

		// 24/12/2013 MB : pour les stats XITI, on a besoin du titre de l'ouvrage
		titreOuv = getTitreElem(racine,giracine,idracine);
		// 24/12/2013 MB : normaliser le titre de l'ouvrage pour les urf
		titreNormalise = entity2char(titreOuv);
		titreNormalise = char2NoAccent(titreNormalise);
		titreNormalise = trimBoth(titreNormalise," ");
		titreNormalise = titreNormalise.replace(0,G_regexpSpace,"_");
		titreOuv = titreNormalise;
		titreNormalise = "";

		// 16/06/2012 AL
		// Dans la majorite des cas l'ouvrage correspond au document SGML entier (etude, index, liste, tag, recapnew, ...)
		// Dans certains cas l'ouvrage est une partie du document SGML (textes, formules)
		// Par exemple dans le cas des textes, l'ouvrage est le texte et non le bloc de textes
		// Dans le cas des encarts de formules, l'ouvrage est la formule (element formtX et non la racine dpform ou dpform2)
		// Pour realiser une travail homogene et simplifier le code source, je commence par etablir une liste des element a traiter
		// Dans la majorite des cas la liste contiendra la racine du document SGML
		// Dans le cas des bloc de textes, la liste contiendra les elements txt-optj
		// Dans le cas des formules la liste contiendra les elements formt1 formt2 formtX
		
		elemList = List(racine);
		if giracine == "TXTBLOC-OPTJ" elemList = searchElemNodes(racine,"TXT-OPTJ");
		if giracine == "DECISBLOC-OPTJ" elemList = searchElemNodes(racine,"DECIS-OPTJ");

		// ------------------------------------------------
		// 16/01/2014 alazreg/yendichi
		// mantis 5802
		// ajout du cas FPBLOC pour les GPxx
		// ------------------------------------------------
		if giracine == "FP-OPTJ" elemList = searchElemNodes(racine,"FP-OPTJ");

		// 11/10/2012 AL
		// Pour les formules laisser la racine pour creer une uc@type="ouvrage"
		if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) elemList = List(racine)+searchElemNodes(racine,eSort(setGiFormtx));

		//cout << format("FORMULAIRES : elemList = %1 \n",elemList);
		

		// 10/10/2012 AL
		// Gestion des fiches gp20
		if giracine == "QR-OPTJ" {
			if codeMatiere == "GP20" elemList = List(racine)+searchElemNodes(racine,"GP20");
			// 23/12/2013 MB : on reactive le niveau document pour les URFs des QR hors gp20 (pour les UCs, a voir avec MG/AB)
			else 	elemList = searchElemNodes(racine,giracine);
		}

		// 23/12/2013 MB : on reactive le niveau document pour les URFs des FP/FPRO (pour les UCs, a voir avec MG/AB)
		if Set("FP-OPTJ","FPRO-OPTJ").knows(giracine) elemList = searchElemNodes(racine,giracine);
		
		// 23/12/2013 MB : en activant le niveau document, il faut apparemment nettoyer l'ID de la racine des fpbloc, ... (-ROOT)
		// 27/01/2014 YE : Mantis 5844 il faudrait ajouter une exception pour ne pas supprimer "-" dans les Id des QR du GP20
		//idracine = idracine.replace(0,"-ROOT","").replace(0,"-","");
		if giracine == "QR-OPTJ" && codeMatiere == "GP20" idracine = idracine.replace(0,"-ROOT","");
		else idracine = idracine.replace(0,"-ROOT","").replace(0,"-","");

		// ----------------------------------------
		// 21/07/2014 alazreg
		// mantis 7241 fomule dalloz
		// dans le cas des formules dalloz on ne modifie pas l'attribut ID
		// ----------------------------------------
		var editeur = get_editeur(racine);
		if giracine == "DPFORM2-OPTJ" && editeur == "DZ"{
			idracine = attr["ID",racine];
			cout << format("debug cas dalloz idracine = %1 %2\n",idracine,echo(racine));
		}
		
		// 11/07/2012 MB : sauf si le fichier est vide (voir + loin)!
		// Creer un fichier doc.uc.xml dans tous les cas
		// avec une racine gendocs
		
		gendocsUc = Node(docxml,"gendocs",false);

		// Pour les formules interactives form2
		// 05/12/2012 al
		// Les uci sont maintenant genere en même temps que les mfi
		// Voir fonction make_mfi_new()
		//gendocsUci = Node(docxml,"gendocs",false);

		// 21/11/2012 MB : ajout pour urf
		gendocsUrf = Node(docxml,"gendocs",false);

		libMatiere = get_lib_matiere(racine);

		var filenametoc = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".toc.sgm");
		//cout << format("filenametoc = '%1'\n",filenametoc);

		// pour les formulaires : generer un seul ouvrage  : (mail YT : 04/07/2012 : 12h47)
		// pas de sgml-id pour le moment (moins urgent !!)
		// reprendre la boucle apres
		// enlever cette partie pour traiter toutes les formules

		for elem in elemList{
			//cout << format("============   ============> elem=%1\n",echo(elem));

			gielem = GI(elem);
			idelem = nothing;
			
			// 08/02/2016 SF ajout pack ope
			// ne pas ajouter gendoc sur la racine
			if gielem == "DPFORM2-OPTJ" && hasAttr(racine,"packope") && attr["packope",racine].transcript(UpperCase) == "YES" && hasAttr(racine,"packid") {
				continue;
			}
			
			// ------------------------------------------------------------------
			// 16/01/2014 alazreg/yendichi
			// pour les etudes on ne prend pas la partie "-REFxxx" de l'id racine
			// exemple : Z4001-REF26 devient idelem = Z4001
			// ------------------------------------------------------------------
			if hasAttr(elem,"ID") idelem = attr["ID",elem].replace(0,"-REF[0-9]+$","");
			if hasAttr(elem,"KEY") idelem = attr["KEY",elem];

			// 23/12/2013 MB : nettoyer partie "-ROOT
			// ----------------------------------------
			// 16/01/2014 alazreg/yendichi
			// mantis 5802
			// remplacer le tiret + "-ROOT" uniquement pour la racine FP-OPTJ
			// ----------------------------------------
			if (elem == racine) idelem = idelem.replace(0,"-ROOT","").replace(0,"-","");
			if hasAttr(racine,"VERSION") && attr["VERSION",racine] == "DZ"{
				idelem = attr["ID",elem];
				// cout << format("debug cas dalloz idelem = %1 %2\n",idelem,echo(elem));
			}

			// YE 07/03/2014 Mantis 6408 
			if (elem == racine) idelem = idelem.replace(0,"HZ","-HZ").replace(0,"--","-");

			
			// 12/07/2012 ajout MB : idelem n'est pas bon pour les fp/qr/fpro/ : T7FPBLOC-1-ROOT au lieu de T7FPBLOC1
			// ex. de resultat : <gendoc uri="EL/GP67/FP/T7FPBLOC-1-ROOT/#/UC"><uc type="ouvrage" uri="EL/GP67/FP/T7FPBLOC-1-ROOT/#/UC">
			// on utilise une fonction getOuvrageId dans le cas des fp/qr/fpro/ (à generaliser)
			// Ne pas appeler cette fonction dans le cas du gp20 : car X0Q001-HZ, X0Q002-HZ sont corrects dans les id
			// Ne pas appeler cette fonction dans les cas du gp23/gp25 : pas de blocs fp/qr/fpro
			// 06/11/2012 MB : ajout du gp25 (car nouveau format bloc adopte)
			
			// 30/01/2013 MB : ajout GP74 
			// 21/08/2013 AL : ajout GP23 qui a evolue vers des fpbloc + qrbloc 
			// 23/12/2013 MB : desormais, on traite les documents dans les URFs/UCs, on active le bon idelem
			// if (Set("FP-OPTJ","QR-OPTJ","FPRO-OPTJ").knows(giracine) && Set("EN23I","GP23","GP25","GP59","GP66","GP67","GP68","GP69","GP74").knows(codeMatiere)){
				// idelem = getOuvrageId(codeMatiere,giracine, idracine,basenameSgm);
		//cout << format("\t-->idelem corrige = %1\n",idelem);
			// }
			// --------------- fin ajout MB

			//cout << format("777777 apres 77777777 idelem = %1  idracine = %2\n",idelem,idracine);

			sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,idelem);
			// 02/02/2016 SF ajout packope
			if hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES" && hasAttr(racine,"PACKID")
				sUri = format("EL/PACKOPE/%1/%2/%3/#/UC",attr["PACKID",racine],typeOuvrage,idracine);
			
			// 05/03/2014 YE Mantis 6404
			// exemple : EL/GP68/DECRYPTAGES/T8FPROBLOC/#/UC/T8FPRO546
			if gielem == "FPRO-OPTJ" {
				if elem == racine sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,idracine);
				else sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
				// 20/06/2016 alazreg evolution pour gp95/preventeur
				if elem == racine && hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG" sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,"FREG",idracine);
			}
			if gielem == "FP-OPTJ" {		
				// 08/01/2016 SF ajout packope
				if hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES" && hasAttr(racine,"PACKID") {
					if elem == racine sUri = format("EL/PACKOPE/%1/%2/%3/#/UC",attr["PACKID",racine],typeOuvrage,idracine);
					else sUri = format("EL/PACKOPE/%1/%2/%3/#/UC/%4",attr["PACKID",racine],typeOuvrage,idracine,idelem);	
				}
				else {
				if elem == racine sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,idracine);
				else sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);			
			}
			}
			if gielem == "QR-OPTJ"{
				if elem == racine sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,idracine);
				else sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);				
			}
			
			// 04/07/2012 MB type ouvrage ==> pas d'element apres #/UC  (verifie avec AB)
			//if gielem == "TXT-OPTJ" sUri = format("EL/%1/%2/%3/%4/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
			if gielem == "TXT-OPTJ" sUri = format("EL/%1/%2/%3/%4/#/UC",codeMatiere,typeOuvrage,idracine,idelem);
			if gielem == "DECIS-OPTJ" sUri = format("EL/%1/%2/%3/%4/#/UC",codeMatiere,typeOuvrage,idracine,idelem);
			//if gielem == "COMJRP-OPTJ" sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,"Y5COMJRP");
			if gielem == "RECMAJ-OPTJ" sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,"RECMAJ");
			
			// // 04/07/2012 MB formulaire ==> solution d'urgence : un ouvrage pour les formulaires 
			// 
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) sUri = format("EL/%1/%2/%3/#/UC",codeMatiere,typeOuvrage,idracine);
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
			if setGiFormtx.knows(gielem) {
				// 02/02/2016 SF ajout packope
				if hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES" && hasAttr(racine,"PACKID") {
					if elem == racine sUri = format("EL/PACKOPE/%1/%2/%3/#/UC",attr["PACKID",racine],typeOuvrage,idracine);
					else sUri = format("EL/PACKOPE/%1/%2/%3/#/UC/%4",attr["PACKID",racine],typeOuvrage,idracine,idelem);	
				}
				else
					sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
			}

			if giracine=="QR-OPTJ" && gielem=="GP20" sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
			
			
			//on ne met que l'uc de type ouvrage y compris pour les form pour le moment (mail YT : 04/07/2012 : 12h47)
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) sUri = format("EL/%1/%2/%3/#/UC/%4",codeMatiere,typeOuvrage,idracine,idelem);
			
			gendoc = Node(docxml,"gendoc",false);
			addAttr(gendoc,"uri",sUri);
			insertSubTree(gendocsUc,-1,gendoc);

			uc = Node(docxml,"uc",false);
			// YE LE 05/03/2014 mantis 6404 + mail de gonçalves du 20/02/2014
			// addAttr(uc,"type","ouvrage");
			// Pour la racine des bloc le type est "ouvrage"
			// if (gielem == "FPRO-OPTJ" || gielem == "FP-OPTJ" || gielem == "QR-OPTJ") && (elem == racine) addAttr(uc,"type","ouvrage");
			if elem == racine addAttr(uc,"type","ouvrage");
			else addAttr(uc,"type","document");
			// YE LE 11/03/2014 mantis 6404 + mail de gonçalves du 20/02/2014
			if giracine=="TXTBLOC-OPTJ" || giracine=="DECISBLOC-OPTJ" addAttr(uc,"type","ouvrage");

			// 09/10/2012 AL
			// 02/05/2013 MB : pour le mode flow, remplacer sgml-id par "document-id" : e-mail AB : 25/01/2013 17:18
			// YE 11/03/2014 Mantis 6404 L utilisation d UC de type "document" peut maintenant etre utilise en place du type "sgml-id"
			// if setGiFormtx.knows(gielem) addAttr(uc,"type","sgml-id");
			if setGiFormtx.knows(gielem) addAttr(uc,"type","document");
			// 06/05/2013 MB : apparemment c'est "document" tout court qu'il faut mettre (e-mail AB : 06/05/2013 11:47)
			// 22/05/2013 MB : on revient momentanement en arriere suite pb applicatif jouve (e-mail MG : 22/05/2013 12:03)
			// if setGiFormtx.knows(gielem) addAttr(uc,"type","document");

			// --------------------------------------------------------
			// 08/11/2012 al
			// Dans le cas des elements QR-OPTJ/GP20 indiquer sgml-id
			// --------------------------------------------------------
			// 23/12/2013 alazreg
			// mantis 4408
			// ajout alerte redaction
			// mail armand betton => indiquer uc@type="document" pour faire focntionner alerte redaction
			// --------------------------------------------------------
			//if giracine == "QR-OPTJ" && gielem == "GP20" addAttr(uc,"type","sgml-id");
			if giracine == "QR-OPTJ" && gielem == "GP20" addAttr(uc,"type","document");

			// 02/07/2012 MB
			// suite retours jouve : "Il faut generer une UC de type "ouvrage" plutôt que "sgml-id" pour les textes : 
			// pour les formules : generer une uc ouvrage + une uc sgml-id
			// pour l'uc sgml-id ==> enlever tout (ce qui est accepte par la dtd) et laisser que les blocs voir aussi
			// pas prioritaire ==> à voir + tard (mail YT : 04/07/2012 : 12h47)
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) addAttr(uc,"type","sgml-id");
			
			// Avec la remarque de Yann intervenue entre-temps, il y aura à la fois des UC "ouvrage" 
			// (pour referencer titre et TDM) et des UC "sgml-id" (pour les liens "personnaliser")
			//if Set("TXTBLOC-OPTJ","DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) addAttr(uc,"type","sgml-id");


			addAttr(uc,"uri",sUri);
			insertSubTree(gendoc,-1,uc);

			//cout << format("Je traite element %1\n",echo(elem));

			// ------------------------------------------------------------------
			// Inserer le titre au dessus du document
			// ------------------------------------------------------------------
			titreElem = getTitreElem(elem,gielem,idelem);
			
			//YE Le 27/08/2014 Mantis 7916- Dans le Cas GP69 fpro le titres est D&#233;cryptages
			if codeMatiere == "GP69" && (idracine.rsearch(0,"LSTFPRO")!= nothing || idracine.rsearch(0,"T9FPRO")!= nothing ) titreElem = "D&#233;cryptages";
			
			// YE 26/06/2014 Mantis 7230
			// Pour les formules on prend le titre de l'encart racine
			if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) titreElem = getTitreElem(racine,giracine,idracine);
			
			// 03/02/2016 SF ajout pack ope			
			if hasAttr(racine,"packope") && attr["packope",racine].transcript(UpperCase) == "YES" && hasAttr(racine,"packid") {						
				// cout << format("debugsf elem = %1\n", GI(elem));
				if enclosed(elem,"tif1") != nothing titreElem = content(enclosed(elem,"tif1"));
				if enclosed(elem,"tif2") != nothing titreElem = content(enclosed(elem,"tif2"));
				if enclosed(elem,"tif3") != nothing titreElem = content(enclosed(elem,"tif3"));
				if enclosed(elem,"tif4") != nothing titreElem = content(enclosed(elem,"TIF4"));
				if enclosed(elem,"tif5") != nothing titreElem = content(enclosed(elem,"tif5"));
				// cout << format("debugsf titreElem = %1\n", titreElem);
			}
			
			
			// 21/11/2012 MB : normaliser el titre pour les urf
			titreNormalise = entity2char(titreElem);
			titreNormalise = char2NoAccent(titreNormalise);
			titreNormalise = trimBoth(titreNormalise," ");
			titreNormalise = titreNormalise.replace(0,G_regexpSpace,"_");

			// YE 26/06/2014 Mantis 7230
			// Pour les formules on prend le titre de l'encart racine
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) titreElem = getTitreElem(racine,giracine,idracine);

			// 06/11/2012 MB : transcoder le titre (ex. : fiche mantis 1484: Texte pb entite dans UC)
			titreElem = entityHtml2EntityNum(titreElem);

			// 11/10/2012 alazreg
			// le bloc <donnees> ne doit pas etre pose sur les uc des formules sgml-id
			// 08/11/2012 al
			// Idem pour les elements qr-optj/gp20 qui ne doivent pas avoir de bloc <donnees>

			//insertSubTree(uc,-1,creerBlocDonnees(docxml,codeMatiere,typeOuvrage,idracine,giracine,idelem,gielem,titreElem,filenametoc));
			
			// 03/07/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-241
			// insérer ici un élément <metadata>
			insertSubTree(uc,-1,getNodeMetadata(docxml,codeMatiere,titreElem));
			
			// 08/02/2016 SF ajout pack ope
			if setGiFormtx.knows(gielem) && hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES" {
				insertSubTree(uc,-1,creerBlocDonnees(docxml,codeMatiere,typeOuvrage,racine,idracine,giracine,idelem,gielem,titreElem,filenametoc));
			}
			else if setGiFormtx.knows(gielem) || (giracine=="QR-OPTJ" && gielem=="GP20"){
				;//ne rien faire
			}		// Mantis 6404 et 6044
			else if Set("FPRO-OPTJ","FP-OPTJ","QR-OPTJ").knows(gielem) && idelem != idracine {
				;
			}		
			else 
				insertSubTree(uc,-1,creerBlocDonnees(docxml,codeMatiere,typeOuvrage,racine,idracine,giracine,idelem,gielem,titreElem,filenametoc));
			

			// 02/08/2016 SF/MB : on deplace ici l'ajout du bloc presentation
			// pour le cycle 38, on ajoute le bloc Presentation
			// pour la couleur de la matiere dans la TOC
			if (hasAttr(uc,"type") && attr["type",uc] == "ouvrage") {
			  // cout << format("Appel creerBlocPresentation\n");
			  var presentation = creerBlocPresentation(docxml,giracine,idracine,codeMatiere);		
			  if (presentation != nothing) insertSubTree(uc,-1,presentation);				  
			}

			// -------------------------------------------------------------
			// 11/03/2013 alazreg
			// insertion du nouveau bloc uc/recherche entre uc/donnees et uc/bloc@type=document
			// -------------------------------------------------------------
			
			// ne pas creer le bloc de recherhce sur certaines racines
			if !Set("LISTE","IDX-OPTJ","IDXET-OPTJ","RECMAJ-OPTJ","TAG-OPTJ").knows(giracine){
				// ne pas creer le bloc recherche sur les uc@type=sgml-id
				
				
				// ne pas creer le bloc recherche sur les uc@type=sgml-id
				//YE 6404 11/03/2014 
				// ne pas ceer de bloc recherche sur les uc avec le type "ouvrage"			
				// if (hasAttr(uc,"type") && attr["type",uc] != "sgml-id"){
				// 08/02/2016 SF ajout pack ope
				if (hasAttr(uc,"type") && attr["type",uc] == "ouvrage") || (setGiFormtx.knows(gielem) && hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES") { 		
					var recherche = creerBlocRecherche(docxml,giracine,idracine);		
					if (recherche != nothing) insertSubTree(uc,-1,recherche);
				}
			}

			sFdoc = getFdocPath(codeMatiere,gielem,idelem);
			if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine) sFdoc = getFdocPath(codeMatiere,giracine,idracine);
			
			// 01/08/2016 SF ajout du typeouvrage FREG pour Preventeur (mantis 13958)
			if (codeMatiere == "GP95" && hasAttr(racine,"TYPEDOC") && attr["TYPEDOC",racine] == "FREG") sFdoc = "EL/GP95/FREG";
			
			// ------------------------------------------------------------------
			// Zone Document a droite
			// ------------------------------------------------------------------
			// 08/11/2012 al
			// Ne pas inserer de bloc <document> à droite pour les elements qr-optj/gp20
			// ------------------------------------------------------------------
			// 23/12/2013 alazreg
			// pour declencher l'alerte redaction sur les fiches du gp20 il faut inserer un element uc/bloc@type="document"
			// je commente donc le test dessous if (giracine=="QR-OPTJ" && gielem=="GP20")
			// ------------------------------------------------------------------
			//if setGiFormtx.knows(gielem) || (giracine=="QR-OPTJ" && gielem=="GP20")
			var bcreerBlocDocument = true;
			
			//ne rien faire 
			if setGiFormtx.knows(gielem) bcreerBlocDocument = false;
			
			// 08/02/2016 SF ajout pack ope			
			if setGiFormtx.knows(gielem) && setGiFormtx.knows(gielem) && hasAttr(racine,"PACKOPE") && attr["PACKOPE",racine] == "YES" 
				bcreerBlocDocument = true;
			
			// Mantis 6404
			if Set("FPRO-OPTJ","FP-OPTJ","QR-OPTJ").knows(gielem) && idelem != idracine bcreerBlocDocument = false;			
			if gielem == "GP20"  bcreerBlocDocument = false;
			// cout << format("idelem '%1', idracine '%2' bcreerBlocDocument '%3' \n",idelem,idracine,bcreerBlocDocument);
			if bcreerBlocDocument insertSubTree(uc,-1,creerBlocDocument(docxml,codeMatiere,racine,idracine,giracine,elem,idelem,titreElem,filenametoc,libMatiere,sFdoc));

			// 07/01/2015 alazreg
			// mantis 4408 : alerte redaction sur fiches gp20
			// on cree un niveau bloc@type="document"/lien@type="AlerteRedaction" pour les elements QR-OPTJ//GP20
			if gielem == "GP20"{
				var bloc_document = get_node_bloc_document(docxml);
				// var lien_alerte_redaction = get_node_lien_alerte_redaction(docxml,idelem);
				var lien_alerte_redaction = getLienAlerteRedaction(docxml,idelem);
				if lien_alerte_redaction != nothing{
					insertSubTree(bloc_document,-1,lien_alerte_redaction);
				}
				insertSubTree(uc,-1,bloc_document);
			}
			
			// ------------------------------------------------------------------
			// Zone vous pouvez
			// 22/06/2012
			// La zone vous pouvez est desactivee pour le moment - vu avec Aude DEFRETIERE
			// ------------------------------------------------------------------
			//var blocVouPouvez = creerBlocVousPouvez(docxml,elem,idracine,giracine,idelem,titreElem,filenametoc,libMatiere,sFdoc);
			//if blocVouPouvez != nothing insertSubTree(uc,-1,blocVouPouvez);

			// ------------------------------------------------------------------
			// Zone vous souhaitez
			// ------------------------------------------------------------------

			// Si le bloc "Vous souhaitez" contient une information alors l'inserer dans l'UC
			var blocVousSouhaitez = creerBlocVousSouhaitez(docxml,codeMatiere,elem,idracine,giracine,idelem,titreElem,filenametoc,libMatiere,sFdoc);
			if blocVousSouhaitez != nothing insertSubTree(uc,-1,blocVousSouhaitez);

			// ------------------------------------------------------------------
			// Zone voir aussi a droite
			// ------------------------------------------------------------------

			// On constitue un Set de tous les ID internes
			setIdInternes = Set();
			for elem2 in searchElemNodes(elem,nothing,"ID",nothing) setIdInternes << attr["ID",elem2];
			
			// On constitue un Set des IDREF externes
			// Cela permettra de creer les UC UCX URX
			setIdrefExternes = Set();
			//cout << format("Je suis ici avant for elem2 in searchElemNodes(elem,nothing,IDREF,nothing)\n");
			for elem2 in searchElemNodes(elem,nothing,"IDREF",nothing){
				gielem2 = GI(elem2);
				// Pour les XACODE traiter seulement les <XACODE ARTDIR="OUI">
				if gielem2 == "XACODE" && not hasAttr(elem2,"ARTDIR") continue;

				//if !setIdInternes.knows(attr["IDREF",elem]) << setIdrefExternes << attr["IDREF",elem];
				// Si IDREF est absent du Set des ID internes nous sommes face a un IDREF externe
				// Il faut donc le traiter :
				// 1. soit comme un lien remontant => UCX + URX
				// 2. soit comme un lien de la zone voir aussi de l'UC
				//    Voir les autres etudes
				//    Voir les autres formules
				//    Voir les autres fiches pratiques
				//    etc.
				if !setIdInternes.knows(attr["IDREF",elem2]){
					//if !mapIdrefExternes.knows(elem) mapIdrefExternes[elem] = Set();
					//mapIdrefExternes[elem] << gielem2;
					//mapIdrefExternesExemples[gielem+gielem2] = format("%1 %2",idelem,echo(elem));

					// Liens remontants TXT JRP ou CODE
					// Je traite  seulement les etudes pour le moment
					// Il faudra faire evoluer pour les FP et autre document qui ont des liens remontants
					// Creer les UCX + URX pour les liens remontant
					//cout << format("Je suis ici if !setIdInternes.knows\n");

					// ---------------------------------------------------------------
					// 17/08/2012 AL
					// Ne pas creer de UCX URX pour les textes anterieurs
					// ---------------------------------------------------------------
					// 04/07/2013 alazreg
					// ne pas creer de ucx urx sur les textes courants
					// => transformation de la regexp dessous ELNETTXTANTBLOC en ELNETTXT(ANT)?BLOC
					// ---------------------------------------------------------------
					if Set("JRP","TJR","XACODE").knows(gielem2) && idracine.rsearch(0,"^ELNETTXT(ANT)?BLOC") == nothing{
						// Creer une racine gendocs pour UCX
						if gendocsUcx == nothing gendocsUcx = Node(docxml,"gendocs",false);
						if papidocUrx == nothing papidocUrx = Node(docxml,"PAPI_document_list",false);

						// ---------------------------------------------------------------
						// 20/07/2012 AL
						// Gestion des CODES SECS Dalloz
						// Je change l'appel de la fonction make_ucx qui retourne maintenant une List de gendoc.
						//insertSubTree(gendocsUcx,-1,make_ucx(docxml,elem2,gielem2,giracine,codeMatiere,typeOuvrage,idelem,titreElem,sFdoc));
						// ---------------------------------------------------------------
						// 16/01/2014 alazreg/yendichi
						// mantis 5802
						// ne pas creer de ucx pour la racine FP-OPTJ
						// ajout du test if gielem == "FP-OPTJ" && elem != racine
						// ---------------------------------------------------------------
						// for gendoc in make_ucx(docxml,elem2,gielem2,idracine,giracine,codeMatiere,typeOuvrage,idelem,titreElem,sFdoc){
						// insertSubTree(gendocsUcx,-1,gendoc);
						// }
						// 11/02/2014 alazreg/yendichi
						// correction de bug sur test suite intervention du 16/01/2014
						// les ucx etaient generees uniquement pour les fpbloc
						// generation des ucx pour toutes les racines SAUF la racine FP-OPTJ
						// ---------------------------------------------------------------
						var b_make_ucx = true;
						// 06/03/2015 SF mantis 9922-9923
						// pas de fpbloc pour les g76 et gp95 ==> activer la generation des ucx pour les FP du gp76 et gp95
						// if gielem == "FP-OPTJ" && elem == racine  b_make_ucx = false;
						if gielem == "FP-OPTJ" && elem == racine && !Set("GP76","GP95").knows(codeMatiere) b_make_ucx = false;
						
						// 18/05/2016 ajout pack ope mantis 13343
						if (gielem == "FP-OPTJ" && hasAttr(racine, "PACKOPE") && hasAttr(racine, "PACKID") && attr["PACKOPE", racine] == "YES") {
							b_make_ucx = true;
							sFdoc = format("EL/PACKOPE/%1/FP", attr["PACKID", racine]);
						}

						// 26/08/2015 alazreg mantis 11229 ne pas creer de UCX pour les racine FPBLOC QRBLOC
						if idelem.rsearch(0,"FPBLOC|QRBLOC") != nothing b_make_ucx = false;
						// 04/04/2016 alazreg mantis 12590 na pas generer de UCX vers les JRP TXT pour les archives de syntheses du CD15
						if giracine == "ETD-OPTJ" && hasAttr(racine,"ARCHIVE") b_make_ucx = false;

						if b_make_ucx{
							for gendoc in make_ucx(racine,docxml,elem2,gielem2,idracine,giracine,codeMatiere,typeOuvrage,idelem,titreElem,sFdoc){
								insertSubTree(gendocsUcx,-1,gendoc);
							}
						}

						// Je change l'appel de la fonction make_urx qui retourne maintenant une List de papidoc.
						//insertSubTree(papidocUrx,-1,make_urx(docxml,elem2,gielem2,giracine,codeMatiere,typeOuvrage,idelem,titreElem,sFdoc));
						for papidoc in make_urx(docxml,elem2,gielem2,giracine,codeMatiere,typeOuvrage,idelem,titreElem,sFdoc){
							insertSubTree(papidocUrx,-1,papidoc);
						}
					}
				}
			}

			// Inserer le bloc "Voir aussi" seulement s'il a ete complete
			var blocVoirAussi = creerBlocVoirAussi(docxml,elem,racine,idracine,giracine,idelem,titreElem,filenametoc,codeMatiere,libMatiere,sFdoc);

			// pas de bloc "voir aussi" pour les formulaires (ouvrage)
			// je desactive artificiellement dans le cas des form :
			//if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(giracine)

			// 11/10/2012 AL
			// Pour les formules le bloc voir aussi doit etre cree seulement sur les uc des formules type=sgml-id
			// et pas sur l'uc de la racine type=ouvrage
			//if Set("DPFORM-OPTJ").knows(giracine) blocVoirAussi = nothing;
			if Set("DPFORM-OPTJ","DPFORM2-OPTJ").knows(gielem) blocVoirAussi = nothing;
			if  blocVoirAussi != nothing insertSubTree(uc,-1,blocVoirAussi);


			// -----------------------------------------------------------
			// 11/07/2013 alazreg
			// mantis 3999
			// Pour le formulaire ECLAIR - ecrire a la redaction
			// ajouter les elements uc/donnees/matiere@code=CDXX + uc/eclair@class=tcontactredac
			// Attention la feuille de style HTMLP doit contenir la meme class que l'element eclair => "tcontactredac"
			// -----------------------------------------------------------
			// <eclair>
			// <css-style>
				// <![CDATA[.contactredac-table {display : block}]]>
			// </css-style>
			// </eclair>

			if (gielem == "ETD-OPTJ"){
				//var eclair = Node(docxml,"eclair",false);
				//addAttr(eclair,"class","tcontactredac");

				
				// ------------------------------------------------------------------
				// 14/11/2013 alazreg
				// ajouter le prefixe #ElApp pour eclair
				// ------------------------------------------------------------------

				//		var eclair = Node(docxml,"eclair",Node(docxml,"css-style",Node(docxml,"#CDATA","<![CDATA[.contactredac-table {display : block}]]>")));
				var eclair = Node(docxml,"eclair",Node(docxml,"css-style",Node(docxml,"#CDATA","<![CDATA[#ElApp.contactredac-table {display : block}]]>")));


				insertSubTree(uc,-1,eclair);
			}

			// ------------------------------------------------------------------
			// Generation URF 
			// ------------------------------------------------------------------
			// 21/11/2012 MB : creation des urf
			// on fait appel a une fct : make_urf()
			// dans un 1er temps ==> on se limite aux etudes
			// les cas a traiter : ADLIST-OPTJ ; CCOL-OPTJ ; CDCOLL ; CODE-OPTJ ; COMJRP-OPTJ ; CORNAFCC-OPTJ ; DECISBLOC-OPTJ ; DOCAM-OPTJ ; 
			// DPFORM-OPTJ ; DPFORM2-OPTJ ; ETD ; ETD-OPTJ ; FP-OPTJ ; FPRO-OPTJ ; IDX-OPTJ ; IDXET-OPTJ ; LISTE ; QR-OPTJ ; RECMAJ-OPTJ ; 
			// REFDOCINFOS ; SRC ; TABLESOURCES ; TAG-OPTJ ;  TXTBLOC-OPTJ ; 

			//cout << format("\n////////IDRACINE = %1 IDELEM = %2 GIRACINE = %3 RANK == %4 /////////\n",idracine,idelem,giracine,rank(elem));
			if codeMatiere != "LP35" && (Set("CCOL-OPTJ","CORNAFCC-OPTJ","RECMAJ-OPTJ","DOCAM-OPTJ","LISTE","TAG-OPTJ","ETD-OPTJ","FP-OPTJ","QR-OPTJ","FPRO-OPTJ","DPFORM2-OPTJ","DPFORM-OPTJ","IDX-OPTJ","IDXET-OPTJ","TXTBLOC-OPTJ","DECISBLOC-OPTJ","COMJRP-OPTJ","CODE-OPTJ").knows(giracine)) {

				//cout << format("\n////  ////Avant test pour appel make_urf pour : %1 sFdoc/idracine = %2/%3\n",giracine,sFdoc,idracine);

				//13/12/2012 al
				// a decommenter le 14/12/2012
				//  if (Set("ETD-OPTJ").knows(giracine))
				// 21/12/2012 al
				// Les urf sont à faire seulement sur les racines
				// pour les textes/jrp ==> traiter le noeud de rank 0
				// 30/01/2013 MB : les urf des fp/qr/fpro ne se generent pas a cause de la comparaison idracine == idelem ==> on ajoute la condition 2

				// 23/12/2013 MB : pour les URFs, on detaille le niveau document pour les forms, fpro, fp, qr (mail MG : 11/12/2013 18h43
				//-------------------------------------------------------------------------------------------
				/*if ((idracine == idelem) || (Set("FPRO-OPTJ","FP-OPTJ","QR-OPTJ").knows(giracine) && idracine.replace(0,"-ROOT","").replace(0,"-","") == idelem) || (Set("TXTBLOC-OPTJ","DECISBLOC-OPTJ","CODE-OPTJ").knows(giracine))) {*/
				if ((idracine == idelem)|| (Set("DPFORM-OPTJ","DPFORM2-OPTJ","FPRO-OPTJ","FP-OPTJ","QR-OPTJ","TXTBLOC-OPTJ","DECISBLOC-OPTJ","CODE-OPTJ").knows(giracine))) {

					// Set("IDX-OPTJ","IDXET-OPTJ","TXTBLOC-OPTJ","DECISBLOC-OPTJ","COMJRP-OPTJ").knows(giracine) {
					//cout << format("\n\t////////make_urf(typeOuvrage=%1,giracine=%2,idracine=%3,idelem=%4,titreNormalise=%5,libMatiere=%6,sFdoc=%7,sUri=%8,titreOuv=%9)\n\n",typeOuvrage,giracine,idracine,idelem,titreNormalise,libMatiere,sFdoc,sUri,titreOuv);
					
					// 25/02/2016 SF modif pack ope
					// var gendoc = make_urf(docxml,codeMatiere,elem,typeOuvrage,giracine,idracine,idelem,titreNormalise,libMatiere,sFdoc,basenameSgm,sUri,titreOuv);
					var gendoc = make_urf(racine,docxml,codeMatiere,elem,typeOuvrage,giracine,idracine,idelem,titreNormalise,libMatiere,sFdoc,basenameSgm,sUri,titreOuv);
					if gendoc != nothing insertSubTree(gendocsUrf,-1,gendoc);
				}
			}
		}//Fin de la boucle sur "elemlist" les elements
		
		//cout << format("\n/////////////////gendocsUc = %1\n",getAllSGML(gendocsUc));
		if gendocsUc != nothing {
			//var filenamexml = filesgm+".uc.xml";
			// 11/07/2012 MB : creer l'UC uniquement si le fichier n'est pas vide !
			// 
			var filenamexml = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".uc.xml");
			if searchElemNodes(gendocsUc,"gendoc").length() > 0 {
				var filexml = FileStream(filenamexml,"w");
				filexml << G_enteteXml+"\n";
				dumpSubTree(gendocsUc,filexml,Map("FORMAT","XML"));		
				close(filexml);

				// 20/07/2012 AL
				// Le dump genere les entites "&amp; &lt; &gt;" au lieu des caracteres "& < >"
				// Corriger ces caracteres apres le dump pour ne pas les oublier
				corrigerCaracteres(filenamexml);
			}
		}
		
		if gendocsUcx != nothing{
			//var filenamexml = filesgm+".uc.xml";
			var filenamexml = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".ucx.xml");
			// 11/07/2012 MB : creer l'Urx uniquement si le fichier n'est pas vide !
			//
			if searchElemNodes(gendocsUcx,"gendoc").length() > 0 {
				var filexml = FileStream(filenamexml,"w");
				filexml << G_enteteXml+"\n";
				dumpSubTree(gendocsUcx,filexml,Map("FORMAT","XML"));		
				close(filexml);
				// 20/07/2012 AL
				// Le dump genere les entites "&amp; &lt; &gt;" au lieu des caracteres "& < >"
				// Corriger ces caracteres apres le dump pour ne pas les oublier
				corrigerCaracteres(filenamexml);
			}
		}
		
		if papidocUrx != nothing{
			//var filenamexml = filesgm+".uc.xml";
			
			var filenamexml = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".urx.xml");
			//if searchElemNodes(papidocUrx,"gendoc").length() > 0 {
			var filexml = FileStream(filenamexml,"w");
			filexml << G_enteteXml+"\n";
			dumpSubTree(papidocUrx,filexml,Map("FORMAT","XML"));		
			close(filexml);
			// 20/07/2012 AL
			// Le dump genere les entites "&amp; &lt; &gt;" au lieu des caracteres "& < >"
			// Corriger ces caracteres apres le dump pour ne pas les oublier
			corrigerCaracteres(filenamexml);
		}

		
		// 05/12/2012 al
		// La generation des uci est deportee dans la fonction make_mfi_new()
		// C'est plus coherent que les UCI soient generees en même temps que les mfi
		// if gendocsUci != nothing{
		// 11/07/2012 MB : creer l'UC uniquement si le fichier n'est pas vide 
		// 
		// var filenamexml = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".uci.xml");
		// if searchElemNodes(gendocsUci,"gendoc").length() > 0 {
		// var filexml = FileStream(filenamexml,"w");
		// filexml << G_enteteXml+"\n";
		// dumpSubTree(gendocsUci,filexml,Map("FORMAT","XML"));
		// close(filexml);
		// 20/07/2012 AL
		// Le dump genere les entites "&amp; &lt; &gt;" au lieu des caracteres "& < >"
		// Corriger ces caracteres apres le dump pour ne pas les oublier
		// corrigerCaracteres(filenamexml);
		// }
		//else cout << format("pas de fichier UCI %1 car UCI vide !\n",filenamexml);
		// }
		


		// 21/11/2012 MB : ajout urf
		if gendocsUrf != nothing{
			var filenamexml = filePath(fileDirName(filesgm),basenameSgm.explode(".")[0]+".urf.xml");
			if searchElemNodes(gendocsUrf,"gendoc").length() > 0 {
				var filexml = FileStream(filenamexml,"w");
				filexml << G_enteteXml+"\n";
				dumpSubTree(gendocsUrf,filexml,Map("FORMAT","XML"));
				close(filexml);
				// cout << format("fichier URF %1 cree.\n",filenamexml);
				corrigerCaracteres(filenamexml);
			}
			else cout << format("pas de fichier URF %1 car vide !\n",filenamexml);
		}

		
		close(doc);
	}

	// afficher ici la derniere iteration du compteur_sgm_traites
	//printn(format("%1 fichiers traites",nombre_sgm));
	printn(format("%1 fichiers traites",compteur_sgm_traites-1));

	return 0;
}

function generer_delivery_suppr_el_sources_jrp_doublons_dz(){
	// Cette fonction generere un delivery de suppression pour les JRP EL qui deviennent doublons de JRP DZ

	// Exemples de fichiers delivery dans /mac.public/hulk/delivery_workflow/hulk-delivery-suppression-jrp-*.xml
	// <delivery date="2014-01-20T18:39:00" desc="suppression JRP EL" importer="editeur_el">
		// <del-entity uri="EL/SOURCES/JRP/ELNETDECISBLOC201304/A219401" scope="entity" />
		// <del-entity uri="EL/SOURCES/JRP/ELNETDECISBLOC200304/A101541" scope="entity" />
		// <del-entity uri="EL/SOURCES/JRP/ELNETDECISBLOC201305/A220080" scope="entity" />
	// </delivery>

	cout << "debug je suis dans la fonction generer_delivery_suppr_el_sources_jrp_doublons_dz\n";

	var delivery_dir = "/mac.public/hulk/delivery_workflow";
	var delivery_name = "hulk-delivery-suppr-auto-el-sources-jrp-doublons-dz-HULKMODE.xml";
	if env("HULKMODE") != nothing delivery_name = "hulk-delivery-suppr-auto-el-sources-jrp-doublons-dz-"+env("HULKMODE")+".xml";
	var filepath = filePath(delivery_dir,delivery_name);
	var filexml = FileStream(filepath,"w");
	filexml << format("<delivery date=\"%1\" desc=\"suppression auto EL/SOURCES/JRP doublons avec DZ\" importer=\"editeur_el\">",timeFormat(timeCurrent(),"%Y-%m-%dT%T"));
	filexml << format("<!-- Fichier genere par le programme %1 -->",progname);
	if G_map_id_logique_jrp_dalloz != nothing{
		for jrpel in eSort(G_map_id_logique_jrp_dalloz){
			var jrpdz = G_map_id_logique_jrp_dalloz[jrpel];
			var jrpbloc = getJrpBlocId(jrpel.transcript(UpperCase),G_mapJrpinfo);
			filexml << format("<del-entity uri=\"EL/SOURCES/JRP/%1/%2\" scope=\"entity\"/>",jrpbloc,jrpel);
			filexml << format("<!-- doublon avec DZ %1-->",jrpdz);
		}
	}
	else filexml << "<!-- G_map_id_logique_jrp_dalloz == nothing : a verifier -->";
	
	filexml << "</delivery>";
	close(filexml);
	
	cout << format("Envoyer le delivery cher JOUVE : %1\n",filepath);
	
	return 0;
}

main{
	/*var idSynt = "OC577";
cout << format("\n",Hulk_getCClistFromSynt(idSynt)); exit(0);*/

	handleCmdLine();
	initGlobales();

	// cout << format("G_setIdjrpToBuild.length()=%2\n",G_setIdjrpToBuild.length());
	cout << format("%1 Debut du programme hulk.bal\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));
	G_fileLog << format("%1 Debut du programme hulk.bal\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"));

	// 01/02/2013 AL
	// je change la regexp pour traiter rapidement seulement le gp74
	var regexp = RegExp(".(body|optj).sgm$");
	//if (G_smode == "test") regexp = RegExp("(gp74|s4).+\\.sgm$");
	//if (G_smode == "test") regexp = RegExp("(gp74|dp04|dp12|dp36|s4|z4|y2|w6).+\\.sgm$");

	var dirsgm = nothing;
	dirsgm = G_dirin;
	//dirsgm = filePath(G_ela_liv,"sgm_test");
	//dirsgm = filePath(G_ela_liv,"sgm");
	//var catalog = filePath(env("ELA_DTD"),"catalog");
	var listfilesgm = listfilesgm(dirsgm,regexp);

	// 18/02/2013 AL
	// Certains fichiers ne doivent pas être traites et provoquent des erreurs qui font planter le programme
	// Ils faut les retirer de la liste des fichiers à traiter
	// exemples :
	// dp13.refdocinfos.sgm
	// cdcoll.sgm
	//var setfilesgm = Set();
	//for elem in listfilesgm setfilesgm << elem;
	//if setfilesgm.knows("dp13.refdocinfos.sgm") setfilesgm.remove("dp13.refdocinfos.sgm");

	//  cout << format("\n\nlistfilesgm = %1\n\n",listfilesgm);
	//  cout << "je quitte ici";
	//  exit(1);

	G_fileLog << format("INFO PWD = %1\n",getCurrentDir());
	G_fileLog << format("INFO dirsgm = %1\n",dirsgm);
	G_fileLog << format("listfilesgm.length = %1\n",listfilesgm.length());
	cout << format("dirsgm = %1\n",dirsgm);
	cout << format("listfilesgm.length = %1\n",listfilesgm.length());

	var scissionDP05FileIn = "/mac.public/_DOCUMENTS_COMMUNS/scissionDP05/scissionDP05.txt" ;
	G_mapBlocsRj = getMapBlocsRj(scissionDP05FileIn);
	
	// 08/06/2012 AL
	// L'initialisation de la Map de versionning des textes se fait dans la fonction make_uc
	// uniquement lorsqu'on traite un texte et pas autre chose.
	// On gagne ainsi un peu de temps d'attente lorsqu'on veut debogguer le programme
	//make_mapTxtVersion();

	//if G_bMfi make_mfi();
	if G_bMfi make_mfi_new();
	// 25/09/2012 MB : cette fct n'est plus utilisee
	// on garde le booleen pour ecarser le set de $ELA_TMP_IDX si option jrp
	//if G_bJrp hulk_getJrpLogicID_prepa(listfilesgm);
	if G_bUc make_uc(listfilesgm);
	if G_bPatchSgml {
		make_patch(listfilesgm);
		// 03/10/2012 AL/MB
		// Charger la map jrpinfo
		// La map jrpinfo sera utilisee pour reconstituer l'id ELNETDECISBLOCAAAAMM
		// l'id ELNETDECISBLOCAAAAMM sera utilise dans les UCX URX
		//G_mapJrpinfo = chargerMapJrpinfoEL(G_setIdjrpToBuild);
	}

	// 23/10/2012 MB : ajout traitement option -sfiles : alimenter les fichier statiques
	if G_bSfiles {
		alim_CD_static_files(listfilesgm);
	}
	// 23/10/2012 MB : ajout traitement option -sverif : verifier les fichier statiques des CD/GPxx
	if G_bSverif {
		verif_CD_static_files(listfilesgm);
	}
	//YE Mantis 4380
	// Dans le cas des sources on traite que les jrp mis a jour 

	// 24/03/2017 alazreg https://jira.els-gestion.eu/browse/SIECDP-109
	// Generation d'un delivery de suppression pour les JRP EL qui deviennent doublon avec les JRP DALLOZ
	generer_delivery_suppr_el_sources_jrp_doublons_dz();

	// 24/03/2014 MB : j'ajoute le G_exitval pour tracage, suite a pb de prod d'aujourd'hui
	cout << format("%1 Fin du programme %2\nvaleur de retour = %3\n",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"),progname,G_exitval);
	G_fileLog << format("%1 Fin du programme %2\nvaleur de retour = %3",timeFormat(timeCurrent(),"%Y-%m-%d %H:%M:%S"),progname,G_exitval);

	close(G_fileLog);

	cout << format("Consulter les fchiers : \t%1\n\t%2\n",G_fileLogName, Hulk_icons_Ok_Nok);
	//system("emacs "+G_fileLogName+"&");

	exit(G_exitval);
}

error{
	ErrorLimit++;
	if SgmlFile == nothing SgmlFile = "";
	var msg = format("%1 :WARNING : Erreur lors de l'analyse du document\n%2\n\n",SgmlFile,errorMessage());
	//cerr << msg;
	//cerr << " erreur "+SgmlFile;

	//cout << msg;   
	
	if isaStream(G_fileLog) G_fileLog << msg;
	else cout << msg;

	set_exitval(1);

	// 27/12/2012 al/mb
	// On constate dans le script hulk_liv, apres appel de ce programme, que la valeur de $status est 0 (zero) alors que le programme hulk.bal plante avec un message stack overflow
	// Solution temporaire
	// Ajouter ici un controle du errorMessage
	// Si le message contient "stack overflow" alors quitter avec une valeur positive differente de 1
	if msg.search(0,"stack overflow") > 0{
		cout << "\n\n\n\n ERREUR stack overflow  \n\n\n\n";
		set_exitval(2);
		exit(G_exitval);
	}
}

