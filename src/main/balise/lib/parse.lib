/*
----------------------------------------------------------------------------
Auteur : Ahmed LAZREG
Date   : 19/03/2013

Objectifs :

Librairie de fonction pour parser les documents
----------------------------------------------------------------------------
*/

function insertComment(node,sComment){
    // cette fonction insere un commentaire dans le node donne en parametre
    insertSubTree(node,0,Node(document(node),"#COMMENT",sComment));
}

function getlistesgm(dirsgm,regexpsgm){
    // cette fonction parcourt le dossier dirsgm
    // elle ajoute dans une liste les fichiers qui correspondent a l'expression regexpsgm
    // la liste est retourn�e au programme appelant

    var listesgm = List();
    var filebasename = nothing;

	for filename in eSort(fileReadDir(dirsgm,true)){
		filebasename = fileBaseName(filename);
		if filebasename.rsearch(0,regexpsgm) != nothing listesgm << filename;
	}

    return listesgm;
}

function getDtdBaseName(filename){
	// 01/11/2014 alazreg
	// cette fonction remplace la fonction parse.lib::getDtdBaseNameFromFileName
	// cette fonction retourne le nom de la dtd en fonction de la racine
	// lecture du fichier en mode text
	// recuperation de la racine avec une regexp
	// cas general : dtd = racine + .dtd
	// exemple : dpform2-optj = dpform2-optj.dtd
	var file = FileStream(filename,"r");
	if file == nothing return nothing;
	
	var s_tmp = readAll(file).extract(0,500);
	close(file);
	var matches = s_tmp.rsearch(0,"<([^>]+)>");
	var dtd = nothing;
	if matches != nothing{
		// on recupere le nom de la balise sans les attributs
		var gi_racine = matches[0].sub[0].value.explode(" ")[0];
		// dtd = gi_racine.transcript(LowerCase).replace(0,"-optj","")+".dtd";
		dtd = gi_racine.transcript(LowerCase)+".dtd";
		if dtd == "liste.dtd" dtd = "listels.dtd";
	}
	
	return dtd;
}


function getDtdBaseNameFromFileName(filename){
	// 23/05/2012 alazreg
	// cette fonction est remplacee par la fonction parse.lib::getDtdBaseName
	// mantis 7241 cas formule dalloz
	// le nom du fichier ne respecte pas la convention de nommage EL
	// developpement d'une nouvelle fonction qui utilise la racine pour determiner la dtd
	// return getDtdBaseName(filename);
	
	// le code source dessous est remplace par l'appel a la fonction getDtdBaseName(filename)
	
    var basename = fileBaseName(filename);
    // la dtd subscription.dtd n'exite pas mais je pr�voit de la cr�er un jour
    //if basename.rsearch(0,"subscription.sgm") != nothing return "subscription.dtd";
    // idem pour type_source_lpveille.sgm
    //if basename.rsearch(0,"type_source_lpveille.sgm") != nothing return "src.dtd";
	
	//cout << format("DEBUG parse.lib function getDtdBaseNameFromFileName %1\n",filename);
	
	// 27/04/2017 alazreg ajout des cas gp259
	// pn => fp
	// tblrecap => fpro

    if basename.rsearch(0,"toc.sgm") != nothing return "toc.dtd";
    if basename.rsearch(0,"_lst-.+\\.optj.sgm") != nothing return "listels.dtd";
    if basename.rsearch(0,"body.sgm") != nothing return "etd-optj.dtd";
    if basename.rsearch(0,"tap.sgm") != nothing return "etd.dtd";
    if basename.rsearch(0,"tag[1-5]?.optj.sgm") != nothing return "tag-optj.dtd";
    if basename.rsearch(0,"taggp.optj.sgm") != nothing return "tag-optj.dtd";
    if basename.rsearch(0,"recapnew[1-4]?.optj.sgm") != nothing return "tag-optj.dtd";
    if basename.rsearch(0,"refdocinfos.sgm") != nothing return "refdocinfos.dtd";
    if basename.rsearch(0,"recmaj.optj.sgm") != nothing return "recmaj-optj.dtd";
    if basename.rsearch(0,"comjrp.optj.sgm") != nothing return "comjrp-optj.dtd";
    if basename.rsearch(0,"lref.sgm") != nothing return "tablesources.dtd";
    if basename.rsearch(0,"^y5[0-9]+.optj.sgm") != nothing return "etd-optj.dtd";
    if basename.rsearch(0,"^y5[0-9]+_archives?[0-9]+.optj.sgm") != nothing return "etd-optj.dtd";
    if basename.rsearch(0,"^[stuvwxyz][0-9][0-9][0-9][0-9].(optj|body).sgm") != nothing return "etd-optj.dtd";

	// 21/03/2016 SF ajout cas des formulaires pack ope
	if basename.rsearch(0,"[a-z][0-9]m[0-9]+p[0-9][0-9].optj.sgm") != nothing return "dpform2-optj.dtd";
	
    // ATTENTION
    // diffcile de reconnaitre les dpform2 des dpform avec le nom du fichier
    // On est oblig� d'ouvrir le fichier pour lire le contenu
    //if basename.rsearch(0,"[a-z][0-9]m[0-9]+.optj.sgm") != nothing return "dpform-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]m[0-9]+.optj.sgm") != nothing{
		// solution 1 : avec un Set fig� � maintenir � la main
		//if Set("w4","x3","z4").knows(basename.extract(0,2)) return "dpform-optj.dtd";
		//return "dpform2-optj.dtd";

		// solution 2 : dynamique avec lecture du contenu du fichier
		var contenu = nothing;
		var file = FileStream(filename,"r");
		contenu = readAll(file);
		close(file);
		if contenu != nothing{
			if contenu.transcript(LowerCase).rsearch(0,"^<dpform\\-optj") != nothing return "dpform-optj.dtd";
			if contenu.transcript(LowerCase).rsearch(0,"^<dpform2\\-optj") != nothing return "dpform2-optj.dtd";
		}
		return nothing;
    }

    if basename.rsearch(0,"^form") != nothing return "dpform2-optj.dtd"; //formule dalloz
    if basename.rsearch(0,"^w5m") != nothing return "dpform2-optj.dtd";
    if basename.rsearch(0,"indet(txt|jrp)\\.optj.sgm") != nothing return "idxet-optj.dtd";
    if basename.rsearch(0,"ind(txt|jrp)\\.optj.sgm") != nothing return "idx-optj.dtd";
    if basename.rsearch(0,"indthematxt\\.optj.sgm") != nothing return "idx-optj.dtd";
    if basename.rsearch(0,"txt(ant)?bloc_[0-9]+\\.optj.sgm") != nothing return "txtbloc-optj.dtd";
    if basename.rsearch(0,"jrpbloc(EJP)?_[0-9\\-]+\\.optj.sgm") != nothing return "decisbloc-optj.dtd";
    if basename.rsearch(0,"fpro[0-9]+\\.optj.sgm") != nothing return "fpro-optj.dtd";
    if basename.rsearch(0,"fprobloc.optj.sgm") != nothing return "fpro-optj.dtd";
    if basename.rsearch(0,"^a9tr") != nothing return "fpro-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]f[0-9]+\\.optj.sgm") != nothing return "fp-optj.dtd";
	// 02/12/2015 SF ajout cas des fp PACK OP
	if basename.rsearch(0,"[a-z][0-9]f[0-9]+(p[0-9][0-9])?\\.optj.sgm") != nothing return "fp-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]fpbloc[0-9]+\\.optj.sgm") != nothing return "fp-optj.dtd";
    if basename.rsearch(0,"^a9pn") != nothing return "fp-optj.dtd";
    if basename.rsearch(0,"^a9cl") != nothing return "fp-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]q[0-9]+\\.optj.sgm") != nothing return "qr-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]q.+\\.optj.sgm") != nothing return "qr-optj.dtd";
    if basename.rsearch(0,"[a-z][0-9]qrbloc.+\\.optj.sgm") != nothing return "qr-optj.dtd";
    if basename.rsearch(0,"[0-9]+[A-Z]\\.optj.sgm") != nothing return "docam-optj.dtd";
    if basename.rsearch(0,"^bofip.+.optj.sgm") != nothing return "docam-optj.dtd";


    if basename.rsearch(0,"cdcoll.sgm") != nothing return "cdcoll.dtd";

    if basename.rsearch(0,"naf.optj.sgm") != nothing return "cornafcc-optj.dtd";
    if basename.rsearch(0,"adlist.optj.sgm") != nothing return "adlist-optj.dtd";

    if basename.rsearch(0,"([dg]p_)?[oc]c[0-9]+.optj.sgm") != nothing return "ccol-optj.dtd";
    if basename.rsearch(0,"[oc]c[0-9]+_archives?[0-9]+.optj.sgm") != nothing return "ccol-optj.dtd";

    // ATTENTION REGEXP CODE
    // pour les codes le nom de fichier commence par la lettre c
    // MAIS attention � ne pas prendre aussi les ccol qui commencent aussi par c
    // exemples :
    // cc002.optj.sgm est une ccol
    // ct2.optj.sgm est un code
    if basename.rsearch(0,"^c.+optj.sgm") != nothing && basename.rsearch(0,"cc[0-9]+.optj.sgm") == nothing return "code-optj.dtd";

	// cout << format("DEBUG parse.lib function getDtdBaseNameFromFileName return %1\n",nothing);

    return nothing;
}

function corriger_dumpbalise46(fichier){
    // cette fonction corrige les entites &#38; qui sont generes par balise46 et balise49
    // exemple :
    // en sortie d'un programme balise46 on obtient "&#38;eacute;" au lieu de "&eacute;"

    // 10/06/2013 alazreg
    // ajout map de correspondance pour 3 caracteres a corriger
    var map_caracteres_a_corriger = Map();
    map_caracteres_a_corriger["&#38;"] = "&";
    map_caracteres_a_corriger["&#60;"] = "<";
    map_caracteres_a_corriger["&#62;"] = ">";

    var filein = FileStream(fichier,"r");
    if filein != nothing{
	var contenu = readAll(filein);
	close(filein);

	var filenameout = filePath(fileDirName(fichier),fileBaseName(fileTmpName()));
	//cout << format("filenameout = %1\n",filenameout);
	var fileout = FileStream(filenameout,"w");
	if fileout != nothing{
	    //fileout << contenu.replace(0,"&#38;","&");
	    for cle,val in map_caracteres_a_corriger contenu = contenu.replace(0,cle,val);
	    fileout << contenu;
	    close(fileout);
	    // on ecrase le fichier
	    var retour = fileRename(filenameout,fichier);
	    //cout << format("fileRename(%1,%2) = %3\n",filenameout,fichier,retour);
	}
    }
}

